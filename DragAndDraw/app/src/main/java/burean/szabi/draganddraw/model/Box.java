package burean.szabi.draganddraw.model;

import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model object to keep track of box coordinates; namely where the user initially placed their
 * finger, and where they dragged their finger to (where it currently is)
 */
public class Box implements Parcelable {

    public void setOrigin(PointF origin) {
        mOrigin = origin;
    }

    //NOTE: PointF already implements parcelable. (These are the sub-parcelable objects)
    private PointF mOrigin;
    private PointF mCurrent;
    private float mCenterRotation; //fixme: parcel it
    private float mAxesShiftDegs;

    public Box(PointF origin) {
        mOrigin = origin;
        mCurrent = origin;
    }//Box-constructor

    /**
     * Constructor to receive a Parcel and restore a Box instance with appropriate state.
     * @param in Parcel containing marshaled Box data.
     */
    private Box(Parcel in) {

        //restore sub-parcelable objects in same order they were saved
        mOrigin = in.readParcelable(PointF.class.getClassLoader());
        mCurrent = in.readParcelable(PointF.class.getClassLoader());
        mCenterRotation = in.readFloat();
        mAxesShiftDegs = in.readFloat();

    }

    public float getAxesShiftDegs() {
        return mAxesShiftDegs;
    }

    public void setAxesShiftDegs(float axesShiftDegs) {
        mAxesShiftDegs = axesShiftDegs;
    }

    public PointF getCurrent() {
        return mCurrent;
    }

    public void setCurrent(PointF current) {
        mCurrent = current;
    }

    public PointF getOrigin() {
        return mOrigin;
    }

    public float getCenterRotation() {
        return mCenterRotation;
    }

    public void setCenterRotation(float centerRotation) {
        mCenterRotation = centerRotation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //Save sub-parcelable objects (parcelable objects part of this object's state)
        dest.writeParcelable(mOrigin, flags);
        dest.writeParcelable(mCurrent, flags);
        dest.writeFloat(mCenterRotation);
        dest.writeFloat(mAxesShiftDegs);
    }

    public static final Creator<Box> CREATOR = new Creator<Box>() {

        @Override
        public Box createFromParcel(Parcel source) {
            return new Box(source); //use constructor taking in a Parcel
        }

        @Override
        public Box[] newArray(int size) {
            return new Box[size];
        }

    };

}//Box
