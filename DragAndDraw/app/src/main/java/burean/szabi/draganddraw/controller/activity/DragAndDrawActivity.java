package burean.szabi.draganddraw.controller.activity;

import android.support.v4.app.Fragment;

import burean.szabi.draganddraw.controller.SingleFragmentActivity;
import burean.szabi.draganddraw.controller.fragment.DragAndDrawFragment;

public class DragAndDrawActivity extends SingleFragmentActivity {

    @Override
    protected Fragment getFragment() {
        return DragAndDrawFragment.newInstance();
    }

}//DragAndDrawActivity
