package burean.szabi.draganddraw.controller.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import burean.szabi.draganddraw.R;
import burean.szabi.draganddraw.view.BoxDrawingView;

/**
 *
 */
public class DragAndDrawFragment extends Fragment {

    public static final String TAG = DragAndDrawFragment.class.getSimpleName();

    private Toolbar mToolbar;
    private BoxDrawingView mBoxDrawingView;

    public static DragAndDrawFragment newInstance() {

        Bundle args = new Bundle();
        //add args here
        DragAndDrawFragment fragment = new DragAndDrawFragment();
        fragment.setArguments(args);
        return fragment;

    }//newInstance

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private void setupToolBar() {

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View fragRootView = inflater.inflate(R.layout.fragment_drag_and_draw, container, false);

        mToolbar = (Toolbar) fragRootView.findViewById(R.id.toolbar);
        mBoxDrawingView = (BoxDrawingView) fragRootView.findViewById(R.id.drag_and_draw_view_id);

        setupToolBar();

        return fragRootView;

    }//onCreateView

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_layout, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_clean:
                //clear custom view
                mBoxDrawingView.cleanCanvas();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }//switch(menuItem)

    }//onOptionsItemSelected

}
