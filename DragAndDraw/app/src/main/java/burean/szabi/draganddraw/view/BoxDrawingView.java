package burean.szabi.draganddraw.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import burean.szabi.draganddraw.R;
import burean.szabi.draganddraw.model.Box;

/**
 * A custom view that will respond to user touching the screen and dragging.
 * Will draw boxes in response to these inputs.
 */
public class BoxDrawingView extends View {

    private static final String TAG = BoxDrawingView.class.getSimpleName();

    private boolean mShouldCleanCanvas = false;
    private Box mCurrentBox;
    private List<Box> mBoxes = new ArrayList<>(); //list of boxes the user drew
    private Paint mBoxPaint;
    private Paint mBackgroundPaint;
    private int mPrimaryPointerId = MotionEvent.INVALID_POINTER_ID;
    private int mSecondaryPointerId = MotionEvent.INVALID_POINTER_ID;

    //Used when creating the view in code
    public BoxDrawingView(Context context) {
        this(context, null);
    }

    //Used when inflating the view from XML
    public BoxDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Paint objects define the characteristics of the shapes we draw (color, thickness, etc..)

        // Canvas objects define what shapes we draw (circle, line, square, etc..)

        //Box background will be an 86% transparent red color
        mBoxPaint = new Paint();
        mBoxPaint.setColor(ContextCompat.getColor(context, R.color.color13OpRed));

        //Canvas background will be 100% opacity white color (not pure white), see ARGB value
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(ContextCompat.getColor(context, R.color.color100OpWhite));

    }

    /**
     * Call to clear this view
     */
    public void cleanCanvas() {
        mShouldCleanCanvas = true;
        mBoxes = new ArrayList<>(); //reset saved boxes
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        /**
         * - Resize box with one finger via dragging.
         * - With two fingers, resize by wherever the second finger was touched.
         * and while moving 2ndPointer, rotate the (now) constant shape.
         */

        /* Instance of MotionEvent describes the touch event, ie: its location and action */

        //MotionEvent actions are int constants (see class), such as ACTION_DOWN, ACTION_MOVE, etc..

//        PointF current = new PointF(event.getX(), event.getY());

        switch (MotionEventCompat.getActionMasked(event)) {
            case MotionEvent.ACTION_DOWN:
                //always primary pointer

                if (mPrimaryPointerId == MotionEvent.INVALID_POINTER_ID) {
                    mPrimaryPointerId = event.getPointerId(event.getActionIndex());
                    Log.i(TAG, "ACTION_DOWN: primary-pointerId: " + mPrimaryPointerId);
                }

                PointF origin = new PointF(
                        event.getX(event.findPointerIndex(mPrimaryPointerId)),
                        event.getY(event.findPointerIndex(mPrimaryPointerId))
                );

                mCurrentBox = new Box(origin);
                mBoxes.add(mCurrentBox);

                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                if (mSecondaryPointerId == MotionEvent.INVALID_POINTER_ID) {
                    mSecondaryPointerId = event.getPointerId(event.getActionIndex());

                    //Second touch, save box shape and rotate only //fixme: after rotate works, after re-press primary, should only be able to resize it
                    mPrimaryPointerId = MotionEvent.INVALID_POINTER_ID;

                    //This offset angle will be a constant for this box - if not already set (if set, must have saved previous angle from last rotation before user lifted second finger)
                    if (mCurrentBox.getAxesShiftDegs() == 0.0f) {
                        Log.i(TAG, "setting axes shift");
                        mCurrentBox.setAxesShiftDegs(getAxesOffset(
                                event.getX(event.findPointerIndex(mSecondaryPointerId)),
                                event.getY(event.findPointerIndex(mSecondaryPointerId)),
                                mCurrentBox)
                        );
                    }

                    Log.i(TAG, "ACTION_POINTER_DOWN: secondary-pointerId: " + mSecondaryPointerId);
                }
                //3,4,etc touches.. can save id as well if previous ids are empty.

                break;

            case MotionEvent.ACTION_MOVE:
//                Log.i(TAG, "ACTION_MOVE");

                if (mCurrentBox != null) {

                    //updates ALL current pointers of this touch event
                    for (int i = 0; i < event.getPointerCount(); i++) {
                        int ptrId = event.getPointerId(i);

                        if (ptrId == mPrimaryPointerId //primary motion, draw only if one pointer
                                && mSecondaryPointerId == MotionEvent.INVALID_POINTER_ID) {
//                            Log.i(TAG, "primary");
                            PointF current = new PointF(event.getX(i), event.getY(i));
                            mCurrentBox.setCurrent(current);

                            invalidate();//re-draw box as it's dragged w/ primary pointer

                        }

                        if (ptrId == mSecondaryPointerId) {//secondary motion, ONLY rotate
//                            Log.i(TAG, "secondary");

                            mCurrentBox.setCenterRotation(
                                    getRotationFromTouchEvent(
                                            event.getX(i),
                                            event.getY(i),
                                            mCurrentBox
                                    )
                            );

                            //Note: if we want to also re-size here, simply
                            //  get a PointF, add as origin, and invalidate

                            invalidate();

                        }

//                        invalidate();

                    }//for each touching pointer

                }//non-null current box

                break;

            case MotionEvent.ACTION_UP:
                Log.i(TAG, "ACTION_UP");

                if (event.getPointerId(event.getActionIndex()) == mPrimaryPointerId) {
                    Log.i(TAG, "releasing primary");

                    //reset primary pointer:
                    mPrimaryPointerId = MotionEvent.INVALID_POINTER_ID;

                }
                if (event.getPointerId(event.getActionIndex()) == mSecondaryPointerId) {
                    Log.i(TAG, "releasing secondary");

                    //reset second pointer
                    mSecondaryPointerId = MotionEvent.INVALID_POINTER_ID;

                }
                mCurrentBox = null;

                break;

            case MotionEvent.ACTION_POINTER_UP:
                Log.i(TAG, "ACTION_POINTER_UP");

                if (event.getPointerId(event.getActionIndex()) == mPrimaryPointerId) {
                    Log.i(TAG, "releasing primary");

                    //reset primary pointer:
                    mPrimaryPointerId = MotionEvent.INVALID_POINTER_ID;

                }
                if (event.getPointerId(event.getActionIndex()) == mSecondaryPointerId) {
                    Log.i(TAG, "releasing secondary");

                    //reset second pointer
                    mSecondaryPointerId = MotionEvent.INVALID_POINTER_ID;

                }
                break;

            case MotionEvent.ACTION_CANCEL:
                Log.i(TAG, "ACTION_CANCEL");

                //reset pointers:
                mPrimaryPointerId = MotionEvent.INVALID_POINTER_ID;
                mSecondaryPointerId = MotionEvent.INVALID_POINTER_ID;

                break;

        }

        return true; // returns T if event was handled, false otherwise

    }//onTouchEvent

    @Override
    protected void onDraw(Canvas canvas) {
        //NOTE: This is called when views are initially set up, and whenever invalidate() is called.

        /*
            Hook that we can use to draw custom views. Top level views delegate their
            drawing behaviour to their children, and so on. Recall the composite pattern.
         */

//        Log.i(TAG, "onDraw");

        if (mShouldCleanCanvas) {
            //if this flag is set, clean the view's canvas by drawing over it

            mShouldCleanCanvas = false;//first, reset flag
            canvas.drawPaint(mBackgroundPaint); //draw a paint to fill entire canvas
        } else {

            //Draw each box in the mBoxes list onto the screen, using the appropriate pain objects:

            //First, fill the background:
            canvas.drawPaint(mBackgroundPaint); //draw a paint to fill entire canvas

            //Then, for each box in the list of boxes, draw the rectangle:
            for (Box box : mBoxes) {

            /*
                NOTE: use min/max because coordinates increase in value left-to-right,
                but boxes can be drawn right to left

                IMPORTANT: screen coordinates go from (0,0) at top/left to (x,y) at bottom/right!
             */

                float left = getBoxLeft(box);
                float right = getBoxRight(box);
                float top = getBoxTop(box);
                float bottom = getBoxBottom(box);

                float pivotX = getBoxPivotXAxes(box);
                float pivotY = getBoxPivotYAxes(box);

                canvas.save();

                canvas.rotate(box.getCenterRotation(), pivotX, pivotY);
                canvas.drawRect(
                        left, //left
                        top, //top
                        right, //right
                        bottom, //bottom
                        mBoxPaint //paint
                );

                canvas.restore();

            }//for each box in mBoxes

        }//should not clean canvas

    }//onDraw

    /**
     * Returns degree of rotation from a pivot point to user TouchEvent.
     * NOTE: top middle = 0degree, bottom middle = pi, <br>
     * and right side = positive angles, left side = negative angles<br>
     * (So angles increase clockwise starting from 12-o'clock to 6-o'clock)
     *
     * @param x x-coordinate (in pixels) of a motion event
     * @param y y-coordinate (in pixels) of a motion event
     * @return Degree of rotation
     */
    private float getRotationFromTouchEvent(float x, float y, Box mBox) {

        float px = getBoxPivotXAxes(mBox);
        float py = getBoxPivotYAxes(mBox);

        //Get x and y offset from pivot point to touch event
        float dx = x - px;
        float dy = py - y; //reversed b/c +y is above x-axis, with 0 = top

        float axesOffset = mBox.getAxesShiftDegs();
        Log.i(TAG, "axesOffset: " + axesOffset);

        //subtract axesOffset to account for initial user press angle with box's pivot point (px,py)
        double rotation = Math.toDegrees(Math.atan(dx / dy)) - axesOffset;

        return (float) (rotation);
    }

    /**
     * Calculates the degree shift (from pi/2 to -pi/2) of the y-axes
     * to shift the cartesian plane such that the user's second touch to the
     * box's pivot point is the new, shifted x-axes
     */
    private float getAxesOffset(float x, float y, Box mBox) {

        //works correctly

        float px = getBoxPivotXAxes(mBox);
        float py = getBoxPivotYAxes(mBox);

        //Get x and y offset from pivot point to touch event
        double dx = x - px;
        double dy = py - y; //reversed b/c +y is above x-axis, with 0 = top

        return (float) Math.toDegrees(Math.atan(dx / dy));

    }//getAxesOffset

    private float getBoxPivotXAxes(Box mBox) {
        float left = getBoxLeft(mBox);
        float right = getBoxRight(mBox);
        return (left + right) / 2;
    }//getBoxPivotXAxes

    private float getBoxPivotYAxes(Box mBox) {
        float top = getBoxTop(mBox);
        float bottom = getBoxBottom(mBox);
        return (top + bottom) / 2;
    }//getBoxPivotYAxes

    private float getBoxLeft(Box mBox) {
        return Math.min(mBox.getOrigin().x, mBox.getCurrent().x); //min x-coord
    }//getBoxLeft

    private float getBoxRight(Box mBox) {
        return Math.max(mBox.getOrigin().x, mBox.getCurrent().x); //max x-coord
    }//getBoxRight

    private float getBoxTop(Box mBox) {
        return Math.min(mBox.getOrigin().y, mBox.getCurrent().y); //min y-coord
    }//getBoxTop

    private float getBoxBottom(Box mBox) {
        return Math.max(mBox.getOrigin().y, mBox.getCurrent().y); //max y-coord
    }//getBoxBottom

    /**
     * Returns screen height dimension (in pixels)
     *
     * @return screen height in pixels.
     */
    private int getScreenHeight() {

        Resources res = getResources();
        return res.getDisplayMetrics().heightPixels;
    }

    /**
     * Returns screen width dimension (in pixels)
     *
     * @return screen width in pixels.
     */
    private int getScreenWidth() {

        Resources res = getResources();
        return res.getDisplayMetrics().widthPixels;

    }

    @Override
    protected Parcelable onSaveInstanceState() {

        //See: https://www.intertech.com/Blog/android-custom-view-tutorial-part-3-saving-state/

        Parcelable superState = super.onSaveInstanceState();// parcelled super state

        //We need to somehow add our own custom state to that parcel, hence this wrapper:
        SavedState savedState = new SavedState(superState); //create instance of SavedState
        savedState.mBoxesToSave = mBoxes;//set view's state to save
        savedState.mPrimaryPointerId = mPrimaryPointerId;
        savedState.mSecondaryPointerId = mSecondaryPointerId;

        return savedState;

    }//onSaveInstanceState

    @Override
    protected void onRestoreInstanceState(Parcelable state) {

        //See: https://www.intertech.com/Blog/android-custom-view-tutorial-part-3-saving-state/

        if (state instanceof SavedState) {//Should always be true

            SavedState savedState = (SavedState) state; //cast Parcelable to SavedState
            super.onRestoreInstanceState(savedState.getSuperState()); //restore super state
            mBoxes = savedState.mBoxesToSave; //restore custom view state
            mPrimaryPointerId = savedState.mPrimaryPointerId;
            mSecondaryPointerId = savedState.mSecondaryPointerId;

        } else {
            Log.e(TAG, "ERROR: Wrong saved stated instance");
        }

    }//onRestoreInstanceState

    /**
     * A wrapper class which we extend to encapsulate the saved state of the super class View,
     * AND add our own custom view state to the Parcelable object that will be returned in
     * MyCustomView.onSaveInstanceState(). <br>
     * (Calling super in that method simply returns a Parcelable, we can't just add our own state.)
     * <p>
     * -> Hence this wrapper class that does just that. Saves super state that we pass as a
     * Parcelable, AND we can specify our own custom view state to sate.
     */
    private static class SavedState extends BaseSavedState {

        //NOTE: The super class [BaseSavedState] takes care of saving & restoring super state
        //      (Just have to call super in both constructors + writeToParcel method)

        /*
            Notice how this is literally just a class like a POJO. It has some state
            (our custom view state that we assign it), and it implements the Parcelable interface
            via the parent View.BaseSavedState class. Then we just implement the parcelable
            components and use this whenever we need to save super-state (handled by
            the parent View.BaseSavedState class), and custom-state.
         */

        //State of outer-class to save:
        private List<Box> mBoxesToSave;
        private int mPrimaryPointerId;
        private int mSecondaryPointerId;

        /**
         * Constructor that's called when we're restoring the state of our custom view class.
         * NOTE: This is a private constructor as it's supposed to be accessed by the CREATOR only
         *
         * @param in Parcel containing saved state
         */
        private SavedState(Parcel in) {
            //required constructor for CREATOR; to restore object from a Parcel
            super(in); //restore parent components
            in.readList(mBoxesToSave, Box.class.getClassLoader()); //restore list
            mPrimaryPointerId = in.readInt();
            mSecondaryPointerId = in.readInt();
        }

        /**
         * Constructor that's called when we're saving the state of our custom view class.
         *
         * @param superState Parcelable saved-state of this view's super class.
         */
        public SavedState(Parcelable superState) {
            //require constructor of View.BaseSavedState, as expected (to save super state)
            super(superState); //let parent handle saving super state
        }

        /**
         * Used to handle writing our custom view state to an out parcel. Will be retrieved in
         * the constructor that has a Parcel as it's parameter.
         *
         * @param out   Parcel that will be saved and passed to SavedState(Parcel source).
         * @param flags Flags for writing Views state to the out Parcel.
         */
        @Override
        public void writeToParcel(Parcel out, int flags) {
            //required from Parcelable to specify what to save
            super.writeToParcel(out, flags); //save super state
            out.writeList(mBoxesToSave);
            out.writeInt(mPrimaryPointerId);
            out.writeInt(mSecondaryPointerId);
        }

        //This is required for any class that implements parcelable; read up on it.
        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
                /*
                     NOTE: it is common here to simply call the private constructor
                     taking in a Parcel and delegate creating a new object to that.
                 */
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }

        };//CREATOR constant

    }//SavedState

}//BoxDrawingView
