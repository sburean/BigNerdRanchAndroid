package burean.szabi.draganddraw.controller;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import burean.szabi.draganddraw.R;

/**
 * Template for an activity that hosts a single fragment.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        FragmentManager fm = getSupportFragmentManager();
        Fragment hostedFrag = fm.findFragmentById(R.id.main_fragment_container);
        if (hostedFrag == null) {
            fm.beginTransaction()
                    .add(R.id.main_fragment_container, getFragment())
                    .commit();
        }

    }//onCreate

    protected abstract Fragment getFragment();

    /**
     * Hook to provide custom layout to inflate for this generic activity.
     * Must contain a FrameLayout with :id="@+id/main_fragment_container"
     * @return Layout resource ID to inflate.
     */
    @LayoutRes
    protected int getLayout() {
        return R.layout.activity_single_fragment;
    }

}
