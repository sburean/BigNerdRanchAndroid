package burean.szabi.criminalintent.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import burean.szabi.criminalintent.R;
import burean.szabi.criminalintent.controller.fragment.CrimeFragment;
import burean.szabi.criminalintent.model.Crime;
import burean.szabi.criminalintent.model.CrimeLab;

/**
 * Concrete activity that hosts a single CrimeFragment and allows users to <br>
 * swipe between various crimeFragments of a collection (Left/Right).
 */
public class CrimePagerActivity extends AppCompatActivity
                                implements CrimeFragment.Callbacks {

    /*
        CrimePagerActivity is responsible for creating & managing a ViewPager.
        NOTE: ViewPager is only available in the support library!
     */

    private static final String TAG = CrimePagerActivity.class.getSimpleName();
    private static final String INTENT_KEY_EXTRA_CRIME_ID =
            CrimePagerActivity.class.getName() + "crime_Id";

    private ViewPager mViewPager;

    private List<Crime> mCrimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crime_pager);
        Log.d(TAG, "onCreate()");

        mCrimes = CrimeLab.get(this).getCrimes();

        //This is simply used to find the crime's index in the collection based on its UUID.
        UUID crimeID = (UUID) getIntent().getSerializableExtra(INTENT_KEY_EXTRA_CRIME_ID);

        //NOTE: for more info about PagerAdapters for fragment: http://stackoverflow.com/q/18747975
        mViewPager = (ViewPager) findViewById(R.id.activity_crime_pager_view_pager);
        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {

            /*
                FragmentManager is needed such that FragmentStatePagerAdapter is able
                to add fragments generated through getItem(..) to the activity.
             */

            @Override
            public Fragment getItem(int position) {
                /*
                    Returns a configured fragment located at the specified position within
                    the model collection.

                    NOTE: We first get a reference to the crime stored at the position,
                        and use that to get its ID to pass into the fragment's factory method.
                 */

                Log.d(TAG, "FragmentStatePagerAdapter: getItem(" + position + ")");
                Crime crime = mCrimes.get(position);
                return CrimeFragment.newInstance(crime.getId());

            } //getItem()

            @Override
            public int getCount() {
                /*
                    Returns # of elements in the model collection;
                    indication of how many fragments the viewPager will have to hold.
                 */

                //called on onTouchEvents = A LOT (don't Log it)
                return mCrimes.size();

            }//getCount
        });

        for (Crime mCrime : mCrimes) {

            /*
                Loop through model collection and set current item to item
                whose ID was passed as the extra with the intent that
                started this CrimePagerActivity instance.
             */

            if (mCrime.getId().equals(crimeID)) {
                mViewPager.setCurrentItem(mCrimes.indexOf(mCrime));
                break;
            }
        }

    }//onCreate

    /**
     * Used by any context that wants to launch this activity to get an appropriate intent.
     *
     * @param context: Source context that will launch an instance of this activity.
     * @param crimeID: Parameter expected by new instance of this activity.
     * @return Intent to start a new instance of this activity with expected parameters.
     */
    public static Intent newIntent(Context context, UUID crimeID) {

        /*
            This intent has to be retrieved by an instance of CrimeActivity;
            Will pass the contained crimeID as an ARG to any hosted CrimeFragments.
         */

        Log.d(TAG, "newIntent()");
        Intent i = new Intent(context, CrimePagerActivity.class);
        i.putExtra(INTENT_KEY_EXTRA_CRIME_ID, crimeID);
        return i;

    }//newIntent

    @Override
    public void onCrimeUpdated(Crime crime) {
        //Do nothing

        /*
            NOTE: This callback method is not used in this activity, since
                this activity is used on Phones and those do not display
                both the CrimeListFragment and CrimeFragment together
                as master-detail fragments. Do not need to update the master
                from the detail.
         */
    }
}
