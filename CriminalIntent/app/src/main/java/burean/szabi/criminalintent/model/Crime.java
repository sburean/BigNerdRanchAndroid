package burean.szabi.criminalintent.model;

import android.util.Log;

import java.util.Calendar;
import java.util.UUID;

public class Crime {

    private static final String TAG = Crime.class.getSimpleName();

    private UUID mId; //uniquely identify an instance of a Crime-object; read-only
    private String mTitle;
    private Calendar mDate; //time when crime occurred; use Date() if only need timestamp.
    private boolean mSolved;
    private String mSuspect; //suspect of this crime

    public Crime() {
        this(UUID.randomUUID()); // Generate unique identifier
        mDate = Calendar.getInstance();
    }

    public Crime(UUID id){
        /*
            Overloaded constructor as we have to create a new Crime
            object with a specific UUID retrieved from a database
         */

        mId = id;
        mDate = Calendar.getInstance(); //populates calendar with timestamp of NOW
    }

    public void setDate(Calendar date) {
        mDate.set(Calendar.YEAR, date.get(Calendar.YEAR));
        mDate.set(Calendar.MONTH, date.get(Calendar.MONTH));
        mDate.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH));
    }

    public void setTime(Calendar time){
        mDate.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
        mDate.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
    }

    public void setSolved(boolean solved) {
//        Log.d(TAG, "setSolved: " + Boolean.toString(solved));
        mSolved = solved;
    }

    public Calendar getDate() {
        return mDate;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setSuspect(String suspect) {
        mSuspect = suspect;
    }

    public String getSuspect() {

        return mSuspect;
    }

    /**
     * Retrieve a file name for a crime object.
     * @return The file name in the format: "IMG_IDasString.jpg"
     */
    public String getPhotoFileName(){
        return "IMG_" + getId().toString() + ".jpg";
    }
}