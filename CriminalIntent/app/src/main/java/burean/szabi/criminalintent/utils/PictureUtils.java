package burean.szabi.criminalintent.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.Log;

import java.io.File;

/**
 * Contains utility methods for retrieving appropriately sized bitmaps for display.
 */
public class PictureUtils {

    private static final String TAG = PictureUtils.class.getSimpleName();

    /**
     * Method to retrieve a full-resolution image file, and scale it down
     * appropriately to match a display element's bounds. <br>
     * NOTE: This method should be executed in a background task as retrieving images
     * from any location other than memory is slow.
     *
     * @param src       File path to image location.
     * @param dstWidth  Display element's width.
     * @param dstHeight Display element's height.
     * @return Bitmap object representing the image; scaled down as needed.
     */
    public static Bitmap getScaledBitmap(File src, int dstWidth, int dstHeight) {
        Log.d(TAG, "getScaledBitmap(" + src.toString() + ")");

        //fixme: THIS METHOD SHOULD ONLY BE EXECUTED IN A WORKER THREAD:

        //Get an appropriate scaling factor to use for the image given by the File's path
        int mInSampleSize = getScalingFactor(src, dstWidth, dstHeight);

        /*
            Finally, set updated inSampleSize in the options-object.
             Use the appropriate overloaded decode method to retrieve a scaled Bitmap object.
         */

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = mInSampleSize;
        return BitmapFactory.decodeFile(src.toString(), options);

    }//getScaledBitmap

    /**
     * Method to retrieve a full-resolution image file, and scale it down
     * appropriately to match a display element's bounds. This should be used
     * if a Bitmap image is needed before the layout is inflated, and therefore have
     * no direct access to the display element's dimensions. <br>
     * NOTE: This method should be executed in a background task as retrieving images
     * from any location other than memory is slow.
     *
     * @param src      File path to image location.
     * @param activity Activity hosting a display element to display this bitmap.
     * @return @return Bitmap object representing the image; scaled down as needed.
     */
    public static Bitmap getScaledBitmap(File src, Activity activity) {

        Point point = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(point); //access screen dimensions
        return getScaledBitmap(src, point.x, point.y);

    }

    /**
     * Helper method called by {@link #getScaledBitmap(File, int, int)} to determine
     * the appropriate scaling factor for decoding an image gives by the file path
     *
     * @param src       File path to image location
     * @param dstWidth  Display element's width
     * @param dstHeight Display element's height
     * @return scaling factor for the specified image to be bound by the specified limits
     */
    private static int getScalingFactor(File src, int dstWidth, int dstHeight) {

        /*
            First, read source file to get full-res width & height
             NEED TO pass option to NOT allocation emory for full-res image at this point
         */
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(src.toString(), options);

        int srcWidth = options.outWidth;
        int srcHeight = options.outHeight;

        /*
            Next, need to figure out if need to scale, and then retrieve the
             scaling factor (options.inSampleSize) by determining if image is
             in landscape of portrait orientation, and using the SHORTER side
             for calculations to avoid distortion.
            (try it the opposite way to see effects)
         */
        int mIntSampleSize = 1;
        if (srcHeight > dstHeight || srcWidth > dstWidth) {
            //Need to scale down:

            if (srcWidth > srcHeight) {
                //Landscape orientation; use height for scaling calculation
                mIntSampleSize = Math.round(srcHeight / dstHeight); //height shorter

            } else {
                //Portrait orientation (or square picture); use width for scaling calculation
                mIntSampleSize = Math.round(srcWidth / dstWidth); //width shorter

            }//if-else (orientation?)

        }//if-else (need to scale down?)

        return mIntSampleSize;

    }//getScalingFactor

}
