package burean.szabi.criminalintent.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import burean.szabi.criminalintent.database.CrimeDbSchema.CrimeTable;

/**
 * Implementation of SQLiteOpenHelper to help manage the SQLite databased used by this app. <br>
 * Designed to get rid of the "grunt work" of opening a SQLiteDataBase.
 */

public class CrimeBaseHelper extends SQLiteOpenHelper {

    private static final String TAG = CrimeBaseHelper.class.getSimpleName();

    //constants to define database version & name
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "crimeBase.db";


    public CrimeBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        Log.d(TAG, "CrimeBaseHelper()");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate()");

        //FIXME: We do not have to specify column types at creation time; but we should
        String sqlQuery =
                "CREATE TABLE " + CrimeTable.NAME
                + "("
                + "_id integer primary key autoincrement, "
                + CrimeTable.Cols.UUID + ", "
                + CrimeTable.Cols.TITLE + ", "
                + CrimeTable.Cols.DATE + ", "
                + CrimeTable.Cols.SOLVED + ", "
                + CrimeTable.Cols.SUSPECT
                + ");";

        Log.d(TAG, "sqlQuery: " + sqlQuery);

        db.execSQL(sqlQuery);

    }//onCreate

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade(..., oldVersion:" + oldVersion + ", newVersion" + newVersion + ")");
        //This application will only have one version; ignore for now. Learn this when needed.
    }


}
