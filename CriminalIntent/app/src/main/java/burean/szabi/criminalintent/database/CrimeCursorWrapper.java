package burean.szabi.criminalintent.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.Calendar;
import java.util.UUID;

import burean.szabi.criminalintent.database.CrimeDbSchema.CrimeTable;
import burean.szabi.criminalintent.model.Crime;

/**
 * Custom CursorWrapper that wraps a Cursor object containing results we're interested in
 * and provides additional methods to retrieve them. <br>
 * (It is a concrete decorator)
 */
public class CrimeCursorWrapper extends CursorWrapper {

    public CrimeCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /**
     * Used to pull relevant column data from the cursor AT ITS CURRENT POSITION,
     * and construct a model object.
     *
     * @return Crime object with relevant data from the cursor.
     *
     * @see burean.szabi.criminalintent.model.CrimeLab#getContentValuesFrom(Crime)
     * CrimeLab.getContentValuesFrom(Crime) - for deconstructing a model object so it
     * may be saved into the database.
     */
    public Crime getCrime() {

        /*
            NOTE: We would usually do: cursor.getString(index);
                   where index = cursor.getColumnIndex(column title)
         */

        String uuidString = getString(getColumnIndex(CrimeTable.Cols.UUID));
        String title = getString(getColumnIndex(CrimeTable.Cols.TITLE));
        long date = getLong(getColumnIndex(CrimeTable.Cols.DATE));
        int isSolved = getInt(getColumnIndex(CrimeTable.Cols.SOLVED));
        String suspect = getString(getColumnIndex(CrimeTable.Cols.SUSPECT));

        Calendar crimeDate = Calendar.getInstance();
        crimeDate.setTimeInMillis(date);

        Crime crime = new Crime(UUID.fromString(uuidString));
        crime.setTitle(title);
        crime.setDate(crimeDate);
        crime.setTime(crimeDate);
        crime.setSolved(isSolved != 0);
        crime.setSuspect(suspect);

        return crime;

    }//getCRime

}
