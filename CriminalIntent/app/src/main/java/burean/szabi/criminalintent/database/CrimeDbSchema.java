package burean.szabi.criminalintent.database;

/**
 * Defines a schema in Java code; contains nested-classes that define each table <br>
 * that will by used by the SQLite database with this application.
 */
public class CrimeDbSchema {

    /**
     * Final class defining a table to hold model (Crime) objects; contains a constant for the <br>
     * table name, and nested classes that define this table's columns based on the model's state.
     *
     * @see burean.szabi.criminalintent.model.Crime
     */
    public static final class CrimeTable {

        public static final String NAME = "crimes";

        /**
         * Final class defining the columns within the CrimeTable.
         *
         * @see CrimeTable
         */
        public static final class Cols{

            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String SOLVED = "solved";
            public static final String SUSPECT = "suspect";

        }//CrimeDbSchema.CrimeTable.Cols

    }//CrimeDbSchema.CrimeTable

    //More table definitions here as needed.

}//CrimeDbSchema
