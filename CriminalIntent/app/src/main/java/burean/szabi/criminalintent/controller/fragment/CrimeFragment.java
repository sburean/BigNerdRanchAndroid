package burean.szabi.criminalintent.controller.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import burean.szabi.criminalintent.R;
import burean.szabi.criminalintent.model.Crime;
import burean.szabi.criminalintent.model.CrimeLab;
import burean.szabi.criminalintent.utils.PictureUtils;
import burean.szabi.criminalintent.utils.StringUtils;

/**
 * CrimeFragment is a controller that interacts with model and view objects.
 * Its job is to present the details of a specific crime and update those details
 * as the user changes them.
 * <p>
 * CrimeFragment displays information about a SINGLE crime.
 */
public class CrimeFragment extends Fragment {

    //TODO: time picker not working properly

    private static final String TAG = CrimeFragment.class.getSimpleName();
    private static final String ARG_CRIME_ID = "crimeUUID";
    private static final String DIALOG_DATE = "DatePickerFragment";
    private static final String DIALOG_TIME = "TimePickerFragment";
    private static final String DIALOG_PHOTO = "PhotoFragment";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_TIME = 1;
    private static final int REQUEST_CONTACT = 2;
    private static final int REQUEST_PHOTO = 3;
    private static final int PERMISSION_REQ_READ_CONTACTS_FOR_CALL = 4;
    private static final int PERMISSION_REQ_READ_EXTERNAL_STORAGE_FOR_PHOTO = 5;

    private EditText mTitleField;
    private Button mDateButton, mTimeButton, mReportButton,
            mSuspectButton, mCallSuspectButton;
    private CheckBox mSolvedCheckBox;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;

    private Crime mCrime;
    private File mPhotoFilePath;

    private Callbacks mCallbacksObserver; //host should update a mode list after changes occurred.

    //Note: fragment lifecycle methods are public - called by host activity internally.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
            Note: We do NOT inflate the fragment's view in onCreate() !
            We simply configure the fragment instance here.
         */

        //Retrieve this fragment's arguments (crimeID):
        UUID crimeID = (UUID) getArguments().getSerializable(ARG_CRIME_ID);

        //Store a reference to a Crime object from CrimeLab's crime-list based on crimeID:
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeID);

        //Store a reference to storage location for this crime's photo
        mPhotoFilePath = CrimeLab.get(getActivity()).getPhotoFilePath(mCrime);

        /* FIXME: This should return an in indication if the model object was changed by user.

            Have to figure out how to set RESULT_OK in a lifecycle method; it gets
            set to RESULT_CANCELED automatically in the super call() within a
            lifecycle method. Even setting result BEFORE the call to SUPER doesn't work.
         */

        //Indicate that this fragment will use a ToolBar and should get lifecycle calls for it.
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crime, container, false);

        /*
            We inflate a fragment's layout, and return the resulting View object
            to the hosting activity!

            Then we can wire up our views contained in the fragment's layout.

            NOTE: By the time the onCreateView(..) lifecycle method is called by the
                    containing activity, onCreate(..) completed and we have a valid
                    reference to our model object (our data) that we're working with!

                --> (this is a common pattern we should follow)
         */

        //Get references to our widgets:
        mTitleField = (EditText) view.findViewById(R.id.crime_title);
        mDateButton = (Button) view.findViewById(R.id.crime_date);
        mTimeButton = (Button) view.findViewById(R.id.crime_time);
        mSuspectButton = (Button) view.findViewById(R.id.crime_suspect);
        mReportButton = (Button) view.findViewById(R.id.crime_report);
        mCallSuspectButton = (Button) view.findViewById(R.id.crime_call);
        mSolvedCheckBox = (CheckBox) view.findViewById(R.id.crime_solved);
        mPhotoButton = (ImageButton) view.findViewById(R.id.crime_camera);
        mPhotoView = (ImageView) view.findViewById(R.id.crime_photo);

        mTitleField.setText(mCrime.getTitle());
        //Add event handlers to the Title-EditText:
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                Log.d(TAG, "mTitleField - onTextChanged");
                mCrime.setTitle(s.toString());
                updateCrime();
            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.d(TAG, "afterTextChanged(..)");
            }
        });

        //Set buttons text:
        mDateButton.setText(StringUtils.getStringDate(mCrime)); //display a formatted date
        mTimeButton.setText(StringUtils.getStringTime(mCrime)); //display a formatted time
        if (mCrime.getSuspect() != null) {
            mSuspectButton.setText(mCrime.getSuspect());//display suspect name if exists
        }
        mReportButton.setText(R.string.crime_report_text);

        //Clicking the checkbox should show an instance of a DatePickerFragment:
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                    create & show dialog; adding it to this fragment's hosting activity.
                    NOTE: Set this CrimeFragment instance as the parent
                            of the dialog fragment instance.
                 */
                DatePickerFragment dialog = DatePickerFragment.newInstance(mCrime.getDate());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);
                dialog.show(getFragmentManager(), DIALOG_DATE);
            }
        });

        mTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                    create & show dialog; adding it to this fragment's hosting activity.
                    NOTE: Set this CrimeFragment instance as the parent
                            of the dialog fragment instance.
                 */
                TimePickerFragment dialog = TimePickerFragment.newInstance(mCrime.getDate());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_TIME);
                dialog.show(getFragmentManager(), DIALOG_TIME);
            }
        });

        //IMPORTANT: android.provider package to access internal device data! (contacts, pics, etc)

        //Get an intent for choosing a contact:
        final Intent pickContactIntent = getImplicitIntentPickContact();

        //Check if intent's Action can be resolved on device:
        if (canResolveIntentActivity(pickContactIntent)) {

            //Action CANNOT be resolved, disable option of choosing a contact
            mSuspectButton.setEnabled(false);

        } else {

            //Action CAN be resolved, construct button event handler:
            mSuspectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*

                    NOTE: We're expecting a result, so we call startActivityForResult(..)
                 */

                    startActivityForResult(pickContactIntent, REQUEST_CONTACT);
                }
            });

        }//if-else(can choose contact)

        mReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                    Create implicit intent to sent a crime report. Action is ACTION_SEND,
                    data MIME type is text/plain. No data source or categories.
                 */
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("text/plain"); //MIME type
//                i.putExtra(Intent.EXTRA_TEXT, getCrimeReport()); //extras; what to send
//                i.putExtra(Intent.EXTRA_SUBJECT,
//                        getString(R.string.crime_report_subject));
//                i = Intent.createChooser(i, //create chooser; ask for app. choice each time
//                        getString(R.string.crime_report_text));

                /*
                    Use a ShareCompat.IntentBuilder to generate an implicit/explicit intent for
                     ACTIVITY_SEND or ACTIVITY_SEND_MULTIPLE! (Will get same intent as above)

                    NOTE: The static nested class IntentBuilder uses a Fluent Interface,
                           methods invoked on it return its own type instead of void.
                           (Allows for chained invocation)
                 */
                ShareCompat.IntentBuilder iiBuilder = ShareCompat.IntentBuilder.from(getActivity());
                iiBuilder
                        .setType("text/plain") //required to find appropriate activities
                        .setSubject(getString(R.string.crime_report_subject))
                        .setText(getCrimeReport())
                        .setChooserTitle(R.string.crime_report_text); // if using chooser:
                Intent i = iiBuilder.createChooserIntent();
//                Intent i = iiBuilder.getIntent(); //.setPackage()
                                /*
                    NOTE: Use setPackage on the retrieved intent to make it an EXPLICIT intent:
                            .getIntent().setPackage( package path of explicit activity to start );

                          Use .createChooserIntent() to get an Intent that shows a chooser!
                 */

                startActivity(i);
            }
        });

        //Get an intent to call a number
        final Intent phoneNumberIntent = getImplicitIntentCallNumber();

        //Check to see if intent's Action can be resolved on the device:
        if (canResolveIntentActivity(phoneNumberIntent)) {

            //Action CANNOT be resolved, disable option of calling the suspect
            mCallSuspectButton.setEnabled(false);

        } else {

            //Action CAN be resolved, construct button event handler:
            mCallSuspectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Do we have a suspect to call?
                    if (mCrime.getSuspect() == null) {

                        //No named suspect
                        Toast.makeText(getActivity(), "No Named Suspect!", Toast.LENGTH_SHORT).show();

                    } else {

                        // First have to check for:android.permission.READ_CONTACTS
                        int permissionCheck =
                                checkIfHavePermission(Manifest.permission.READ_CONTACTS);

                        /****** Have to declare permissions we're requesting in the manifest! ******/
                        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {

                            //If need to give explanation why permission is required, see link above.

                            /*
                                Request permission; and wait for callback.
                                For requestingPermission in a fragment, see:
                                    http://stackoverflow.com/a/33080682
                            */
                            requestPermissions(
                                    new String[]{Manifest.permission.READ_CONTACTS}, //permission(s)
                                    PERMISSION_REQ_READ_CONTACTS_FOR_CALL //request code to handle callback
                            );

                        } else {

                            //have permission; perform action that requires it:
                            Intent makeCallIntent = addNumberDataToIntent(
                                    phoneNumberIntent, //requires permission
                                    lookUpPhoneNumber(mCrime.getSuspect()) //suspect non-null
                            );
                            startActivity(makeCallIntent);

                        }//if-else(have permission)

                    }//if-else(no named suspect)
                }
            });

        }//if-else(can contact be called)

        //Get an implicit intent to take a picture;
        final Intent takePhotoIntent = getImplicitIntentTakePhoto();

        // Check this device can resolve intent's action, or if we have a valid path to store photo:
        if (canResolveIntentActivity(takePhotoIntent) || mPhotoFilePath == null) {

            //Action CANNOT be resolved OR no valid storage path for photo,
            // disable option of taking a picture
            mPhotoButton.setEnabled(false);

        } else {

            //Action CAN be resolved, create button listener and handle it:
            mPhotoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*
                        Intent's action can be resolved, and have valid storage path for photo
                        Just have to check permission now:
                      */

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        Log.d(TAG, "API > 18 - Don't need READ_EXTERNAL_STORAGE permission");
                        //Don't need READ_EXTERNAL_STORAGE permission on API > 18

                        //Add primary external storage URI to photo storage to take full-res picture
                        Intent takeFullResPhotoIntent = putStorageExtraUriToIntent(
                                takePhotoIntent,
                                mPhotoFilePath
                        );
                        startActivityForResult(takeFullResPhotoIntent, REQUEST_PHOTO);

                    } else {
                        Log.d(TAG, "API <= 18 - Need READ_EXTERNAL_STORAGE permission");
                        //Need READ_EXTERNAL_STORAGE permission on API <= 18

                        //Check if we have permission to read from External Storage (to save picture)
                        int permissionCheck =
                                checkIfHavePermission(Manifest.permission.READ_EXTERNAL_STORAGE);

                        /****** Have to declare permissions we're requesting in the manifest! ******/
                        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                            //We don't have permission, ask for it:
                            Log.d(TAG, "Don't have permission API <= 18");

                            requestPermissions(
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},//permission to request
                                    PERMISSION_REQ_READ_EXTERNAL_STORAGE_FOR_PHOTO//request code for handling result
                            );

                        } else {
                            //We have permission, perform action that requires it
                            Log.d(TAG, "have permission API <= 18");

                            //Add primary external storage URI to photo storage to take full-res picture
                            Intent takeFullResPhotoIntent = putStorageExtraUriToIntent(
                                    takePhotoIntent,
                                    mPhotoFilePath
                            );
                            startActivityForResult(takeFullResPhotoIntent, REQUEST_PHOTO);

                        }//if-else(have READ_EXTERNAL_STORAGE permission)?

                    }//if-else(API > 18?)

                }
            });

        }//if-else(can take picture)

        mSolvedCheckBox.setChecked(mCrime.isSolved());
        //Update the crime's solved boolean in accordance to the solved checkbox's state
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
                updateCrime();
            }
        });

        //Add a ViewTreeObserver to our ImageView to set a picture once dimensions are available:
        ViewTreeObserver mPhotoViewTreeObserver = mPhotoView.getViewTreeObserver();
        mPhotoViewTreeObserver.addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (mPhotoView.getDrawable() == null) {
                            // no image displayed
                            Log.d(TAG, "onGlobalLayout - no image displayed");
                            updatePhotoView();

                        } else {
                            // image already displayed
                            Log.d(TAG, "onGlobalLayout - image displayed");

                        }//if-else(photo already displayed?)

                    }//onGlobalLayout
                });

        //When user clicks thumbnail, show a larger version in a DialogFragment:
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoFragment photoDialogFragment = PhotoFragment.newInstance(mPhotoFilePath);
                photoDialogFragment.show(getFragmentManager(), DIALOG_PHOTO);
            }
        });

        return view;

    } //onCreateView(..)

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

                /*
            Ensure hosting activity implements this Fragment's required interface
            for proper functionality. (Displaying CrimeFragment)
         */

        try {
            mCallbacksObserver = (CrimeFragment.Callbacks) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    "ERROR: " + context.getClass().getSimpleName()
                            + " must implement " + TAG + ".Callbacks interface!");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

        mCallbacksObserver = null;
    }

    /**
     * Retrieves phone number of a suspect from the contacts database.
     * Shouldn't be excessively called, it searches a database - slow.
     *
     * @param user contact whose number to retrieve
     */
    private String lookUpPhoneNumber(String user) {
        Log.d(TAG, "lookUpPhoneNumber()");


        //We're interested in querying the columns containing user names & phone numbers
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, //cursor column 0
                ContactsContract.CommonDataKinds.Phone.NUMBER //cursor column 1
        };

        //We only want row belonging to suspect; we filter by user-name:
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " = ?";

        String[] selectionArgs = new String[]{
                user //we're searching by suspect's name
        };

        //for more info of below: http://stackoverflow.com/a/19223016
        Cursor cursor = getActivity().getContentResolver()
                .query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, //phone-book
                        projection, //columns to get; null for all (inefficient)
                        selection, //rows to get, a WHERE-clause; null for all
                        selectionArgs, //selection-args
                        null //orderBy
                );

        String phoneNumber = null;

        if (cursor == null) {
            Log.e(TAG, "cursor null for phone lookup query");
        } else {

            if (cursor.getCount() == 0) {
                Log.e(TAG, "no results in cursor");
                cursor.close();
            } else {

                //If we have results, retrieve suspect's phone number:
                cursor.moveToFirst();
                phoneNumber = cursor.getString(1); //1st index is user's number from projection
                cursor.close();

                Log.d(TAG, "number for " + user + " is: " + phoneNumber);

            }//if-else(have query results?)

        }//if-else(query results null?)

        return phoneNumber;

    }//lookUpPhoneNumber

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        Log.d(TAG, "onRequestPermissionsResult()");
        switch (requestCode) {
            case PERMISSION_REQ_READ_CONTACTS_FOR_CALL:

                if (grantResults.length > 0 && //not cancelled/interrupted (length=0 in that case)
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) { //granted permission
                    Log.d(TAG, "READ_CONTACTS Permission GRANTED");

                    /* If-Check explained:
                        If grantResults is empty (length = 0), permission request was CANCELLED
                        See if permission is granted via PackageManager constant at grantResults[0]
                     */

                    //permission granted; perform action that required it
                    Intent phoneNumberIntent = getImplicitIntentCallNumber();
                    Intent makeCallIntent = addNumberDataToIntent(
                            phoneNumberIntent, //requires permission
                            lookUpPhoneNumber(mCrime.getSuspect()) //suspect non-null
                    );
                    startActivity(makeCallIntent);

                } else {
                    Log.d(TAG, "READ_CONTACTS Permission DENIED");

                    //permission denied; show toast
                    Toast.makeText(
                            getActivity(),
                            "Need Contact permission to make a call!",
                            Toast.LENGTH_SHORT)
                            .show();

                }//if-else(permission granted?)
                break;

            case PERMISSION_REQ_READ_EXTERNAL_STORAGE_FOR_PHOTO:

                //Check if user didn't cancel permission request and that it was granted:
                if ((grantResults.length > 0) &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "READ_EXTERNAL_STORAGE Permission GRANTED");

                    //Permission granted, perform task that required it
                    Intent takePhotoIntent = getImplicitIntentTakePhoto();

                    //Add primary external storage URI to photo storage to take full-res picture
                    Intent takeFullResPhotoIntent = putStorageExtraUriToIntent(
                            takePhotoIntent,
                            mPhotoFilePath
                    );
                    startActivityForResult(takeFullResPhotoIntent, REQUEST_PHOTO);

                } else {
                    Log.d(TAG, "READ_EXTERNAL_STORAGE Permission DENIED");

                    //Permission denied, show toast
                    Toast.makeText(
                            getActivity(),
                            "Need Storage permission to save picture!",
                            Toast.LENGTH_SHORT)
                            .show();
                }//if-else(permission granted?)

                break;

            default:
                Log.e(TAG, "default in onRequestPermissionsResult()");
                break;

        }//switch(requestCode)

    }//onRequestPermissionsResult

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater); //convention

        /*
            Inflate a menu, and configure items if needed (get reference through menu.findItem(id))
         */
        inflater.inflate(R.menu.fragment_crime, menu);

    }//onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_item_del_crime:
                Log.d(TAG, "onOptionsItemSelected() - delCrime");
                //Delete the crime displayed by this CrimeFragment from the CrimeLab & finish().
                CrimeLab.get(getActivity()).deleteCrime(mCrime);
                getActivity().finish();
                return true;

            default:
                Log.d(TAG, "onOptionsItemSelected() - default");
                return super.onOptionsItemSelected(item);

        }//switch(item.getItemId())

    }//onOptionsItemSelected

//    @Override
//    public void onPause() {
//        super.onPause();
//        Log.d(TAG, "onPause()");
//        /*
//            NOTE: mCrime is altered within CrimeFragment, so when it
//                   goes out of focus/the foreground we want to push changes to CrimeLab.
//
//            We do this in onPause()
//         */
//
//        //FIXME: again, this should only be called if mCrimes was actually changed.
//        CrimeLab.get(getActivity()).updateCrime(mCrime);
//
//    }//onPause

    /**
     * When the crime displayed by this instance of CrimeFragment is changed,
     * need to update the information in the SQL database managed by the singleton,
     * AND notify observers that the crime has changed.
     */
    private void updateCrime() {
        CrimeLab.get(getActivity()).updateCrime(mCrime);
        mCallbacksObserver.onCrimeUpdated(mCrime);
    }//updateCrime

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
            Used to take appropriate action once any child fragments started by this fragment
               complete and return results.

               NOTE: onActivityResult of fragments is always called, whether the fragment
                      started an activity or showed a dialog (through parent-child relationship)
                     Therefore, we can use the same code to handle different situations!

         */
        if (data == null) {
            Log.e(TAG, "Intent data is null in onActivityResult");
//            return;
        }

        switch (requestCode) {

            case REQUEST_DATE:
                switch (resultCode) {

                    case Activity.RESULT_OK:
                        Log.d(TAG, "REQUEST_DATE/RESULT_OK");
                        Calendar updatedDate = DatePickerFragment.getDateFromData(data);
                        mCrime.setDate(updatedDate);
                        mDateButton.setText(StringUtils.getStringDate(mCrime));
                        updateCrime();
                        break;

                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "REQUEST_DATE/RESULT_CANCELED");
                        break;

                    default:
                        Log.e(TAG, "Invalid result-code in onActivityResult:REQUEST_DATE");
                        break;

                }//switch(resultCode)
                break;

            case REQUEST_TIME:

                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "REQUEST_TIME/RESULT_OK");
                        Calendar updatedTime = TimePickerFragment.getTimeFromData(data);
                        mCrime.setTime(updatedTime);
                        mTimeButton.setText(StringUtils.getStringTime(mCrime));
                        updateCrime();
                        break;

                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "REQUEST_TIME/RESULT_CANCELED");
                        break;

                    default:
                        Log.e(TAG, "Invalid result-code in onActivityResult:REQUEST_TIME");
                        break;

                }//switch(resultCode)
                break;

            case REQUEST_CONTACT:

                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "REQUEST_CONTACT/RESULT_OK");

                        if (data == null) {
                            throw new IllegalStateException("Data is null");
                        }

                        /* TODO:
                            IMPORTANT NOTE TO SELF:
                            Read javadoc on Intent.ACTIONS to see what they take and return.
                         */

                        /*
                            URI points to data's location in the DataBase
                            (that we're interested in).

                            It may be multiple rows, we don't know at this point.
                              This is a "starting point of rows containing our data".
                              (like how array pointer points to first index of array)
                         */
                        Uri contactUri = data.getData();

                        /*
                            Specify which columns you want your query to return values for.
                         */
                        String[] projection = new String[]{
                                ContactsContract.Contacts.DISPLAY_NAME //select column: name
                        };

                        //Perform query to get a cursor over our data-of-interest
                        Cursor cursor = getActivity().getContentResolver()
                                .query(
                                        contactUri,
                                        projection, //columns we're interested in
                                        null, //rows to return, use =?; null to return all (=1 here)
                                        null, //selectionArgs if use "=?"; as String[]
                                        null //sortOrder
                                );

                        if (cursor == null) {
                            Log.e(TAG, "null cursor");
                            return;
                        } else {

                            //Now handle data from cursor, and close it afterwards:
                            try {

                                if (cursor.getCount() == 0) {

                                    //if we got no results, exit
                                    return;

                                } else {

                                    cursor.moveToFirst();

                                    /*
                                        We pull out the first column of data from our cursor.
                                        These is only one column in the cursor, as we only
                                        specified a single arg for the WHERE-clause
                                            - the contact's DISPLAY_NAME.
                                     */
                                    String suspect = cursor.getString(0);
                                    Log.d(TAG, "suspect's name = " + suspect);

                                    //update crime's suspect and display it:
                                    mCrime.setSuspect(suspect);
                                    mSuspectButton.setText(suspect);
                                    updateCrime();
                                    /*
                                        Call to updateCrime() not needed here as
                                        the suspect field is not shown in the list displaying
                                        the model collection, but it's good to place here.
                                     */

                                }//if-else (cursor.getCount() == 0)

                            } finally {
                                //Close cursor once handled
                                cursor.close();
                            }//try/finally

                        }//if(cursor == null)
                        break;

                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "REQUEST_CONTACT/RESULT_CANCELED");
                        break;

                    default:
                        Log.e(TAG, "Invalid result-code in onActivityResult:REQUEST_CONTACT");
                        break;

                }//switch(resultCode)
                break;

            case REQUEST_PHOTO:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        /*
                            NOTE: If Intent with MediaStore.ACTION_IMAGE_CAPTURE has
                             an extra set containing URI of fileStoragePath, camera activity
                             will NOT return an intent data with a pic thumbnail, it will
                             save a full-resolution picture directly to the
                             primary external storage location specified by the URI
                         */
                        Log.d(TAG, "REQUEST_PHOTO/RESULT_OK");
                        updatePhotoView();
                        updateCrime();
                        /*
                            Call to updateCrime() not needed here as
                            the thumbnail is not shown in the list displaying
                            the model collection, but it's good to place here.
                         */
                        break;

                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "REQUEST_PHOTO/RESULT_CANCELED");
                        break;

                    default:
                        Log.e(TAG, "Invalid result-code in onActivityResult:REQUEST_PHOTO");
                        break;

                }//switch(resultCode)
                break;

            default:
                Log.e(TAG, "Invalid request-code in onActivityResult");
                break;

        }//switch(requestCode)

    }//onActivityResult

    /**
     * FactoryMethod to instantiate an instance of CrimeFragment, and <br>
     * attach a bundle containing the specified parameters as arguments.<br>
     * NOTE: Arguments should NOT be changed after fragment is instantiated.
     *
     * @param crimeID: An argument representing the unique ID of a model object this <br>
     *                 fragment instance will handle.
     * @return A new instance of CrimeFragment with appropriate arguments set.
     */
    public static CrimeFragment newInstance(UUID crimeID) {

        CrimeFragment frag = new CrimeFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME_ID, crimeID);
        frag.setArguments(args);
        return frag;

    }//newInstance

    /**
     * Used to create a report for this crime.
     *
     * @return A string containing a crime report based on the Crime shown by this instance of
     * CrimeFragment.
     */
    private String getCrimeReport() {

        String report, solvedString, suspect;

        solvedString = mCrime.isSolved() ?
                getString(R.string.crime_report_solved) :
                getString(R.string.crime_report_unsolved);

        StringBuilder dateString = new StringBuilder();
        dateString
                .append(StringUtils.getStringDate(mCrime))
                .append(" at ")
                .append(StringUtils.getStringTime(mCrime));

        suspect = mCrime.getSuspect();
        suspect = suspect == null ?
                getString(R.string.crime_report_no_suspect) :
                getString(R.string.crime_report_suspect, suspect);

        report = getString(
                R.string.crime_report, //has 4 format specifiers, needs 4 args:
                mCrime.getTitle(), //title
                dateString, //date
                solvedString, //solved
                suspect //suspect
        );

        return report;

    }//getCrimeReport()

    /**
     * Loads a bitmap of the crime into an ImageView.
     */
    private void updatePhotoView() {

        if (mPhotoFilePath == null || !mPhotoFilePath.exists()) {
            //File path to photo DOES NOT exist or is not valid
            Log.d(TAG, "updatePhotoView() - No valid photo to display");

            mPhotoView.setImageBitmap(null);
            //fixme: this should display a placeholder image if one isn't available

        } else {
            //File path to photo exists and is valid
            Log.d(TAG, "updatePhotoView() - Displaying photo");

//            Bitmap imgToDisplay = PictureUtils
//                    .getScaledBitmap(mPhotoFilePath, getActivity());

            //Called after layout pass happened, uses valid Width & Height dimensions.
            Bitmap imgToDisplay = PictureUtils
                    .getScaledBitmap(
                            mPhotoFilePath,
                            mPhotoView.getWidth(),
                            mPhotoView.getHeight()
                    );

            mPhotoView.setImageBitmap(imgToDisplay);
//            mPhotoView.setBackgroundColor(Color.parseColor("#00000000")); //FIXME

        }//if-else(valid path to photo exists)

    }//updatePhotoView

    /**
     * Convenience method to create an implicit intent to take a picture. <br>
     * Action = MediaStore.ACTION_IMAGE_CAPTURE <br>
     *
     * @return Intent containing appropriate settings
     */
    private Intent getImplicitIntentTakePhoto() {

        Intent i = new Intent();
        i.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        //NOTE: extra DATA for storage URI only added when necessary

        // Temporary category my device doesn't have to test if device can resolve intent's action
//        i.addCategory(Intent.CATEGORY_CAR_DOCK);

        return i;

    }//getImplicitIntentTakePhoto

    /**
     * Method to create an implicit intent to select a contact. <br>
     * Action = Intent.ACTION_PICK <br>
     *
     * @return Intent containing appropriate settings
     */
    private Intent getImplicitIntentPickContact() {

        /*
            Create implicit intent to pick a contact. Action is ACTION_PICK,
            and data location is within a contacts database:
            ContactsContract.Contact.CONTENT_URI.

            Define implicitIntent to select a contact here; used in two places below.
            NOTE: implicitly set .addCategory(Intent.CATEGORY_DEFAULT) by default!
         */

        Intent i = new Intent(Intent.ACTION_PICK);
        i.setData(ContactsContract.Contacts.CONTENT_URI); //can also set in constructor

        // Temporary category my device doesn't have to test if device can resolve intent's action
//        i.addCategory(Intent.CATEGORY_CAR_DOCK);

        return i;

    }//getImplicitIntentPickContact

    /**
     * Method to create an implicit intent to call a phone number. <br>
     * Action = Intent.ACTION_DIAL <br>
     *
     * @return Intent containing appropriate settings
     */
    private Intent getImplicitIntentCallNumber() {

        Intent i = new Intent();
        i.setAction(Intent.ACTION_DIAL); // NOTE: Intent.ACTION_CALL automatically dials number

        // Temporary category my device doesn't have to test if device can resolve intent's action
//        i.addCategory(Intent.CATEGORY_CAR_DOCK);

        return i;

    }//getImplicitIntentCallNumber

    /**
     * Used to set an intent's data with the URI of a phone number
     *
     * @param intent      Intent with action: Intent.ACTION_DIAL
     * @param phoneNumber Phone number to set as intent's data
     * @return Intent with phone number set as extra
     */
    private Intent addNumberDataToIntent(Intent intent, String phoneNumber) {

        Uri numberUri = Uri.parse("tel:" + phoneNumber); //parsing a number in form "tel:5196167912"
        return intent
                .setData(numberUri);

    }//addNumberDataToIntent

    /**
     * Used to set an intent's extra with URI to storage location
     *
     * @param intent      Intent with action: MediaStore.ACTION_IMAGE_CAPTURE
     * @param storagePath File representing path to storage location
     * @return Intent with storagePath set as extra
     */
    private Intent putStorageExtraUriToIntent(Intent intent, File storagePath) {

        Uri storageUri = Uri.fromFile(storagePath);
        return intent.putExtra(
                MediaStore.EXTRA_OUTPUT, storageUri
        );

    }//putStorageExtraUriToIntent

    /**
     * Method to see if this device can resolve an intent's action. <br>
     * NOTE: Only checks against activity's intent-filters declaring the DEFAULT category.
     *
     * @param i Implicit intent whose action to resolve
     * @return boolean representing if action can be resolved (T/F)
     */
    private boolean canResolveIntentActivity(Intent i) {

        PackageManager pm = getActivity().getPackageManager();
        return pm.resolveActivity(i, PackageManager.MATCH_DEFAULT_ONLY) == null;

    }//canResolveIntentActivity

    /**
     * Method to check if this application has a specified permission.
     *
     * @param permissionToCheck The permission to check.
     * @return int indicating if application has permission. Check with {@link PackageManager}
     */
    private int checkIfHavePermission(String permissionToCheck) {

        /*
            More info here: https://developer.android.com/training/permissions/requesting.html
            NOTE: use ContextCOMPAT for backwards compatibility.
         */

        return ContextCompat.checkSelfPermission(
                getActivity(),
                permissionToCheck
        );

    }//checkIfHavePermission

    /**
     * Required interface for hosting activities to handle this fragment's tasks.
     */
    public interface Callbacks {

        //Ensures hosting activity will take care of this action, regardless of concrete activity.

        /**
         * Hosting activity needs to pass along an event when this crime was updated.
         * Fragment displaying a list of crimes should update its list.
         *
         * @param crime Crime that was updated.
         */
        void onCrimeUpdated(Crime crime);

        //NOTE: This is called when user updates/changes the crime this Fragment is displaying.

    }//Callbacks

}//CrimeFragment
