package burean.szabi.criminalintent.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

import burean.szabi.criminalintent.R;
import burean.szabi.criminalintent.controller.fragment.CrimeFragment;
import burean.szabi.criminalintent.model.Crime;
import burean.szabi.criminalintent.utils.SingleFragmentActivity;
import burean.szabi.criminalintent.controller.fragment.CrimeListFragment;

/**
 * Concrete activity that hosts a listFragment instance <br>
 * to display a list of crimes.
 * <p>
 * LAUNCHER-ACTIVITY
 */
public class CrimeListActivity extends SingleFragmentActivity
        implements CrimeListFragment.Callbacks, CrimeFragment.Callbacks{

    private static final String TAG = CrimeListActivity.class.getSimpleName();

    @Override
    protected Fragment createFragment() {
        return CrimeListFragment.newInstance();
    }

    @Override
    protected int getLayoutResId() {

        /*
            Use a resource alias that points to an appropriate layout to inflate
            based on the device's minimum screen size: 600. (ie: phone or tablet)
         */

        return R.layout.activity_masterdetail;
    }

    @Override
    public void onCrimeSelected(Crime crime) {

        /*
            -> If running on a Phone, use an intent to start CrimePagerActivity and
            pass the received Crime reference.
            -> While running on a Tablet, commit a fragment transaction that REPLACES
            the CrimeFragment on the layout's detail container with a new one.
         */

        //Check if current layout contains a second frag. container: R.id.detail_fragment_container
        if(findViewById(R.id.detail_fragment_container) != null){
            //contains container; running on tablet
            Log.d(TAG, "onCrimeSelected - Tablet");
            Fragment detailFragment = CrimeFragment.newInstance(crime.getId());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();

        } else {
            //doesn't contain a container; running on phone
            Log.d(TAG, "onCrimeSelected - Phone");
            Intent i = CrimePagerActivity.newIntent(this, crime.getId());
            startActivity(i);

        }//if-else(is device a phone or tablet?)

    }

    @Override
    public void onCrimeUpdated(Crime crime) {
        /*
            Used to reload CrimeListFragment's displayed list of crimes. (collection of models)
         */

        //Get a reference to the list fragment: (BY CONTAINER-ID)
        Fragment listFrag =  getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);

        if(listFrag instanceof CrimeListFragment){
            //update list fragment's model collection
            ((CrimeListFragment) listFrag).updateUI();
        }

    }
}
