package burean.szabi.criminalintent.controller.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import java.io.File;

import burean.szabi.criminalintent.R;
import burean.szabi.criminalintent.utils.PictureUtils;

/**
 * Dialog fragment to display an image to the user after clicking on a thumbnail.
 */
public class PhotoFragment extends DialogFragment {

    private static final String TAG = PhotoFragment.class.getSimpleName();
    private static final String ARG_PHOTO_FILE_PATH = "mPhotoFilePath";

    private File mPhotoFilePath;
    private ImageView mPhotoDisplay;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Save a reference to file path of photo to display
        String path = getArguments().getString(ARG_PHOTO_FILE_PATH);
        if (path == null) {
            //TODO; show empty dialog with message
            throw new IllegalArgumentException("ERROR: Missing argument: ARG_PHOTO");
        } else {
            //FIXME: path will never be null, but picture will be empty; need to account for it
            mPhotoFilePath = new File(path);
        } //if-else (is path null)

    }//onCreate

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Log.d(TAG, "onCreateDialog");

        View dialogRootView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_photo, null);

        mPhotoDisplay = (ImageView) dialogRootView.findViewById(R.id.dialog_photo_display);

        /*
            Set a GlobalLayoutListener to the ImageView to load image once layout pass
            happens, otherwise ImageView.GetWidth/GetHeight() will be zero and produce
            divide by zero exception in PictureUtils.getScalingFactor.
         */
        ViewTreeObserver mPhotoDisplayViewTreeObserver =
                mPhotoDisplay.getViewTreeObserver();

        mPhotoDisplayViewTreeObserver.addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        if (mPhotoDisplay.getDrawable() == null) {
                            //If we're not already display a photo, display it
                            updateDialogPhoto(
                                    mPhotoDisplay.getWidth(),
                                    mPhotoDisplay.getHeight()
                            );
                        }//if-else(picture already displayed?)

                    }//onGlobalLayout
                }
        );

        //Dialog has no title or buttons, only populates an ImageView
        return new AlertDialog.Builder(getActivity())
                .setView(dialogRootView)
                .create();

    }//onCreateDialog

    /**
     * Helper method to retrieve a bitmap from a file path, and display
     * it within a display element after it's been appropriately scaled to the
     * display's bounds.
     *
     * @param width  Display element's bounding width
     * @param height Display element's bounding height
     */
    private void updateDialogPhoto(int width, int height) {

        Bitmap bitmapToDisplay = PictureUtils.getScaledBitmap(
                mPhotoFilePath,
                width,
                height
        );
        mPhotoDisplay.setImageBitmap(bitmapToDisplay);

    }//updateDialogPhoto

    public static PhotoFragment newInstance(File photoSrc) {

        Bundle args = new Bundle();
        args.putString(ARG_PHOTO_FILE_PATH, photoSrc.toString());

        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(args);
        return fragment;

    }//newInstance
}
