package burean.szabi.criminalintent.controller.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;

import burean.szabi.criminalintent.R;

/**
 * Wraps an AlertDialog that displays a DatePicker widget. <br>
 * User may select a new date, and this fragment will return it to its parent fragment. 
 */
public class DatePickerFragment extends DialogFragment {

    /*
        Wrapping subclasses of Dialog within a DialogFragment allows the Dialog to
         be managed by a FragmentManager, giving it more options for presenting it.

        NOTE: Also handles Device Configuration changes; Dialog will be re-created
               appropriately along with fragment.
     */

    private static final String TAG = DatePickerFragment.class.getSimpleName();
    private static final String ARG_DATE = "date";
    private static final String INTENT_KEY_EXTRA_DATE =
            DatePickerFragment.class.getName() + "date";

    private DatePicker mDatePicker;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Log.d(TAG, "onCreateDialog()");
        /*
            Dialog is created in this method via an instance of AlertDialog.Builder
             (AlertDialog's static-nested-class called Builder)

            Also used to wire up any widgets for the dialog (if it uses any via a Layout)
         */

        //Inflate a view for this Dialog; to be attached via AlertDialog.Builder.
        @SuppressLint("InflateParams") View dialogRootView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_date, null);

        //Set initial date for the date picker from Calendar obj.
        Calendar crimeInitDate = (Calendar) getArguments().getSerializable(ARG_DATE);
        mDatePicker = (DatePicker) dialogRootView.findViewById(R.id.dialog_date_date_picker);
        mDatePicker.init(
                crimeInitDate.get(Calendar.YEAR),
                crimeInitDate.get(Calendar.MONTH),
                crimeInitDate.get(Calendar.DAY_OF_MONTH),
                null //onDateChangedListener
        );

        //Crate and return a dialog (AlertDialog)
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.date_picker_title)
                .setView(dialogRootView) //can also use setView(int id) - from API 21
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Positive-Button click handler; retrieve selected date and call setResult()
                                Calendar chosenDate = Calendar.getInstance();
                                chosenDate.set(Calendar.YEAR, mDatePicker.getYear()); //year
                                chosenDate.set(Calendar.MONTH, mDatePicker.getMonth()); //month
                                chosenDate.set(Calendar.DAY_OF_MONTH, mDatePicker.getDayOfMonth()); //day-of-month
                                sendResult(chosenDate);
                            }
                        })
                .create();

    }//onCreateDialog

    /**
     * Used by an instance of DatePickerFragment to return data to this fragment's <br>
     * target. Data returned as an extra, packaged into an intent and passed back <br>
     * via an explicit call to the target's onActivityResult(...) method.
     * <p>
     *
     * @param result The new date to be sent back to this fragment's parent.
     */
    private void sendResult(Calendar result) {
        Log.d(TAG, "sendResult()");
        /*
            Notice how adding this still keeps the fragment modular.
            It doesn't depend on its target fragment. It can simply have one, and
            if it does, it sends a Calendar object to it as the result.
         */

        //Get this fragment's target & request code (via hosting activity's FragmentManager)
        Fragment targetFragment = getTargetFragment();
        if (targetFragment == null) {
            //should have a null check to make sure a target fragment exists.
            throw new IllegalStateException("DatePickerFragment has no target");
            /*
                NOTE: if targetFragment is null, it could also mean that this dialogFragment
                       is HOSTED within an activity instead of being shown as a dialog.
                      In this case, we can call setResult(..) on the hosting activity.
             */
        }
        int targetRequestCode = getTargetRequestCode();

        //Create an intent and package it with resulting data.
        Intent resultIntent = new Intent();
        resultIntent.putExtra(INTENT_KEY_EXTRA_DATE, result);

        //Pass result back to target fragment:
        targetFragment.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                resultIntent
        );

    }//sendResult

    /**
     * Returns the calendar object containing updated DATE information, as packaged <br>
     * into the intent and returned by an instance of DatePickerFragment.
     *
     * @param data Intent containing packaged Calendar object.
     * @return Extracted Calendar object.
     */
    public static Calendar getDateFromData(Intent data) {

        Calendar toReturnCalendar = (Calendar) data.getSerializableExtra(INTENT_KEY_EXTRA_DATE);
        if (toReturnCalendar == null) {
            throw new IllegalArgumentException("No Calendar object to extract from arguments");
        } else {
            return toReturnCalendar;
        }

    }//getDateFromData

    /**
     * FactoryMethod to instantiate an instance of DatePickerFragment <br>
     * with appropriate parameters to be set as arguments to the <br>
     * new instance. <br>
     * NOTE: Arguments should NOT be changed after fragment is instantiated.
     *
     * @param calendar Calendar object representing the initial date of the DatePicker.
     * @return A new instance of DatePickerFragment with appropriate arguments set.
     */
    public static DatePickerFragment newInstance(Calendar calendar) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, calendar);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;

    }//newInstance

}
