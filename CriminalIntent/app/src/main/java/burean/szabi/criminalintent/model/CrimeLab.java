package burean.szabi.criminalintent.model;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import burean.szabi.criminalintent.database.CrimeBaseHelper;
import burean.szabi.criminalintent.database.CrimeCursorWrapper;
import burean.szabi.criminalintent.database.CrimeDbSchema.CrimeTable;

/**
 * A singleton that stores a collection of crimes in a SQLite Database, and provides a <br>
 * global access point to them.
 */
public class CrimeLab {

    private static final String TAG = CrimeLab.class.getSimpleName();

    @SuppressLint("StaticFieldLeak")
    private static transient CrimeLab sCrimeLab; //lazy-instantiation singleton instance

    //    private List<Crime> mCrimes;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    private CrimeLab(Context context) {
        Log.d(TAG, "CrimeLab()");
        /*
            Application context has same lifetime as statics; safe to store.

             For more info, see:
              https://forums.bignerdranch.com
              /t/do-not-place-android-context-classes-in-static-fields-warning/10797
         */
        mContext = context.getApplicationContext();
        mDatabase = new CrimeBaseHelper(mContext).getWritableDatabase();
//        mCrimes = new ArrayList<>();
    }//CrimeLab

    /**
     * Get a list of model objects saved by the application.
     *
     * @return List of model objects.
     */
    public List<Crime> getCrimes() {
//        return mCrimes;
//        Log.d(TAG, "getCrimes()");

        /*
            We still need a list of model objects because it's used in the RecyclerView.Adapter
            FIXME: Obviously this is bad, we create a new list each time we query the dataBase.
         */
        List<Crime> crimes = new ArrayList<>();

        //Get a cursor containing all crimes from the database:
        CrimeCursorWrapper cursor = queryCrimes(null, null);
        cursor.moveToFirst(); //make cursor point to the first item (if any)

        try {
            //Loop through each result in the cursor and extract model object to add into list:
            while (!cursor.isAfterLast()) {
                //while we have results to loop through
                crimes.add(cursor.getCrime());
                cursor.moveToNext(); // move cursor position
            }
        } finally {
            cursor.close();
        }

        return crimes;

    }//getCrimes()

    /**
     * Returns a specific crime object.
     *
     * @param uuid ID of crime object to retrieve
     * @return Crime object with matching UUID
     */
    public Crime getCrime(UUID uuid) {
//        Log.d(TAG, "getCrime()");

//        for (Crime crime : mCrimes) {
//            if (crime.getId().equals(uuid)) {
//                return crime;
//            }
//        } //foreach

        //Query should only return one result here, as each model object has a unique UUID.
        //Get a wrapped cursor containing results
        CrimeCursorWrapper cursor = queryCrimes(
                CrimeTable.Cols.UUID + " = ?", //filter; Where clause
                new String[]{uuid.toString()} //args for Where clause
        );

        try {
            if (cursor.getCount() == 0) {
                //No items found for specified Where clause and args
                return null;
            } else {
                //Move wrapped cursor to first (and only) item
                cursor.moveToFirst();
                return cursor.getCrime();
            }
        } finally {
            //NOTE: finally block runs even if try has a return. It ALWAYS runs.
            cursor.close();
        }

    }//getCrime

    public static CrimeLab get(Context context) {
//        Log.d(TAG, "get()");

        if (sCrimeLab == null) {
            synchronized (CrimeLab.class) { //lock singleton instantiation to one thread
                if (sCrimeLab == null) {
                    //check again in-case another thread got lock first
                    sCrimeLab = new CrimeLab(context);
                }
            }//synchronized
        }
        return sCrimeLab;

    }

    /**
     * Used to add a new crime.
     *
     * @param c Crime object to add.
     */
    public void addCrime(Crime c) {
//        mCrimes.add(c);
        Log.d(TAG, "addCrime(" + c.getTitle() + ")");

        mDatabase.insert(
                CrimeTable.NAME, //table we want to insert into
                null, //can specify data to pass in as null if contentValues is null; rarely used.
                getContentValuesFrom(c) //contentValues (data) we're inserting into the table
        );

    }//addCrime

    /**
     * Used to delete an existing crime.
     *
     * @param c Crime object to delete
     */
    public void deleteCrime(Crime c) {
        Log.d(TAG, "deleteCrime(" + c.getTitle() + ")");

        mDatabase.delete(
                CrimeTable.NAME,
                CrimeTable.Cols.UUID + " = ?",
                new String[]{c.getId().toString()} //identify model to delete by it's UUID
        );

    }//deleteCrime

    /**
     * Used to update an existing crime.
     *
     * @param crime Crime object to update.
     */
    public void updateCrime(Crime crime) {
        Log.d(TAG, "updateCrime(" + crime.getTitle() + ")");

        /*
            NOTE: We use ? in our where clause because the where clause can contain SQL code
                   that will get executed if we don't use '?' . This can alter our Query or even
                   change the database -> "SQL injection attack"
                  Using ? will treat it as a string value, not code.
         */

        mDatabase.update(
                CrimeTable.NAME, // table with row we're updating
                getContentValuesFrom(crime), // content values we want to update rows with
                CrimeTable.Cols.UUID + " = ?", // where clause; which rows we want to update
                new String[]{crime.getId().toString()} // specify values for where clause
        );

    }//updateCrime

    /**
     * Use to package a model object's (Crime) state into a ContentValues object. <br>
     * NOTE: This function deconstructs a model object so it may be stored in a Database.
     *
     * @param crime The model object to package.
     * @return ContentValues object containing model object state
     * @see CrimeCursorWrapper#getCrime() CrimeCursorWrapper.getCrime() - for reconstructing <br>
     * a model object from saved database information.
     */
    private ContentValues getContentValuesFrom(Crime crime) {
        Log.d(TAG, "getContentValuesFrom()");

        /*
            NOTE: keys have to correspond to column titles we're inserting to or updating!
                   Use them directly from schema!

            Do not explicitly specify the _id column; created and updated automatically.
         */
        ContentValues cv = new ContentValues();

        cv.put(CrimeTable.Cols.TITLE, crime.getTitle());
        cv.put(CrimeTable.Cols.UUID, crime.getId().toString());
        cv.put(CrimeTable.Cols.DATE, crime.getDate().getTimeInMillis());
        cv.put(CrimeTable.Cols.SOLVED, crime.isSolved() ? 1 : 0); //True = 1, False = 0
        cv.put(CrimeTable.Cols.SUSPECT, crime.getSuspect());

        return cv;
    }

    /**
     * Used to retrieve a cursor containing results from the SQLiteDataBase. <br>
     * Caller's responsibility to free cursor once done with it, <br>
     * and to move it to the first item (if any).
     *
     * @param whereClause Filter declaring which rows to search; SQL WHERE statement
     * @param whereArgs   Search values for the whereClause.
     * @return Cursor containing results that match criteria.
     */
    private CrimeCursorWrapper queryCrimes(String whereClause, String[] whereArgs) {
//        Log.d(TAG, "queryCrimes()");

        //Caller's responsibility to free cursor after done using it.
        @SuppressLint("Recycle") Cursor cursor = mDatabase.query(
                CrimeTable.NAME, //database table we want to search/query
                null, //table columns we want to retrieve; null = retrieve all columns
                whereClause, //null to search all columns
                whereArgs, //null if searching all columns
                null, //groupBy
                null, //having
                null //orderBy
        );

        return new CrimeCursorWrapper(cursor);

    }//queryCrimes

    /**
     * Retrieve location where photos generated by this application
     * will be stored.
     *
     * @param crime Model object whose photos to store
     * @return A <b>File</b> object pointing to a storage location.
     */
    public File getPhotoFilePath(Crime crime) {
        Log.d(TAG, "getPhotoFilePath()");

        /*
            Get absolute path to primary external storage device (internal or SD card)
            where this application can save files it owns. (folder is specific to application)
            -> These files are internal to applications; not visible to user like media files!
         */
        File externalFilesDir = mContext.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES // type of files directory to return,
                // -> see .getExternalFilesDir javadoc for param info
        );

         /* PRIOR TO GETTING PATH:

            State of the primary external storage path may be checked via:
            Environment.getExternalStorageState(File path)
         */

        //Check that primary external storage is available:
        if (externalFilesDir == null) {
            Log.e(TAG, "Primary external storage NOT AVAILABLE");
            return null;
        } else {

            //parent pathname (File) + child pathname (String)
            File pathToReturn = new File(externalFilesDir, crime.getPhotoFileName());
            Log.d(TAG, crime.getTitle() + "'s storage path: " + pathToReturn);
            return pathToReturn;

        }//if-else(primaryStorageLocation available)

    }//getPhotoFilePath

}
