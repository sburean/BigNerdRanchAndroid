package burean.szabi.criminalintent.controller.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import java.util.Calendar;

import burean.szabi.criminalintent.R;

/**
 * Wraps an AlertDialog that displays a TimePicker widget. <br>
 * User may select a new time-stamp, and this fragment will return it to its parent fragment.
 * FIXME: Issue with TimePicker widget in landscape mode; see http://stackoverflow.com/questions/35494234/android-timepicker-not-displayed-well-on-landscape-mode
 */
public class TimePickerFragment extends DialogFragment {

    private static final String TAG = TimePickerFragment.class.getSimpleName();
    private static final String ARG_TIME = "time";
    private static final String INTENT_EXTRA_KEY_TIME =
            TimePickerFragment.class.getName() + "time";

    private TimePicker mTimePicker;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog()");

        @SuppressLint("InflateParams") View dialogRootView = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_time, null);

        Calendar crimeInitTime = (Calendar) getArguments().getSerializable(ARG_TIME);
        if (crimeInitTime == null) {
            throw new IllegalArgumentException("ERROR: Missing argument: ARG_TIME");
        }
        mTimePicker = (TimePicker) dialogRootView.findViewById(R.id.dialog_time_time_picker);
        int initHour = crimeInitTime.get(Calendar.HOUR_OF_DAY);
        int initMin = crimeInitTime.get(Calendar.MINUTE);

        // FIXME: The SDK check and different code usage should be encapsulated; see: http://stackoverflow.com/q/33122147
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            //API 23 and above; use new TimePicker methods:
            mTimePicker.setHour(initHour);
            mTimePicker.setMinute(initMin);
        } else {

            //API 22 and below, use old (depreciated) TimePicker methods:
            mTimePicker.setCurrentHour(initHour);
            mTimePicker.setCurrentMinute(initMin);
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.time_picker_title)
                .setView(dialogRootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Returns selected time to parent activity.
                        Calendar chosenTime = Calendar.getInstance();
                        int updatedHour;
                        int updatedMinute;

                        // FIXME: The SDK check and different code usage should be encapsulated; see: http://stackoverflow.com/q/33122147
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                            //API 23 and above; use new TimePicker methods:
                            updatedHour = mTimePicker.getHour();
                            updatedMinute = mTimePicker.getMinute();

                        } else {

                            //API 22 and below, use old (depreciated) TimePicker methods:
                            updatedHour = mTimePicker.getCurrentHour();
                            updatedMinute = mTimePicker.getCurrentMinute();

                        }
                        chosenTime.set(Calendar.HOUR_OF_DAY, updatedHour);
                        chosenTime.set(Calendar.MINUTE, updatedMinute);
                        sendResult(chosenTime);
                    }
                })
                .create();

    }//onCreateDialog

    /**
     * Used by an instance of TimePickerFragment to return data to this fragment's <br>
     * target. Data returned as an extra, packaged into an intent and passed back <br>
     * via an explicit call to the target's onActivityResult(...) method.
     * <p>
     *
     * @param result The new date to be sent back to this fragment's parent.
     */
    private void sendResult(Calendar result) {

        Fragment targetFragment = getTargetFragment();
        if (targetFragment == null) {
            //should have a null check to make sure a target fragment exists.
            throw new IllegalStateException("TimePickerFragment has no target");
             /*
                NOTE: if targetFragment is null, it could also mean that this dialogFragment
                       is HOSTED within an activity instead of being shown as a dialog.
                      In this case, we can call setResult(..) on the hosting activity.
             */
        }
        int targetRequestCode = getTargetRequestCode();

        Intent resultIntent = new Intent();
        resultIntent.putExtra(INTENT_EXTRA_KEY_TIME, result);

        targetFragment.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                resultIntent);

    }//sendResult()

    /**
     * Returns the calendar object containing updated TIME information, as packaged <br>
     * into the intent and returned by an instance of TimePickerFragment.
     *
     * @param data Intent containing packaged Calendar object.
     * @return Extracted Calendar object.
     */
    public static Calendar getTimeFromData(Intent data) {
        Calendar toReturnCalendar = (Calendar) data.getSerializableExtra(INTENT_EXTRA_KEY_TIME);

        if (toReturnCalendar == null) {
            throw new IllegalArgumentException("No Calendar object to extract from arguments");
        } else {
            return toReturnCalendar;
        }

    }//getTimeFromData

    /**
     * FactoryMethod to instantiate an instance of TimePickerFragment <br>
     * with appropriate parameters to be set as arguments to the <br>
     * new instance. <br>
     * NOTE: Arguments should NOT be changed after fragment is instantiated.
     *
     * @param calendar Calendar object representing the initial date of the TimePicker.
     * @return A new instance of TimePickerFragment with appropriate arguments set.
     */
    public static TimePickerFragment newInstance(Calendar calendar) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_TIME, calendar);

        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
