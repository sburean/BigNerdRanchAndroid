package burean.szabi.criminalintent.utils;

import java.util.Locale;

import burean.szabi.criminalintent.model.Crime;

/**
 * Class containing methods to retrieve formatted strings based on a Crime's date.
 */
public class StringUtils {

    private static final String TAG = StringUtils.class.getSimpleName();

    /**
     * Returns a string containing a date formatted as such: <br>
     * [ <b>Thursday, Mar 02, 2017 - 9:09 PM</b> ]
     *
     * @param crime Crime containing a date to format, as a Calendar object
     * @return Formatted string in format described
     */
    public static String getStringFullDate(Crime crime) {
        return String.format(
                Locale.CANADA,
                "%tA, %<tb %<td, %<tY - %<tl:%<tM %<Tp",
                crime.getDate());
    }//getStringFullDate

    /**
     * Returns a string containing time formatted as such: <br>
     * [ <b>9:09 PM</b> ]
     *
     * @param crime Crime containing time to format, as a Calendar object
     * @return Formatted string in format described
     */
    public static String getStringTime(Crime crime){
        return String.format(Locale.CANADA,
                "%tl:%<tM %<Tp", // format: "hh:mm AM/PM" - no leading zeros
                crime.getDate()
        );
    }//getStringTime

    /**
     * Returns a string containing a date formatted as such: <br>
     * [ <b>Sunday, Jan 15, 2017</b> ]
     *
     * @param crime Crime containing a date to format, as a Calendar object
     * @return Formatted string in format described
     */
    public static String getStringDate(Crime crime){
        return String.format(
                Locale.CANADA,
                "%tA, %<tb %<td, %<tY", // format: "Sunday, Jan 15, 2017"
                crime.getDate()
        );
    }//getStringDate

}
