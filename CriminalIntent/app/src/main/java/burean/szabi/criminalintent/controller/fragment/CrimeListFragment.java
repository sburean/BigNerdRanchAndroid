package burean.szabi.criminalintent.controller.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import burean.szabi.criminalintent.R;
import burean.szabi.criminalintent.controller.activity.CrimePagerActivity;
import burean.szabi.criminalintent.model.Crime;
import burean.szabi.criminalintent.model.CrimeLab;
import burean.szabi.criminalintent.utils.StringUtils;

/**
 * Contains a RecyclerView that displays a list of crimes to the user.
 * <p>
 * Currently uses the build-in ToolBar via the application's theme. <br>
 * May alternative define separate ToolBar from scratch through a layout.
 */
public class CrimeListFragment extends Fragment {

    private static final String TAG = CrimeListFragment.class.getSimpleName();
    private static final String STATE_SUBTITLE_VISIBLE = "mSubtitleVisible";

    private RecyclerView mCrimeRecyclerView;
    private CrimeAdapter mAdapter;
    private Button mPlaceHolderButton;
    private TextView mPlaceHolderTextView;
    private boolean mSubtitleVisible; //flag; control menu item title text
    private Callbacks mCallbacksObserver; //to handle displaying a CrimeFragment; on phone or tablet

    /* TODO: important notes about fragment lifecycle:

        DO NOT add arguments to a fragment instance after it's been created. If we want to
        save state, need to use savedInstanceState mechanism.
        ( We can access saved instance state via Bundle in:
            onCreate(), onCreateView(), or onActivityCreated() )

        HOWEVER, if we return to an activity that was placed on the system's back-stack, the
        fragments it hosts are retained as well -> instance variables are saved.

        We only need to restore state via bundle if fragments are destroyed, such as
        when we perform fragment transactions and do not explicitly ask to have them placed
        on the activity's back-stack that's managed by the fragmentManager.
        (Same deal in FragmentStatePagerManager
     */

    /*
        NOTE: Since we're accessing our collection of model objects from a
              global access point (singleton), whenever we change anything on
              model, it is saved and everything that access it has an updated version.

              [ NOTE: Obviously only saved in current JVM instance. Need to persist if we
                        want long term solution. ]

              HOWEVER, in order for views to display an updated version, they
              must be refreshed/reloaded as appropriate.
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
            This fragment gets created first since it's hosted by the launcher activity.
            Indicate to the fragmentManager that hosted fragments should get calls to
                onCreateOptionsMenu(..) by calling:
                supportFragment.setHasOptionsMenu(Boolean hasMenu):
         */

        setHasOptionsMenu(true); //allow hosted fragments to create menus on the ToolBar.
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView(..)");

        View rootView = inflater.inflate(R.layout.fragment_crime_list, container, false);

        mCrimeRecyclerView = (RecyclerView) rootView.findViewById(R.id.crime_recycler_view);
        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mPlaceHolderTextView = (TextView) rootView.findViewById(R.id.placeholder_no_crime_text);
        mPlaceHolderButton = (Button) rootView.findViewById(R.id.placeholder_add_crime_button);
        mPlaceHolderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                    Create a new Crime, add it to the CrimeLab, and then start a new
                    instance of CrimePagerActivity to edit the new crime.
                 */
                Crime newCrime = new Crime();
                CrimeLab.get(getActivity()).addCrime(newCrime);
                mCallbacksObserver.onCrimeSelected(newCrime);
            }
        });

        if (savedInstanceState != null) {
            mSubtitleVisible = savedInstanceState.getBoolean(STATE_SUBTITLE_VISIBLE);
        }

        updateUI();

        return rootView;

    }// onCreateView

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater); //convention, base Fragment method does nothing.

        /*
            NOTE: This is also called when AppCompatActivity.invalidateOptionsMenu() is called!
            Therefore this method is also used to set up menu items as needed (titles, etc..)
         */

        //Inflate a menu defined in the appropriate Menu XML file.
        inflater.inflate(R.menu.fragment_crime_list, menu);

        /*
            Use a flag to keep track if the subtitle is shown or not.
            Used to present appropriate menu title & functionality, and
            persist through device config. change if needed.
         */
        MenuItem item = menu.findItem(R.id.menu_item_show_subtitle);
        if (mSubtitleVisible) {
            item.setTitle(R.string.hide_subtitle);
        } else {
            item.setTitle(R.string.show_subtitle);
        }

    }//onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        /*
            Handle menu item clicks here.
            Parameter indicates user's selection from ALL possible menus.
            So use the .getItemId() method to determine which <item> was pressed.
         */

        switch (item.getItemId()) {

            case R.id.menu_item_new_crime:
                Log.d(TAG, "onOptionsItemSelected() - newCrime");

                /*
                    Create a new Crime, add it to the CrimeLab, and then start a new
                    instance of CrimePagerActivity to edit the new crime.
                 */

                Crime newCrime = new Crime();
                CrimeLab.get(getActivity()).addCrime(newCrime);
                updateUI(); // required b/c list stays visible to user; update soon as crime created
                mCallbacksObserver.onCrimeSelected(newCrime);
                return true; //indicate menu click was handled.

            case R.id.menu_item_show_subtitle:
                Log.d(TAG, "onOptionsItemSelected() - showSubtitle");
                mSubtitleVisible = !mSubtitleVisible; //toggle subtitle visibility flag.
                getActivity().invalidateOptionsMenu(); //trigger a re-creation of menu items.
                updateSubtitle();
                return true;

            default:
                Log.d(TAG, "onOptionsItemSelected() - default");
                return super.onOptionsItemSelected(item);

        }//switch(item.getItemId())

    }//onOptionsItemSelected

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "onResume()");
        updateUI();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /*
            Called when fragment is destroyed, and only needed when fragment has to
            restore state when it's being re-created.

            Saved bundle accessible in onCreate(), onCreateView(), onActivityCreated().
         */

        Log.d(TAG, "onSaveInstanceState()");
        outState.putBoolean(STATE_SUBTITLE_VISIBLE, mSubtitleVisible);
    }

    /**
     * Updates the UI shown by this fragment.
     */
    public void updateUI() {

        List<Crime> crimes = CrimeLab.get(getActivity()).getCrimes();

        if (mAdapter == null) {
            Log.d(TAG, "updateUI() - creating Adapter");
            //Creates a RecyclerView.Adapter<VH> and sets it to the RecyclerView.
            mAdapter = new CrimeAdapter(crimes);
            mCrimeRecyclerView.setAdapter(mAdapter);
        } else {
            Log.d(TAG, "updateUI() - notifyDataSetChanged()");
            /*
                Refresh RecyclerView's list by notifying the adapter. Since our model collection
                now comes from a database, we no longer have a global list of models.
                We now need to set the adapter's data source with a new list (via setter)
                and then call notifyDataSetChanged().
             */
            mAdapter.setCrimes(crimes);
            mAdapter.notifyDataSetChanged();
        }

        // handle the case when there are no crimes to display:
        if (mAdapter.getItemCount() == 0) {
            //No crimes to display, show placeholders and hide the RecyclerView
            mPlaceHolderTextView.setVisibility(View.VISIBLE);
            mPlaceHolderButton.setVisibility(View.VISIBLE);
            mCrimeRecyclerView.setVisibility(View.INVISIBLE);
        } else {
            //Have crimes to display, hide placeholders and show RecyclerView
            mPlaceHolderTextView.setVisibility(View.INVISIBLE);
            mPlaceHolderButton.setVisibility(View.INVISIBLE);
            mCrimeRecyclerView.setVisibility(View.VISIBLE);
        }//if-else(Crimes to display?)

        updateSubtitle(); //update Toolbar's subtitle with any new info.

    } //updateUI

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        /*
            Ensure hosting activity implements this Fragment's required interface
            for proper functionality. (Displaying CrimeFragment)
         */

        try {
            mCallbacksObserver = (Callbacks) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    "ERROR: " + context.getClass().getSimpleName()
                            + " must implement " + TAG + ".Callbacks interface!");
        }

    }//onAttach

    @Override
    public void onDetach() {
        super.onDetach();

        //Reset hosting activity for callback
        mCallbacksObserver = null;
    }

    /**
     * ViewHolder for wrapping a rootView and caching its child views. <br>
     * The rootView will be used to display crime object details to the user. <p>
     * <p>
     * Also implements an onClickListener & handles click-events for the entire row.
     */
    private class CrimeHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitleTextView, mDateTextView;
        private CheckBox mSolvedCheckBox;
        private Crime mCrime;

        /*
            ViewHolder should store a reference to the model object it's using to
            populate its cached views.
         */

        public CrimeHolder(View itemView) {

            super(itemView); //itemView is usually an inflated layout view; a rootView!
            itemView.setOnClickListener(this); //respond to whole row clicks
            cacheViews();

        }//CrimeHolder

        /**
         * Store references to each View (widget) within a rootView, <br>
         * and the rootView itself as "itemView"
         */
        private void cacheViews() {

            if (itemView instanceof android.view.ViewGroup) {
//                Log.d(TAG, "instanceof android.view.ViewGroup");
                mTitleTextView = (TextView)
                        itemView.findViewById(R.id.list_item_crime_title_text_view);
                mDateTextView = (TextView)
                        itemView.findViewById(R.id.list_item_crime_date_text_view);
                mSolvedCheckBox = (CheckBox)
                        itemView.findViewById(R.id.list_item_crime_solved_check_box);
            } else {
                throw new IllegalStateException("You must pass an inflated Layout view into CrimeHolder!");
            }

        }//cacheViews

        /**
         * Encapsulate binding data fields to cached views of a rootView
         *
         * @param crime: data object to be bound.
         */
        private void bindCrime(Crime crime) {

            mCrime = crime;
            mTitleTextView.setText(mCrime.getTitle());
            mDateTextView.setText(StringUtils.getStringFullDate(mCrime));
            mSolvedCheckBox.setChecked(mCrime.isSolved());

        }//bindCrime

        @Override
        public void onClick(View v) {
            /*
                Launches a CrimePagerActivity, passing it the ID of the
                appropriate Crime Object to handle.
             */

            //Delegate task of display a CrimeFragment to the hosting activity
            mCallbacksObserver.onCrimeSelected(mCrime);

        } //onClick

    }//CrimeHolder

    /**
     * An adapter used for creating ViewHolders (of CrimeHolder), and <br>
     * binding model data to its cached views for <br>
     * a RecyclerView to display.
     */
    private class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> {

        private List<Crime> mCrimes;

        public CrimeAdapter(List<Crime> crimes) {
            //Constructor takes in a collection of elements.

            this.mCrimes = crimes;
        }

        @Override
        public CrimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            /*
                RecyclerView calls this whenever it needs a ViewHolder.
                It uses one ViewHolder PER View it creates; however many
                views it can display at a time; depends on its height.

                Responsible for creating and returning a ViewHolder.
             */

            //NOTE: We need to get a LayoutInflater to inflate a rootView (& pass to ViewHolder!)
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View rootView = inflater.inflate(R.layout.list_item_crime, parent, false);
            return new CrimeHolder(rootView);

        } //onCreateViewHolder

        @Override
        public void onBindViewHolder(CrimeHolder holder, int position) {
//            Log.d(TAG, "onBindViewHolder(..): " + holder.toString());

            /*
                RecyclerView calls this whenever it needs
                a view populated with data.
                This is usually called MUCH more frequently than
                onCreateViewHolder because Views are recycled.

                ( and since we cached references to views of a rootView in our
                  viewHolders, we don't have to call view.findViewById(..) here.
                  This greatly increases efficiency, as it's a slow operation
                  in comparison to accessing a view through a stored reference. )

                Responsible for binding views in the ViewHolder with
                    data at position specified.
             */

            //Get data object at position:
            Crime data = mCrimes.get(position);

            //Bind data object to ViewHolder's views:
            holder.bindCrime(data);

        } //onBindViewHolder

        @Override
        public int getItemCount() {
            /*
                Tells the RecyclerView how many elements in
                the collection to possibly display.
                (User may not scroll through whole list)
             */

            return mCrimes.size();
        }

        public void setCrimes(List<Crime> crimes) {
            mCrimes = crimes;
        }
    } //CrimeAdapter

    /**
     * Used to indicate the total number of crimes within the CrimeLab, <br>
     * on the ToolBar.
     */
    private void updateSubtitle() {

        int numCrimes = CrimeLab.get(getActivity()).getCrimes().size();

        /*
            The getString(..)/getQuantityString(..) method returns a string defined in
            the res/values/strings.xml file, and ACCEPTS PARAMETERS to substitute
            in the string format specifiers defined in the resource.
            ( This is how we can place strings into res/values/strings.xml with
               varying values! )
         */

        String subtitle = getString(R.string.subtitle_format, numCrimes);

        /*
            NOTE: getResources().getQuantityString is used to correctly pluralize a string
                   resource. Simply pass in the actual quantity and the correct resource is
                   chosen.
         */
        String pluralSubtitle = getResources().getQuantityString(
                R.plurals.subtitle_plural,
                numCrimes,
                numCrimes
        );

        if (!mSubtitleVisible) {
            pluralSubtitle = null; // if subtitle is set to hide, reset it.
        }

        //get a reference to the ToolBar and set the Subtitle.
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            activity.getSupportActionBar().setSubtitle(pluralSubtitle);
        } else {
            throw new NullPointerException("ToolBar is null - updateSubtitle");
        }

    }//updateSubtitle

    /**
     * FactoryMethod to instantiate an instance of CrimeListFragment. <br>
     * NOTE: Arguments should NOT be changed after fragment is instantiated
     *
     * @return A new instance of CrimeFragment.
     */
    public static CrimeListFragment newInstance() {
        Log.d(TAG, "newInstance()");

        Bundle args = new Bundle();
        //add args here as needed
        CrimeListFragment fragment = new CrimeListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Required interface for hosting activities to handle this fragment's tasks.
     */
    public interface Callbacks {

        //Ensures hosting activity will take care of this action, regardless of concrete activity.

        /**
         * Hosting activity needs to display a CrimeFragment based on chosen crime.
         * @param crime Crime to display in a CrimeFragment.
         */
        void onCrimeSelected(Crime crime);

        //NOTE: This is called when user selected an existing crime, or creates a new crime.

    }//Callbacks

}
