package burean.szabi.criminalintent.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.UUID;

import burean.szabi.criminalintent.model.Crime;
import burean.szabi.criminalintent.utils.SingleFragmentActivity;
import burean.szabi.criminalintent.controller.fragment.CrimeFragment;

/**
 * Concrete activity that hosts a single CrimeFragment instance.
 * NOTE: Not used in project anymore as of chapter 11. Kept here for reference.
 */
public class CrimeActivity extends SingleFragmentActivity
                            implements CrimeFragment.Callbacks {

    private static final String TAG = CrimeActivity.class.getSimpleName();
    private static final String INTENT_KEY_EXTRA_CRIME_ID =
            CrimeActivity.class.getName() + "crime_id";

    @Override
    protected Fragment createFragment() {
        /*
            Overriding abstract method of SingleFragmentActivity
            to instantiate the fragment this activity will host.

            Passes in model object's ID that the fragment will
            retrieve and display it's data.
         */

        UUID crimeID = (UUID) getIntent().getSerializableExtra(INTENT_KEY_EXTRA_CRIME_ID);
        return CrimeFragment.newInstance(crimeID);
    }

    /**
     * Used by any context that wants to launch this activity to get an appropriate intent.
     * @param context: Source context that will launch an instance of this activity.
     * @param crimeID: Parameter expected by new instance of this activity.
     * @return Intent to start a new instance of this activity with expected parameters.
     */
    public static Intent newIntent(Context context, UUID crimeID){

        /*
            This intent has to be retrieved by an instance of CrimeActivity;
            Will pass the contained crimeID as an ARG to any hosted CrimeFragments.
         */

        Intent i = new Intent(context, CrimeActivity.class);
        i.putExtra(INTENT_KEY_EXTRA_CRIME_ID, crimeID);
        return i;

    }//newIntent

    @Override
    public void onCrimeUpdated(Crime crime) {
        Log.d(TAG, "onCrimeUpdated - TODO");
    }
}
