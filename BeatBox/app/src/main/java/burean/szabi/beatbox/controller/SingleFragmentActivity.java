package burean.szabi.beatbox.controller;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import burean.szabi.beatbox.R;

/**
 * An abstract class encapsulating code for hosting a single fragment.
 * It inflates a generic layout that contains a FrameLayout.
 * <p>
 * It is a Template for generating fragment-hosting activities.
 * Contains abstract method for instantiating fragment to host; to be implemented by subclasses.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId()); //Hook; specify layout to inflate

        //populate the FrameLayout with a fragment:
        FragmentManager fm = getSupportFragmentManager();

        //first get reference to fragment via CONTAINER-ID:
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) { //if fragment doesn't already exist.
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }

    }//onCreate()

    /**
     * Abstract method to instantiate a fragment to host.
     * Protected such that only subclasses may override it.
     */
    protected abstract Fragment createFragment();

    /**
     * Hook that specifies the layout this Activity will inflate.
     * Subclasses should override to specify custom layouts as needed.
     *
     * @return Resource ID of layout to be inflated.
     */
    @LayoutRes
    protected int getLayoutResId() {
        /*
            Note: THe @LayoutRes annotation tells Android Studio that any
             implementation of this method should return a layout resource ID.
         */
        return R.layout.activity_fragment;
    }//getLayoutResId

}//SingleFragmentActivity
