package burean.szabi.beatbox.model;

import android.util.Log;

import java.io.File;

/**
 * Used to keep track of a sound file name, display name, etc..
 */
public class Sound {

    private static final String TAG = Sound.class.getSimpleName();

    private String mAssetPath;
    private String mName;
    private Integer mSoundId;
    /*
        - Each sound loaded into a SoundPool has an integer ID to uniquely identify it;
        retrieved when a sound is loaded into the SoundPool.
        - Using Integer instead of int, we can represent the state where a
        Sound has no value with a value of null.
        - Used to easily re-play a Sound, unload it, etc..
     */

    public Sound(String assetPath) {
        this.mAssetPath = assetPath;

        //Now trim down file extension to store filename
        String[] components = assetPath.split(File.separator);
        String fileName = components[components.length - 1]; //filename is at last index
        mName = fileName.replace(".wav", "");
    }

    public int getSoundId() {
        return mSoundId;
    }

    public void setSoundId(Integer soundId) {
        mSoundId = soundId;
    }

    public String getAssetPath() {
        return mAssetPath;
    }

    public String getName() {
        return mName;
    }

}//Sound
