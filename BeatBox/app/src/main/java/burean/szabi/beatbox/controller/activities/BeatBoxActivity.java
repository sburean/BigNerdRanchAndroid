package burean.szabi.beatbox.controller.activities;

import android.support.v4.app.Fragment;

import burean.szabi.beatbox.controller.SingleFragmentActivity;
import burean.szabi.beatbox.controller.fragments.BeatBoxFragment;

public class BeatBoxActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return BeatBoxFragment.newInstance();
    }//createFragment

}//BeatBoxActivity
