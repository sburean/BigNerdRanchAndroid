package burean.szabi.beatbox.model;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Used for asset management, and general control.
 */
public class BeatBox {

    private static final String TAG = BeatBox.class.getSimpleName();
    private static final String SOUNDS_FOLDER = "sample_sounds";
    private static final int MAX_SOUNDS = 5;

    private AssetManager mAssets;
    private List<Sound> mSounds;
    private SoundPool mSoundPool; //can't persist

    public BeatBox(Context context) {
        this.mAssets = context.getAssets();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //In API21+, can use SoundPool.Builder
            mSoundPool = new SoundPool.Builder()
                    .setMaxStreams(5)
                    .setAudioAttributes( //AudioAttributes supersede AudioManager stream types.
                            new AudioAttributes.Builder()
                                    .setUsage(AudioAttributes.USAGE_MEDIA)
                                    .build()
                    )
                    .build();
        } else {
            //In API21 or lower, use depreciated constructor for compatibility
            mSoundPool = new SoundPool(MAX_SOUNDS, AudioManager.STREAM_MUSIC, 0);
        }//if-else(API21 or greater?)

        loadSounds();

    }//BeatBox

    public List<Sound> getSounds() {
        return mSounds;
    }//getSounds

    /**
     * Loads all sound files within this application's assets into a SoundPool for future use.
     */
    private void loadSounds() {

        mSounds = new ArrayList<>();
        String[] soundNames;
        try {

            //lists the filenames in the provided folder path.
            soundNames = mAssets.list(SOUNDS_FOLDER);
            Log.i(TAG, "Found " + soundNames.length + " sounds");

            for (String fileName : soundNames) {

                try {
                    String assetPath = SOUNDS_FOLDER + File.separator + fileName;
                    Sound sound = new Sound(assetPath);
                    mSounds.add(sound); //store sound into model collection
                    load(sound); //load into SoundPool
                } catch (IOException ioe) {
                    Log.e(TAG, "Could not load sound " + fileName, ioe);
                }

            }//for-each(fileName in list of assets)

        } catch (IOException ioe) {
            Log.e(TAG, "Could not list assets", ioe);
        }

    }//loadSounds

    /**
     * Play a loaded sound file from the SoundPool.
     *
     * @param sound A loaded sound to play.
     */
    public void play(Sound sound) {

        Integer soundId = sound.getSoundId();
        if (soundId != null) {
            //See .play(..) method's javadoc for param info
            mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
        } else {
            //Can happen if a sound failed to load into the SoundPool for whatever reason
            Log.e(TAG, "Cannot play a sound; null parameter");
        }//if-else(is soundId null?)

    }//play

    /**
     * Used to load a sound into the SoundPool for later playback.
     *
     * @param sound Sound object to load.
     * @throws IOException If an error occurs while reading asset resources.
     */
    private void load(Sound sound) throws IOException {
        AssetFileDescriptor afd = mAssets.openFd(sound.getAssetPath());
        int soundId = mSoundPool.load(afd, 1); //returns loaded sound's Id.
        Log.i(TAG, "loaded sound: " + sound.getName());
        sound.setSoundId(soundId);//save ID
    }//load

    /**
     * Used to clean up resources used by our instance of the
     * SoundPool once we're done with it.
     */
    public void release(){
        mSoundPool.release();
        mSoundPool = null;
    }

}//BeatBox
