package burean.szabi.beatbox.controller.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import burean.szabi.beatbox.model.Sound;
import burean.szabi.beatbox.model.BeatBox;
import burean.szabi.beatbox.R;

/**
 * do once complete
 */
public class BeatBoxFragment extends Fragment {

    public static BeatBoxFragment newInstance() {

        Bundle args = new Bundle();
        //add args here
        BeatBoxFragment fragment = new BeatBoxFragment();
        fragment.setArguments(args);
        return fragment;

    }//newInstance

    private BeatBox mBeatBox;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        //instantiates a SoundPool object internally; have to clean up onDestroy()
        mBeatBox = new BeatBox(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_beat_box, container, false);

        //Get a reference to the RecyclerView, add a GridLayoutManager to it. (3 columns in grid)
        RecyclerView recyclerView = (RecyclerView) rootView
                .findViewById(R.id.fragment_beat_box_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setAdapter(new SoundAdapter(mBeatBox.getSounds()));

        return rootView;

    }//onCreateView

    /**
     * ViewHolder for a RecyclerView; wraps a rootView and caches its child views.
     * Implements any event handler that the whole list view should respond to.
     */
    private class SoundHolder extends RecyclerView.ViewHolder
                                implements View.OnClickListener {

        private Button mButton;
        private Sound mSound;

        public SoundHolder(View itemView) {
            super(itemView);
            cacheViews();
        }//SoundHolder-Constructor

        /**
         * Method to encapsulate caching all child views contained in a rootView.
         */
        private void cacheViews() {

            mButton = (Button) itemView.findViewById(R.id.list_item_sound_button);
            mButton.setOnClickListener(this);

        }//cacheViews

        public void bindSound(Sound sound){

            mSound = sound; //First store a reference to our sound
            mButton.setText(mSound.getName());

        }//bindSound

        @Override
        public void onClick(View view) {
            //When the user clicks on a RecyclerView list item (button), play that sound:
            mBeatBox.play(mSound);
        }

    }//SoundHolder

    /**
     * Adapter for a RecyclerView; contains a collection of models. Acts as the bridge between
     * the RecyclerView and the collection. Will create ViewHolders for each item being displayed
     * in the RecyclerView and cache views appropriately.
     */
    private class SoundAdapter extends RecyclerView.Adapter<SoundHolder> {

        List<Sound> mSounds;

        public SoundAdapter(List<Sound> sounds) {
            mSounds = sounds;
        }

        @Override
        public SoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View viewHolderRootView = LayoutInflater.from(getActivity())
                    .inflate(R.layout.list_item_sound, parent, false);
            return new SoundHolder(viewHolderRootView);

        }//onCreateViewHolder

        @Override
        public void onBindViewHolder(SoundHolder holder, int position) {

            Sound sound = mSounds.get(position);
            holder.bindSound(sound);

        }//onBindViewHolder

        @Override
        public int getItemCount() {
            return mSounds.size();
        }//getItemCount
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBeatBox.release(); //release SoundPool resources
    }
}//BeatBoxFragment
