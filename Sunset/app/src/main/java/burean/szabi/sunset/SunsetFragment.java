package burean.szabi.sunset;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;

public class SunsetFragment extends Fragment {

    private static final String TAG = SunsetFragment.class.getSimpleName();
    private static final int ANIM_SECONDS = 3;
    private static final int ONE_SECOND_IN_MILLIS = 1000;
    private static final long ANIM_DURATION = ANIM_SECONDS * ONE_SECOND_IN_MILLIS;

    private View mSunView;
    private View mSunRefView;
    private View mSkyView;
    private View mSceneView;
    private int mBlueSkyColor, mSunsetSkyColor, mNightSkyColor;
    private int mBrightSunColor, mBrighterSunColor;
    private boolean mIsSunset; // used to keep track of which animation to play
    private float mSunStartY, mSunRefStartY, mSunRefEndY;
    private AnimatorSet mSunSetAnimatorSet, mSunRiseAnimatorSet;

    public static SunsetFragment newInstance() {

        Bundle args = new Bundle();
        //args here
        SunsetFragment fragment = new SunsetFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragRootView = inflater.inflate(R.layout.fragment_sunset, container, false);

        mSceneView = fragRootView;

        ViewTreeObserver vto = mSceneView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //On layout pass, populate animationSets (they require view measurements)
                createSunsetAnimation();
                createSunriseAnimation();
                startPulsatingSunAnimation();

//                Log.d(TAG, "layout pass");

                //then remove this listener
                mSceneView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        mSunSetAnimatorSet = new AnimatorSet();
        mSunRiseAnimatorSet = new AnimatorSet();

        mSceneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mIsSunset) {
                    //sunset animation needed

                    if (!isAnimationRunning()) {
                        toggleNextAnimation();
//                        createSunsetAnimation();
                        mSunSetAnimatorSet.start();
                    } else {
                        Log.e(TAG, "ERROR: Already playing SunRise animation!"); //this is called on repeated clicks of runRISE animation - flag toggled before animation starts
                    }//if-else mSunSetAnimatorSet running?

                } else {
                    //sunrise animation needed

                    if (!isAnimationRunning()) {
                        toggleNextAnimation();
//                        createSunriseAnimation();
                        mSunRiseAnimatorSet.start();
                    } else {
                        Log.e(TAG, "ERROR: Already playing SunSet animation!"); //this is called on repeated clicks of runSET animation - flag toggled before animation starts
                    }//if-else mSunRiseAnimatorSet running?

                }//if-else sunrise/sunset animation?

            }//onCLick
        });

        mSunView = fragRootView.findViewById(R.id.sun);
        mSunRefView = fragRootView.findViewById(R.id.sun_reflection);
        mSkyView = fragRootView.findViewById(R.id.sky);

        mBlueSkyColor = ContextCompat.getColor(getActivity(), R.color.blue_sky);
        mSunsetSkyColor = ContextCompat.getColor(getActivity(), R.color.sunset_sky);
        mNightSkyColor = ContextCompat.getColor(getActivity(), R.color.night_sky);
        mBrightSunColor = ContextCompat.getColor(getActivity(), R.color.bright_sun);
        mBrighterSunColor = ContextCompat.getColor(getActivity(), R.color.pulsating_sun);

        //by default, sun is up, so play sunset animation on click
        mIsSunset = true;

        return fragRootView;
    }

    /**
     * Toggles flag to swap which animation to play on repeated user clicks.
     */
    private void toggleNextAnimation() {
        mIsSunset = !mIsSunset;
    }

    /**
     * Returns boolean to indicate if either AnimatorSet is running
     *
     * @return True if either SunRISE/SunSET animation is running, False otherwise.
     */
    private boolean isAnimationRunning() {
        return mSunRiseAnimatorSet.isStarted() || mSunSetAnimatorSet.isStarted();
    }

    /**
     * Create individual property animations for a SunSet, and choreograph them through
     * an AnimatorSet
     */
    private void createSunsetAnimation() {

        //top-edge of sun's local layout rectangle (LLR) has to end up at bottom of sky's LLR.
        mSunStartY = mSunView.getTop();
        float sunEndY = mSkyView.getBottom();
        float seaTop = 0; // explicitly state, since SeaView.getTop = w.r.t parent and is NOT 0.

        /**
         NOTE: Keyframes are used to synchronize animations;
         specify what animation value should be at certain points in the animation
         **/
        Keyframe sunSetK0 = Keyframe.ofFloat(0f, mSunStartY);
        //at 75% of animation, sun's bottom edge should touch sea's top edge
        Keyframe sunSetK1 = Keyframe.ofFloat(0.7f, mSkyView.getBottom() - mSunView.getHeight());
        Keyframe sunSetK2 = Keyframe.ofFloat(1f, sunEndY);
        //accelerate animation from sunSetK0 - sunSetK1
        sunSetK1.setInterpolator(new AccelerateInterpolator());
        PropertyValuesHolder pvh = PropertyValuesHolder
                .ofKeyframe("y", sunSetK0, sunSetK1, sunSetK2);

        //Animator to move a drawable object along the Y-axis from start to end
        ObjectAnimator heightAnimator = ObjectAnimator
//                .ofFloat(mSunView, "y", mSunStartY, sunEndY)//change sun's y value from start->end
                .ofPropertyValuesHolder(mSunView, pvh) //use Keyframes for synchronization
                .setDuration(ANIM_DURATION); // animate over 3 seconds

        //Specify how property values are calculated as a function of time.
//        heightAnimator.setInterpolator(new AccelerateInterpolator());

        /** NOTE: Animations and dimensions are w.r.t view's parent layout! **/
        //bottom-edge of Sun reflection's LLR has to end up at top-edge of sky's LLR
        mSunRefStartY = mSunRefView.getTop(); // 0 here is top of skyLayout (parent)
        mSunRefEndY = seaTop - mSunRefView.getHeight();
        //NOTE: ending y-coordinate is 0 - height of imageView holding sun's reflection

        Keyframe sunSetRefK0 = Keyframe.ofFloat(0f, mSunRefStartY);
        //at 75% of animation, sun reflection's top edge should touch sea's top edge
        Keyframe sunSetRefK1 = Keyframe.ofFloat(0.7f, seaTop);
        Keyframe sunSetRefK2 = Keyframe.ofFloat(1f, mSunRefEndY);
        sunSetRefK1.setInterpolator(new AccelerateInterpolator());
        PropertyValuesHolder refPVH = PropertyValuesHolder
                .ofKeyframe("y", sunSetRefK0, sunSetRefK1, sunSetRefK2);

        //Animate sun's reflection -> NOTE: Sun's reflection "point of motion reference" is its TOP!
        ObjectAnimator reflectionHeightAnimator = ObjectAnimator
//                .ofFloat(mSunRefView, "y", mSunRefStartY, mSunRefEndY)
                .ofPropertyValuesHolder(mSunRefView, refPVH)//use Keyframes for synchronization
                .setDuration(ANIM_DURATION);
//        reflectionHeightAnimator.setInterpolator(new AccelerateInterpolator());

        //commented out because will use in AnimatorSet
//        heightAnimator.start(); //start the object-animation

        /*  Animator to change a view's background color from mBlueSkyColor to mSunsetSkyColor

            NOTE: a color is an int value composed of ARGB values. Changing these results in
            a new int value representing some other random color. It's NOT a smooth gradient.
            THEREFORE, have to add a correct EVALUATOR (ARGB Evaluator())

            (or just use .ofArgb(..) for API 21+)
         */

//        //Using an ObjectAnimator:
//        ObjectAnimator sunsetSkyAnimator = ObjectAnimator
//                .ofInt(mSkyView, "backgroundColor", mBlueSkyColor, mSunsetSkyColor)
//                .setDuration(ANIM_DURATION);

        //Using a ValueAnimator: - [note we have to set value to object via event-handler]
        ValueAnimator sunsetSkyAnimator = ValueAnimator
                .ofInt(mBlueSkyColor, mSunsetSkyColor)
                .setDuration(ANIM_DURATION);

        //Animation update listener:
        sunsetSkyAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //Handle each frame update
                int updatedColorValue = (int) animation.getAnimatedValue();
                mSkyView.setBackgroundColor(updatedColorValue);
            }
        });

        sunsetSkyAnimator.setEvaluator(new ArgbEvaluator());
//        sunsetSkyAnimator.start(); //commented out because will use in AnimatorSet

        /*
            To choreograph animations together, need to use an AnimatorSet object.
            -> With this, we can set animations to play one after another, after some delay, etc..
            (see API documentation for specifics)

            -> Use an AnimatorSet to make the sky change from sunsetSky to nightSky after sun goes down
            ( a more crude implementation would be to do it via AnimationListener
              on sunsetSkyAnimator; override onAnimationEnd() and start new animation )
         */

        //First create the sunsetSky -> nightSky animation (can use either object or value animator)
        ObjectAnimator nightSkyAnimator = ObjectAnimator
                .ofInt(mSkyView, "backgroundColor", mSunsetSkyColor, mNightSkyColor)
                .setDuration(ANIM_DURATION);
        nightSkyAnimator.setEvaluator(new ArgbEvaluator());

        //Then place ALL animations within an AnimatorSet & set play sequence
//        AnimatorSet sunsetAnimatorSet = new AnimatorSet();
        mSunSetAnimatorSet.play(heightAnimator)
                .with(sunsetSkyAnimator)
                .with(reflectionHeightAnimator)
                .before(nightSkyAnimator);
//        sunsetAnimatorSet.play(nightSkyAnimator).after(heightAnimator); //after either one

        //Set listener to animatorSet to toggle mIsSunrise flag:
//        mAnimatorSet.addListener(new AnimationEndAdapter());

    }//createSunsetAnimation

    /**
     * Create individual property animations for a SunRise, and choreograph them through
     * an AnimatorSet
     */
    private void createSunriseAnimation() {

        float seaTop = 0; // explicitly state, since SeaView.getTop = w.r.t parent and is NOT 0.

        //Change sky view color to SunsetSkyColor from NightSkyColor
        ObjectAnimator sunsetSkyAnimator = ObjectAnimator
                .ofInt(mSkyView, "backgroundColor", mNightSkyColor, mSunsetSkyColor)
                .setDuration(ANIM_DURATION);
        sunsetSkyAnimator.setEvaluator(new ArgbEvaluator());

        //Create sunrise animation:
        Keyframe sunRiseK0 = Keyframe.ofFloat(0f, mSkyView.getBottom());
        Keyframe sunRiseK1 = Keyframe.ofFloat(0.45f, mSkyView.getBottom() - mSunView.getHeight());
        Keyframe sunRiseK2 = Keyframe.ofFloat(1f, mSunStartY);
//        sunRiseK1.setInterpolator(new DecelerateInterpolator());
//        sunRiseK2.setInterpolator(new AccelerateInterpolator());
        PropertyValuesHolder sunRisePVH = PropertyValuesHolder
                .ofKeyframe("y", sunRiseK0, sunRiseK1, sunRiseK2);

        ObjectAnimator sunriseAnimator = ObjectAnimator
                .ofPropertyValuesHolder(mSunView, sunRisePVH)
//                .ofFloat(mSunView, "y", mSkyView.getBottom(), mSunStartY) //use Keyframes instead
                .setDuration(ANIM_DURATION);
        sunriseAnimator.setInterpolator(new AccelerateInterpolator());


        //Create sunrise reflection animation:
        Keyframe sunRefRiseK0 = Keyframe.ofFloat(0f, mSunRefEndY);
        Keyframe sunRefRiseK1 = Keyframe.ofFloat(0.45f, seaTop);
        Keyframe sunRefRiseK2 = Keyframe.ofFloat(1f, mSunRefStartY);
//        sunRefRiseK1.setInterpolator(new DecelerateInterpolator());
//        sunRefRiseK2.setInterpolator(new AccelerateInterpolator());
        PropertyValuesHolder sunRefRisePVH = PropertyValuesHolder
                .ofKeyframe("y", sunRefRiseK0, sunRefRiseK1, sunRefRiseK2);

        ObjectAnimator reflectionRiseAnimator = ObjectAnimator
                .ofPropertyValuesHolder(mSunRefView, sunRefRisePVH)
//                .ofFloat(mSunRefView, "y", mSunRefEndY, mSunRefStartY)
                .setDuration(ANIM_DURATION);
        reflectionRiseAnimator.setInterpolator(new AccelerateInterpolator());

        //Change sky view color to blue sky from sunrise
        ObjectAnimator blueSkyAnimator = ObjectAnimator
                .ofInt(mSkyView, "backgroundColor", mSunsetSkyColor, mBlueSkyColor)
                .setDuration(ANIM_DURATION);
        blueSkyAnimator.setEvaluator(new ArgbEvaluator());

        //Place animations inside corresponding Animator Set
        mSunRiseAnimatorSet.play(sunriseAnimator)
                .with(blueSkyAnimator)
                .with(reflectionRiseAnimator)
                .after(sunsetSkyAnimator);

        //Set listener to sunRiseAnimatorSet to toggle mIsSunrise flag:
//        mAnimatorSet.addListener(new AnimationEndAdapter());

    }//createSunriseAnimation

    /**
     * Create individual property animation to change the sun view's color
     */
    private void startPulsatingSunAnimation() {

        //Value animator for color transition; manually update drawable background
        ValueAnimator pulsatingSunAnimator = ValueAnimator
                .ofInt(mBrightSunColor, mBrighterSunColor)
                .setDuration(500); //half a second for color to "pulse"

        //set proper evaluator to interpret ARGB colors
        pulsatingSunAnimator.setEvaluator(new ArgbEvaluator());

        //add listener to update drawable resource
        pulsatingSunAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                updateSunDrawableColor(animation);
            }
        });

        //Loop infinitely, and reverse color change on each iteration
        pulsatingSunAnimator.setRepeatCount(ValueAnimator.INFINITE);
        pulsatingSunAnimator.setRepeatMode(ValueAnimator.REVERSE);

        pulsatingSunAnimator.start();

        /* For sun's reflection: */

    }//createPulsatingSunAnimation

    /**
     * Changes the sun drawable's background color to the animation value
     *
     * @param animation ValueAnimation containing updated values.
     */
    private void updateSunDrawableColor(ValueAnimator animation) {
        //on each new frame: MORE INFO @ (http://stackoverflow.com/a/17825210)

        Drawable sunDrawable = ((ImageView) mSunView).getDrawable();
        Drawable sunReflectionDrawable = ((ImageView) mSunRefView).getDrawable();
        if (sunDrawable instanceof GradientDrawable
                && sunReflectionDrawable instanceof  GradientDrawable) {

            GradientDrawable gradientSunDrawable
                    = (GradientDrawable) sunDrawable;
            GradientDrawable gradientSunReflectionDrawable
                    = (GradientDrawable) sunReflectionDrawable;
            int ArgbColor = getArgbColor((int) animation.getAnimatedValue());

            /*
                Use mutate if this drawable is shared and only want to
                change this specific one
             */
//            gradientDrawable.mutate();

            gradientSunDrawable.setColor(ArgbColor);
            gradientSunReflectionDrawable.setColor(ArgbColor);

        } else {
            Log.e(TAG, "ERROR: Expecting different background type");
        }

    }//updateSunDrawableColor

    private int getArgbColor(int intColor) {
        int A = Color.alpha(intColor);
        int R = Color.red(intColor);
        int G = Color.green(intColor);
        int B = Color.blue(intColor);
        return Color.argb(A, R, G, B);
    }

    /**
     * Class to implement behaviour for a subset of AnimatorListener handlers;
     * namely only "onAnimationEnd(..)"
     */
    private class EndAnimationAdapter extends AnimatorListenerAdapter {
        @Override
        public void onAnimationEnd(Animator animation) {
//            super.onAnimationEnd(animation); //super does nothing

            //BUG: https://code.google.com/p/android/issues/detail?id=227671
            //Apparently these are incorrectly called by the framework if set to AnimatorSet

        }//onAnimationEnd

    }//NormalToBrightSunEndAnimationAdapter

}
