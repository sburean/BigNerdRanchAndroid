package burean.szabi.sunset;

import android.support.v4.app.Fragment;

public class SunsetActivity extends SingleFragmentActivity {

    @Override
    protected Fragment getFragment() {
        return SunsetFragment.newInstance();
    }

}
