package burean.szabi.sunset;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    private static final String TAG = SingleFragmentActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        FragmentManager fm = getSupportFragmentManager();
        Fragment hostedFrag = fm.findFragmentById(R.id.fragment_container);
        if(hostedFrag == null){
            //fragment not hosted
            fm.beginTransaction()
                    .add(R.id.fragment_container, getFragment())
                    .commit();
        }

    }

    @LayoutRes
    protected int getLayout() {
        return R.layout.activity_single_fragment;
    }

    protected abstract Fragment getFragment();

}
