package burean.szabi.photogallery.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

/**
 * SharedPreference to provide quick & easy persistence of application's primitive data
 * as key-value pairs.
 */
public class QueryPreferences {

    private static final String TAG = QueryPreferences.class.getSimpleName();
    private static final String PREF_SEARCH_QUERY = "searchQuery";
    private static final String PREF_LAST_RESULT_ID = "lastResultId";
    private static final String PREF_IS_ALARM_ON = "isAlarmOn";

    /**
     * Used to retrieve a String representation of the user's latest search query
     *
     * @param context Component context to access storage.
     * @return String object for latest search query.
     */
    @Nullable
    public static String getStoredQuery(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_SEARCH_QUERY, null);
        //2nd arg = default string value (null in this case)
    }//getStoredQuery

    /**
     * Used to persist the user's latest search query.
     *
     * @param context Component context to access storage.
     * @param query   search query to persist.
     */
    public static void setStoredQuery(Context context, String query) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_SEARCH_QUERY, query)
                .apply();
    }//setStoredQuery

    /**
     * Used to retrieve a String representation of the latest searched photo Id.
     *
     * @param context Component context to access storage.
     * @return String object of the latest searched photo Id
     */
    @Nullable
    public static String getLastResultId(Context context) {

        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_LAST_RESULT_ID, null); //null as def. value

    }//getLastResultId

    /**
     * Used to persist the last searched photo's Id.
     *
     * @param context      Component context to access storage.
     * @param lastResultId photo Id to persist.
     */
    public static void setLastResultId(Context context, String lastResultId) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_LAST_RESULT_ID, lastResultId)
                .apply();
    }//setLastResultId

    /**
     * Persists current state of PollService alarm.
     * @param context Component context.
     * @param isOn flag indicating if PollService alarm is on.
     */
    public static void setAlarmOn(Context context, boolean isOn){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(PREF_IS_ALARM_ON, isOn)
                .apply();
    }//setAlarmOn

    /**
     * Indicates if an alarm was previously set for PollService.
     * @param context Component context.
     * @return boolean indicating alarm's previous state.
     */
    public static boolean isAlarmOn(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(PREF_IS_ALARM_ON, false);
    }//isAlarmOn

}
