package burean.szabi.photogallery.utils.preloader;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Interface representing an abstraction for ThumbnailPreLoaders.
 */
public interface ThumbnailPreLoader {

    /**
     * Start pre-loading thumbnails defined as a collection of urls.
     * @param urls collection of URLs of thumbnails to download.
     */
    void start(List<String> urls);

    /**
     * Clean up any resources used by the pre-loader
     */
    void shutdown();

    /**
     * Observer interface to be implemented by requester of pre-loading function
     */
    interface ThumbnailPreLoaderHandler {
        void onThumbnailPreLoaded(String url, Bitmap thumbnail);
    }

    void registerPreLoaderHandler(ThumbnailPreLoaderHandler handler);

}
