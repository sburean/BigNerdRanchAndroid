package burean.szabi.photogallery.controller;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import burean.szabi.photogallery.service.PollService;

/**
 * Abstract fragment that automatically registers a dynamic broadcast receiver that
 * listens for action: "burean.szabi.photogallery.service.PollService.SHOW_NOTIFICATION"
 * <p>This broadcast will be picked up by subclasses of this abstract class while
 * the fragment is visible.
 * <p>Used to hide foreground notifications.
 */
public abstract class VisibleFragment extends Fragment {

    private NotificationBroadcastReceiver mOnShowNotification = new NotificationBroadcastReceiver();
    private static final String TAG = VisibleFragment.class.getSimpleName();

    @Override
    public void onStart() {
        super.onStart();
//        Log.i(TAG, "onStart");
        //IMPORTANT: Have to define an IntentFilter object for dynamic receiver.
        IntentFilter intentFilter = new IntentFilter(PollService.ACTION_SHOW_NOTIFICATION);
        getActivity().registerReceiver(
                mOnShowNotification, //implementation of BroadcastReceiver
                intentFilter, // <intent-filter> for receiver
                PollService.PERMISSION_PRIVATE, // use broadcast's permission to receive it
                null // thread's handler to handle receiver; main thread handler if null.
        );

        /*
            NOTE: Adding private permission here ensures receiver is only triggered by broadcasts
                    that also utilize the same permission! (prevents other components from sending
                    fake broadcasts)
         */

    }

    @Override
    public void onStop() {
        super.onStop();
//        Log.i(TAG, "onStop");
        getActivity().unregisterReceiver(mOnShowNotification);
    }

    private class NotificationBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            /* Called on main thread while fragment is visible */

            //If we're receiving this intent, the application is in the foreground; cancel intent.
            Log.i(TAG, "canceling notification");
            setResultCode(Activity.RESULT_CANCELED);
            //NOTE: Can also return data via setResultData(String) or setResultExtras(Bundle)

        }//onReceive

    }//NotificationBroadcastReceiver

}//VisibleFragment
