package burean.szabi.photogallery.controller.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import burean.szabi.photogallery.controller.SingleFragmentActivity;
import burean.szabi.photogallery.controller.fragments.PhotoPageFragment;

/**
 * Activity that hosts a fragment to display a web page.
 */
public class PhotoPageActivity extends SingleFragmentActivity {

    private Uri mUri;
    private PhotoPageFragment mPhotoPageFragment;

    /**
     * Used to retrieve an intent to start an instance of this Activity.
     *
     * @param context Component context to start activity from.
     * @param uri     Uri of data
     * @return Intent configured to launch a new instance of this Activity.
     */
    public static Intent newIntent(Context context, Uri uri) {
        Intent i = new Intent(context, PhotoPageActivity.class);
        i.setData(uri);
        return i;
    }//newIntent

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        //Get Uri to web page from intent & pass it to fragment:
        mUri = getIntent().getData();

        super.onCreate(savedInstanceState);

    }//onCreate

    @Override
    protected Fragment createFragment() {
        //Called by super implementation of onCreate, need valid URI
        if (mUri == null) {
            throw new IllegalArgumentException(
                    "Null Uri; need to set before super call to onCreate!"
            );
        }
        mPhotoPageFragment = PhotoPageFragment.newInstance(mUri);
        return mPhotoPageFragment;
    }

    @Override
    public void onBackPressed() {

        //This activity has to host a fragment with a WebView for this to work?
        if (mPhotoPageFragment.canWebViewGoBack()) {
            //user navigated through WebView, can go back:
            mPhotoPageFragment.webViewGoBack();
        } else {
            //user did NOT navigate through WebView; delegate behaviour to super:
            super.onBackPressed();
        }

    }
}//PhotoPageActivity
