package burean.szabi.photogallery.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.util.LruCache;
import android.util.Log;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import burean.szabi.photogallery.utils.preloader.FactoryPoolPreLoader;
import burean.szabi.photogallery.utils.preloader.ThumbnailPreLoader;

/**
 * Prepares a background thread with a Looper to create a
 * message-loop that's used to retrieve thumbnails from various images through
 * the use of a message-queue.
 * <p>
 * //fixme: should check + store methods for cache be synchronized? they are already thread safe..
 */
public class ThumbnailDownloader<T> extends HandlerThread {

    /*
        NOTE: The HandlerThread super-class is a subclass of java.lang.Thread.

          It's a normal thread that's with extra features; see docs.
          Once it's created (via new..), it's in the RUNNABLE state, so we
          have to call ThumbnailDownloader.start() to make the thread eligible
          to be placed in the RUNNING state by the scheduler
          (JVM calls thread's .run() method)
     */

    private static final String TAG = ThumbnailDownloader.class.getSimpleName();
    private static final int MESSAGE_DOWNLOAD = 0; //Message's what field (ID code)

    private Handler mRequestHandler; //Handler that deals with REQUEST messages (THIS Looper)
    private Handler mResponseHandler; //Handler that deals with RESPONSE messages (OTHER Looper)
    private ThumbnailDownloadListener<T> mThumbnailDownloadListener; //observer of this work
    private final LruCache<String, Bitmap> mThumbnailCache; //cache for downloaded thumbnails
    private ThumbnailPreLoader mThumbnailPreLoader; //strategy: pre-loading handled by this object

    //store/retrieve URL associated with a PhotoHolder
    private ConcurrentMap<T, String> mRequestMap = new ConcurrentHashMap<>();

    public ThumbnailDownloader(Handler responseHandler, int maxMemory) {
        super(TAG); //call the super-class, single-arg constructor as required. (arg = name)

        this.mResponseHandler = responseHandler; //which handler this thread should send responses to.

        /* NOTE: maxMemory is max memory of VM (in KiloBytes)
                - exceeding this causes OutOfMemory exception.

            Use 1/8th of available memory for thumbnail cache.
            //fixme: (What's a good rule of thumb here?)
         */
        int cacheSize = maxMemory / 8;
        this.mThumbnailCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                //override units of cache size to be KiloBytes
                return value.getByteCount() / 1024;
            }
        };

        this.mThumbnailPreLoader = new FactoryPoolPreLoader().getPreLoader();

    }//ThumbnailDownloader-C'tor

    /**
     * Listener interface that will be used to communicate the responses of this thread's
     * work with the requester of that work.
     * This is an OBSERVER INTERFACE, the requester is observing ths subject (this). Once
     * background work is complete, worker will notify listener (requester) 's HANDLER!
     *
     * @param <T> Work identifier.
     */
    public interface ThumbnailDownloadListener<T> {
        void onThumbnailDownloaded(T target, Bitmap thumbnail);
    }

    /**
     * To be used by observers interested in receiving notifications from this background
     * message-loop once work is completed.
     *
     * @param observer Observer object that implements the observer interface
     */
    public void setThumbnailDownloadListener(ThumbnailDownloadListener<T> observer) {
        this.mThumbnailDownloadListener = observer;
    }

    /**
     * Delegates a pre-loading task to a composed object
     * for the specified collection of URLs.
     *
     * @param urlsToPreLoad Collection of url Strings to preload
     */
    public void preloadThumbnails(List<String> urlsToPreLoad) {

        /* RUNS ON MAIN THREAD */

        for (Iterator<String> iterator = urlsToPreLoad.iterator(); iterator.hasNext(); ) {
            String url = iterator.next();
            if (mThumbnailCache.get(url) != null) {
                //trim any thumbnails to pre-load if they already exist in cache
                iterator.remove();
            }//thumbnail already in cache?
        }

        //Handler once a thumbnail is pre-loaded
        mThumbnailPreLoader.registerPreLoaderHandler(
                new ThumbnailPreLoader.ThumbnailPreLoaderHandler() {

            /* CALLED FROM A THREAD-POOL WORKER THREAD! */

                    @Override
                    public void onThumbnailPreLoaded(String url, Bitmap thumbnail) {
//                Log.i(TAG, "Received pre-loaded bitmap for URL: " + url);

                        //If thumbnail is not cached already - store it
                        if (getBitmapFromCache(url) == null) {

                /*
                    NOTE: This check & put isn't synchronized here, because even if something
                        changes cache after this check, the store operation is a
                        atomic check & put operation.
                 */

//                    Log.i(TAG, "and stored it into the cache!");
                            storeBitmapToCache(url, thumbnail);

                        }//thumbnail already cached?

                    }//onThumbnailPreLoaded

                });

        //fixme: should bind queue so it doesn't get unlimited requests -> how to check how many it has?
        mThumbnailPreLoader.start(urlsToPreLoad);

    }//preloadThumbnails

    /**
     * Creates & sets a message in the message queue for thread to retrieve a thumbnail
     * from the specified URL. Should be accessed from thread looking to do work in
     * the background via this background thread.
     *
     * @param target Generic type to identify each download and which UI element to update
     * @param url    Thumbnail's url
     */
    public void queueThumbnail(T target, String url) {
//        Log.i(TAG, "Got a URL: " + url);

        //This method of the Handler runs on the Main UI Thread

        /* NOTE: Instead of locking the target type to an ImageView ID, use generics for flexibility

            We need a way to identify where the result of the background thread should be placed.
            (And what we're processing.)

            In this case, we provide a generic type that contains an unbounded object type that
            represents a ViewHolder. This contains a reference to an imageView where the
            downloaded thumbnail should go, and what we're downloading

            ****************************************************************************************
            BASICALLY, args to this should contains necessary info such that the background-thread *
            can perform what it needs to and return the result somewhere                           *
            ****************************************************************************************
            *
         */

        //Make sure passed - URL is non-null
        if (url == null) {
            //Fixme:
            //this check is never valid using the parser (FlickrFetchr#parseJSON)
            // because it doesn't create a GalleryImage UNLESS it has a valid url!
            mRequestMap.remove(target);
        } else {

            //First, store the PhotoHolder into the concurrentHashMap with the URL as the value.
            mRequestMap.put(target, url);

            //Then obtain a message from the Handler, and set it's fields: (what & obj)
            Message msg = mRequestHandler
                    .obtainMessage(MESSAGE_DOWNLOAD, target); //handler set automatically

            //Finally, sent the message to its handler to be placed in the queue & eventually handled
            msg.sendToTarget();

            /* ALTERNATIVELY:

                Can send message directly through Handler, where we can include delays, etc..
                (See Handler.sendMessage... methods) - Useful for other functionality as well!

             */
//            mRequestHandler.sendMessageDelayed(msg, 4000);
        }

    }//queueThumbnail

    @Override
    protected void onLooperPrepared() {
        /*
            NOTE: This is called before looper checks the messageQueue for the first time.
            Therefore, it's a good place to create our Handler(s) with their implementations
         */

        // handler associated with current thread's Looper
        mRequestHandler = new Handler(new Handler.Callback() {

            /*
                Handler implementation to handle received messages once looper pulls a message
                off of its queue and sends it to the message's target to be services:
             */
            @Override
            public boolean handleMessage(Message message) {

                //Differentiate between message IDs to know how to service:
                switch (message.what) {
                    case MESSAGE_DOWNLOAD:

                        @SuppressWarnings("unchecked")
                        T target = (T) message.obj; //get our object from message

//                        Log.i(TAG, "Got a request for URL: " + mRequestMap.get(target));
                        //handle request:
                        handleRequest(target);

                        return true; //message handled
                    default:
                        Log.e(TAG, "Reached default cause in switch(message.what)");
                        return false; //message handled
                }//switch-message req. ID

            }//handleMessage

        });

    }//onLooperPrepared

    /**
     * Helper method to retrieve a thumbnail and display it on a ViewHolder
     * Handles Message.what = MESSAGE_DOWNLOAD.
     */
    private void handleRequest(final T target) {
        /* ON THIS BACKGROUND THREAD */

        try {

            //Get URL from concurrentHashMap for the passed target T:
            final String url = mRequestMap.get(target);

            //Make sure URL is valid: (NOTE: not a valid check with our current parser)
            if (url == null) {
                return;
            }

            /*
                First check if target exists in cache:
                - if it does, load the associated bitmap.
                - if it doesn't, download the thumbnail and create a bitmap.
             */
            Bitmap thumbnail = getBitmapFromCache(url);

            if (thumbnail == null) {

                //cache miss; retrieve thumbnail:

                //Download thumbnail from the URL via FlickFetchr as an array of bytes:
                byte[] bitmapBytes = new FlickrFetchr().getUrlBytes(url);

                //NOTE: We're dealing with thumbnails, safe to directly decode
                // - BUT, would should still check if need to load bitmaps efficiently.
                thumbnail = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);

                //Store to LruCache:
                storeBitmapToCache(url, thumbnail);

            } //cache miss

            //Have thumbnail, notify listener so they may handle it:
            notifyListener(target, url, thumbnail);

//            Log.i(TAG, "Bitmap created");

        } catch (IOException ioe) {
            Log.e(TAG, "Error downloading image", ioe);
        }

    }//handleRequest

    /**
     * Used to create a new runnable for the listener's Handler such that it's looper
     * may handle it appropriately.
     *
     * @param target    target of the download request
     * @param url       url of the target's request
     * @param thumbnail thumbnail for the target
     */
    private void notifyListener(final T target, final String url, final Bitmap thumbnail) {

        /*
            NOTE: We can't just call Observer.notify(data) directly from this method,
                    because this is running in the background thread.

              The Observer's notify method will run in the background
              thread too (placed on top of this stack), so it WON'T have access
              to any UI elements, and if it tries to access them, we will get
              a "CallFromWrongThreadException"
         */

        //Use the Handler associated with main thread's Looper to
        // post a Runnable containing the Observer.notify(...) method; to be ran in main thread!

        mResponseHandler.post(new Runnable() {
            @Override
            public void run() {
                    /* ON mRequestHandler's LOOPER THREAD (main UI in this case) */

                    /*
                        Need this check because RecyclerView recycles it's views.
                        By the time this run() is executes, the view that originally
                        requested the thumbnail may have been recycled, and reused.
                        THEREFORE, this makes sure that each PhotoHolder gets the
                        correct image! - if we're trying to set a thumbnail for
                        an image that the user already scrolled past, just exit.
                     */

                //Make sure we're receiving correct thumbnail on main Thread:
                if (mRequestMap.get(target) != url) {
                    /*
                        url is where the thumbnail is from, it's associated with a target

                        We check to make sure our target we're updating is the one associated
                            with the thumbnail's source url that we got at the time the thumbnail
                             was downloaded (url depicts what thumbnail we downloaded)

                        Need to ensure we're setting the thumbnail to the correct target!
                     */
                    Log.i(TAG, "---------received incorrect thumbnail!----------");
                    return;
                }

                //Remove this target's url from concurrentHashMap:
                mRequestMap.remove(target); //note need for concurrentHashMap

                //Notify observer
                mThumbnailDownloadListener.onThumbnailDownloaded(target, thumbnail);
            }
        });

    }//notifyListener

    /**
     * Retrieve a cached bitmap for a specified key, if it exists
     *
     * @param key target for this bitmap
     * @return Bitmap if one exists for the key, null otherwise
     */
    private Bitmap getBitmapFromCache(String key) {
        return mThumbnailCache.get(key);
    }//getBitmapFromCache

    /**
     * Add a downloaded bitmap to the cache IF it doesn't already exist
     *
     * @param key    target for this bitmap
     * @param bitmap bitmap to cache
     */
    private void storeBitmapToCache(String key, Bitmap bitmap) {
        synchronized (mThumbnailCache) {
            //Check & put should be an atomic operation
            if (getBitmapFromCache(key) == null) {
                //Note to self: do we REALLY need this check? - simply doesn't override if already exists.
                mThumbnailCache.put(key, bitmap);
            }//if key doesn't exist in cache already
        }
    }

    /**
     * Clean resources when Views are destroyed; remove all references to them.
     */
    public void clearQueue() {

        /*
            Map uses ViewHolders as Keys (which store views, and therefore context).
             Once views are destroyed, we need to clear keys, otherwise they would remain
             in map forever and create a memory leak.
            NOTE: Don't need to clear cache, it only stores string URLs as keys - keep
         */

        mRequestMap.clear();
        mRequestHandler.removeMessages(MESSAGE_DOWNLOAD);
//        mThumbnailCache.evictAll();
    }

    @Override
    public boolean quit() {
        mThumbnailPreLoader.shutdown();//Terminate the PreLoader
        return super.quit();
    }

}//ThumbnailDownloader
