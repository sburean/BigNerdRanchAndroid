package burean.szabi.photogallery.utils.preloader;

/**
 * Concrete factory that returns a new instance of a PreLoader utilizing a ThreadPool.
 */
public class FactoryPoolPreLoader implements FactoryPreLoader<ThumbnailThreadPoolPreLoader> {

    @Override
    public ThumbnailThreadPoolPreLoader getPreLoader() {
        return new ThumbnailThreadPoolPreLoader();
    }

}
