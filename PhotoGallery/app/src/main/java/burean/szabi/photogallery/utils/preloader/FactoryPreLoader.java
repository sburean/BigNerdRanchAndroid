package burean.szabi.photogallery.utils.preloader;

/**
 * Factory abstraction to retrieve instances of a Pre-loader
 */
public interface FactoryPreLoader<T extends ThumbnailPreLoader> {

    T getPreLoader();

}
