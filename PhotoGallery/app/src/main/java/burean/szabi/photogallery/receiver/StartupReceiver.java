package burean.szabi.photogallery.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import burean.szabi.photogallery.service.PollService;
import burean.szabi.photogallery.utils.QueryPreferences;

/**
 * Static receiver to listen for the system broadcast with the action
 * Intent.ACTION_BOOT_COMPLETED. This re-schedules the PollService alarm
 * as it's removed upon device shut-down.
 */
public class StartupReceiver extends BroadcastReceiver {

    private static final String TAG = StartupReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        //Upon device boot, re-register PollService alarm if it was registered prior to shut-down.
//        Log.i(TAG, "onReceive");
        boolean isAlarmOn = QueryPreferences.isAlarmOn(context);
        PollService.setServiceAlarm(context, isAlarmOn);
    }//onReceive

}//StartupReceiver
