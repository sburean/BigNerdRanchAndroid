package burean.szabi.photogallery.receiver;

import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import burean.szabi.photogallery.service.PollService;

/**
 * Standalone broadcast receiver that has its priority altered to receive an
 * OrderedBroadcastIntent (with filtered action) last in a sequence of broadcast receivers.
 * Must be standalone to outlive the PollService and send a notification appropriately.
 * <p>
 * It checks the intent's status code to determine if a notification should be posted
 * to the status bar in response to PollService having new search results available.
 * <p>
 * NOTE: Have to set this receiver's priority in the AndroidManifest.xml receiver's
 * intent-filter tag to the lowest of all declared receivers to ensure it runs last
 * in the ordered sequence, as an "artificial ResponseReceiver" <br>
 * (-999 is lowest user defined priority)
 */
public class NotificationReceiver extends BroadcastReceiver {

    private static final String TAG = NotificationReceiver.class.getSimpleName();

    /*
        NOTE: Combination of standalone receivers, ordered broadcasts, and
            dynamic receivers are the best way to see if a fragment is in the
            foreground or not, and act accordingly.
     */

    @Override
    public void onReceive(Context context, Intent intent) {

        int resultCode = getResultCode();
        if(resultCode == Activity.RESULT_OK){
            //Okay to send notification

            //Get notification from intent's extra
            Notification notification = intent
                    .getParcelableExtra(PollService.B_INTENT_EXTRA_NOTFN);

            //Get notification request code form intent's extra
            int requestCode = intent
                    .getIntExtra(PollService.B_INTENT_EXTRA_NOTFN_REQ_CODE, 0);

            /*
                Post notification through the NotificationManagerCompat system service:
                [NOTE: Use NotificationManagerCompat for compatibility!]
             */
            Log.i(TAG, "sending notification...");
            NotificationManagerCompat nmc = NotificationManagerCompat.from(context);
            nmc.notify(requestCode, notification);

        } //okay to send notification?

        //Notification cancelled / do nothing - foreground receiver cancelled notification

    }//onReceive

}//NotificationReceiver
