package burean.szabi.photogallery.controller.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import burean.szabi.photogallery.controller.SingleFragmentActivity;
import burean.szabi.photogallery.controller.fragments.PhotoGalleryFragment;

/**
 * fixme: add doc once done
 */
public class PhotoGalleryActivity extends SingleFragmentActivity {

    private static final String TAG = PhotoGalleryActivity.class.getSimpleName();

    /**
     * Returns an explicit intent to start this activity from provided context.
     * @param context package context to start this activity.
     * @return Explicit intent object to star this activity.
     */
    public static Intent newIntent(Context context){
        return new Intent(context, PhotoGalleryActivity.class);
    }

    @Override
    protected Fragment createFragment() {
        return PhotoGalleryFragment.newInstance();
    }

}
