package burean.szabi.photogallery.utils.preloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import burean.szabi.photogallery.utils.FlickrFetchr;

/**
 * Used to preload a set number of thumbnails, and notify caller.
 * Shutting down this ThreadPool is the responsibility of the caller!
 */
public class ThumbnailThreadPoolPreLoader implements ThumbnailPreLoader {

    private static final String TAG = ThumbnailThreadPoolPreLoader.class.getSimpleName();
    private static final int NUMBER_WORKER_THREADS = 10; //random decently sized number

    private ExecutorService mThreadPool;
    private ThumbnailPreLoaderHandler mListener;

    ThumbnailThreadPoolPreLoader() {

//        int nThreads = thumbnailsToPreload / 2; //Use one thread per pair of neighbours
        mThreadPool = Executors.newFixedThreadPool(NUMBER_WORKER_THREADS);

    }//ThumbnailPreLoader - c'tor

    public void start(List<String> urls) {
        //loop through all urls to download and start a task for each:
        for (String url : urls) {
            preLoadThumbnail(url);
        }
    }//start

    public void registerPreLoaderHandler(ThumbnailPreLoaderHandler handler) {
        this.mListener = handler;
    }

    private void preLoadThumbnail(final String url) {

        mThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                /* Executed by worker threads within mThreadPool */

                try {
//                    Log.i(TAG, "Pre-loading URL: " + url);

                    //Download thumbnail from URL & create a Bitmap from it.
                    byte[] bitmapBytes = new FlickrFetchr().getUrlBytes(url);
                    Bitmap thumbnail = BitmapFactory
                            .decodeByteArray(bitmapBytes, 0, bitmapBytes.length);

                    //Notify caller to handle
//                    Log.i(TAG, "Got Pre-loaded Bitmap! Notifying caller...");
                    mListener.onThumbnailPreLoaded(url, thumbnail);

                } catch (IOException ioe) {
                    Log.e(TAG, "Error downloading image", ioe);
                }

            }//run
        });

    }

    /**
     * Terminates the thread-pool NOW;
     * existing tasks are no longer executed, working threads interrupted
     * (on a best-effort interrupt - no guarantees)
     */
    public void shutdown() {
        mThreadPool.shutdownNow();
    }//shutdown

}
