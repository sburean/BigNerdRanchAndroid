package burean.szabi.photogallery.controller.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import burean.szabi.photogallery.R;
import burean.szabi.photogallery.controller.VisibleFragment;
import burean.szabi.photogallery.controller.activities.PhotoPageActivity;
import burean.szabi.photogallery.model.GalleryItem;
import burean.szabi.photogallery.service.PollService;
import burean.szabi.photogallery.utils.FlickrFetchr;
import burean.szabi.photogallery.utils.QueryPreferences;
import burean.szabi.photogallery.utils.ThumbnailDownloader;

/*
 * TODO: spinner status on screen rotation -> switch to invisible by default.
 */
public class PhotoGalleryFragment extends VisibleFragment {

    private static final String TAG = PhotoGalleryFragment.class.getSimpleName();

    private static final int MAX_MEMORY;

    private RecyclerView mRecyclerView;
    private SearchView mSearchView;
    private ProgressBar mProgressBar;

    private List<GalleryItem> mItems = new ArrayList<>();
    private ThumbnailDownloader<PhotoHolder> mThumbnailDownloader; //ref. to background message-loop
    //type-parameter = ID and in this case also where target downloaded image should go

    static {
        //Static initializer of max memory available to VM (in KiloBytes)
        MAX_MEMORY = (int) (Runtime.getRuntime().maxMemory() / 1024);
    }

    public static PhotoGalleryFragment newInstance() {

        Bundle args = new Bundle();
        //args here
        PhotoGalleryFragment fragment = new PhotoGalleryFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true); //retain fragment for AsyncTask & background thread handling
        setHasOptionsMenu(true); //receive menu callbacks from host in this fragment

        /*
            NOTE: Internet permission is required to fetch data from the web-server,
                    however it'a a NORMAL permission - safe for users.

                  Therefore, it the system DOESN'T explicitly ask the user for it!
                  (automatically granted)

            MoreInfo: https://developer.android.com/guide/topics/permissions/normal-permissions.html
         */

//        final int maxMemory =  //.maxMemory() returns in bytes

        //Create background message-loop to retrieve thumbnails:
        mThumbnailDownloader = new ThumbnailDownloader<>(new Handler(), MAX_MEMORY);
        mThumbnailDownloader.start();
        mThumbnailDownloader.getLooper(); //blocks until Looper is initialized (ensures onLooperPrepared() is complete)
        Log.i(TAG, "Background Thread Started");

        //register an event-handler as the observer to the background message-loop:
        mThumbnailDownloader.setThumbnailDownloadListener(new ThumbnailDownloader
                .ThumbnailDownloadListener<PhotoHolder>() {

            //Ran by THIS THREAD'S handler once a thumbnail has been downloaded.
            @Override
            public void onThumbnailDownloaded(PhotoHolder target, Bitmap thumbnail) {

                if (!isAdded()) {
                    return; //make sure fragment is still attached to activity.
                }
                //Call the viewHolder's bind method to set appropriate ImageView source
                Drawable drawable = new BitmapDrawable(getResources(), thumbnail);
                target.bindViewContent(drawable);

            }//onThumbnailDownloaded

        });

        //start background task to fetch data from web-server via URL
        //NOTE: This is placed in onCreate() since this fragment is retained, and onCreate()
        //      is only called when fragment is originally created.
        updateItems();

        //DEBUG CODE:
        //schedule service to be ran at various intervals:
//        Intent i = PollService.newIntent(getActivity());
//        getActivity().startService(i);

        //Schedule background polling service to be started on a set interval
//        PollService.setServiceAlarm(getActivity(), true);

    }//onCreate

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_photo_gallery, container, false);

        //NOTE: ProgressBar invisible by default.
        mProgressBar = (ProgressBar) rootView
                .findViewById(R.id.fragment_photo_gallery_indeterminate_progress);
        if(savedInstanceState == null){
            //first time fragment is created (ie: not restored via retaining), show progress bar
            showProgressBar();

            //fixme: progress bar won't show if rotating screen during async-task execution.
            //(would need to check if task is running, in that case won't need this if(), only
            // show progress here if async task is running.)
            // -> hold async task in a variable, and call getStatus() on it, check if .RUNNING
        }


        mRecyclerView = (RecyclerView) rootView
                .findViewById(R.id.fragment_photo_gallery_recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3)); //3 columns in grid
        mRecyclerView.addItemDecoration(new ItemOffsetDecorator());

        /*
            Adding an onScrollListener is to detect when user scrolled to the bottom of the list.
            Here, we will display a small loading icon (see recycler view progress notification),
                while we fetch the next X items from the WebService to display.
            (Essentially how the reddit app works)
         */
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                Log.i(TAG, "onScrollStateChanged");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                GridLayoutManager glm = (GridLayoutManager) recyclerView.getLayoutManager();
                int totalViews = glm.getItemCount();
                int viewsDisplayed = glm.getChildCount();
                int posLastDisplayedView = glm.findFirstVisibleItemPosition();

//                Log.i(TAG, "onScrolled, totalViews = " + totalViews
//                        + ", viewsDisplayed = " + viewsDisplayed + " posLastDisplayedView = "
//                        + posLastDisplayedView);

//                if (viewsDisplayed + posLastDisplayedView >= totalViews) {
//                    Log.d(TAG, "Last-Row!");
//                    /*
//                        Once we have last row, can retrieve next X items from WebService.
//                        IMPORTANT: Have to make sure that we do not repeatedly create
//                                    new AsyncTasks - Use flag to see if one is already in progress.
//
//                        For more info:
//
//                        http://stackoverflow.com/questions/26543131/how-to-implement-endless-list-with-recyclerview
//                     */
//                }

            }
        });

        setUpAdapter();

        return rootView;
    }

    private void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //Inflate menu so they may be added to the this fragment's toolbar
        inflater.inflate(R.menu.menu_fragment_photo_gallery, menu);

        //Attach handler to handle search query once entered by user
        final MenuItem actionViewItem = menu.findItem(R.id.menu_item_search);
        mSearchView = (SearchView) actionViewItem.getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                //Called whenever the user pressed the search button & submitted query
                Log.d(TAG, "onQueryTextSubmit: " + query);
                QueryPreferences.setStoredQuery(getActivity(), query);

                updateItems();
//
//                //hide soft keyboard
//                mSearchView.clearFocus();
////                hideSoftKeyBoard(getActivity());

                //collapse search view
                mSearchView.onActionViewCollapsed();
                return true; //search request has been handled.
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Called whenever text is changed in the search box.
//                Log.d(TAG, "onQueryTextChange " + newText);
                return false;
            }

        });
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //pre-populate search view with previous query (if any)
                String query = QueryPreferences.getStoredQuery(getActivity());
                mSearchView.setQuery(query, false); //2nd arg indicates if query should be submitted
            }

        });

        MenuItem togglePolling = menu.findItem(R.id.menu_item_toggle_polling);
        if (PollService.isServiceAlarmOn(getActivity())) {
            //alarm on, show text to STOP polling
            togglePolling.setTitle(R.string.stop_polling);

        } else {
            //alarm off, show text to START polling
            togglePolling.setTitle(R.string.start_polling);

        }//set title to stop/start polling

    }//onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_clear:
                //Selected "clear search", clear query from SharedPreferences (set to null)
                QueryPreferences.setStoredQuery(getActivity(), null);
                //clear query text in SearchView
                mSearchView.setQuery(QueryPreferences.getStoredQuery(getActivity()), false);

//                //hide soft keyboard
//                mSearchView.clearFocus();

                //collapse search view - onActionViewCollapsed also clears focus.
                mSearchView.onActionViewCollapsed();
//                getActivity().invalidateOptionsMenu();

                updateItems();
                return true;

            case R.id.menu_item_toggle_polling:

                //Flag indicating alarm should be started if it's not on:
                boolean shouldStartAlarm = !PollService.isServiceAlarmOn(getActivity());
                PollService.setServiceAlarm(getActivity(), shouldStartAlarm);

                //Update toolbar to refresh polling text appropriately:
                getActivity().invalidateOptionsMenu();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }//onOptionsItemSelected

    @Override
    public void onDestroy() {
        super.onDestroy();
        //destroy the message-loop for retrieving thumbnails
        mThumbnailDownloader.quit();
        Log.i(TAG, "Background thread destroyed");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mThumbnailDownloader.clearQueue();
    }

    /**
     * Wrapper used to update the model collection displayed in the RecyclerView
     * based on the user's previous search query, or null if cleared/not yet entered.
     */
    private void updateItems() {
        //NOTE: this also runs before onViewCreated finishes

        if(mItems.size() != 0){
            //reset adapter data if it already has anything
            mItems.clear();
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }

        String query = QueryPreferences.getStoredQuery(getActivity());
        new FetchItemsTask(query, this).execute();
    }//updateItems

    /**
     * Creates a {@link PhotoAdapter} instance and sets it to the RecyclerView
     * every time a new RecyclerView is created, or the model collection changes.
     */
    private void setUpAdapter() {

        /*
            Model collection can change in a worker thread; This is also called
            from the worker thread's callback method: FetchItemsTask.onPostExecute(...)

            Therefore we have to make sure that the fragment is attached to an activity
            whenever the RecyclerView is to be updated with an adapter.
         */

        if (isAdded()) {
            //attached to host activity, getActivity() won't be null
            mRecyclerView.setAdapter(new PhotoAdapter(mItems));
        }//attached to host activity?

    }//setUpAdapter

    private class PhotoHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
        /** Recall: Everything to do with ViewHolders is encapsulated here, including onClicks! **/

        private ImageView mImageView;
        private GalleryItem mGalleryItem;

        public PhotoHolder(View itemView) {
            super(itemView);
            cacheViews();
        }

        private void cacheViews() {

            if (!(itemView instanceof ImageView)) {
                //Passed in wrong rootView for grid-item
                throw new IllegalArgumentException("ERROR: Inflated wrong layout for grid-item!");
            } else {
                //Correctly inflated an ImageView:

                //Set OnClickListener for whole item view:
                itemView.setOnClickListener(this);

                //Cache itemView's child view(s):
                mImageView = (ImageView) itemView
                        .findViewById(R.id.fragment_photo_gallery_image_view);
            }//valid rootView?

        }//cacheViews

        public void bindViewContent(Drawable drawable) {
            mImageView.setImageDrawable(drawable);
        }//bindViewContent

        /**
         * Binds a GalleryItem with the ViewHolder used to display it's data.
         * @param galleryItem GalleryItem to bind.
         */
        public void bindGalleryItem(GalleryItem galleryItem){
            this.mGalleryItem = galleryItem;
        }

        @Override
        public void onClick(View v) {
            //Create implicit intent to launch mGalleryItem's Flickr web-page

            //NOTE: This can open a web-browser for the photo's Flickr page even if the thumbnail
            //      has NOT yet been displayed!

            /** Using implicit intents & a web browser app: **/
//            Intent i = new Intent();
//            i.setAction(Intent.ACTION_VIEW);
//            i.setData(mGalleryItem.getPhotoPageUri());//Uri for data to view (ie: Web-Page uri)
////            i.setType(""); //MIME type; don't need?
////            Intent iChooser = Intent.createChooser(i, "Chooser Title Here");
//
//            //Verify device has a web-browser installed (intent can be executed)
//            if(i.resolveActivity(getActivity().getPackageManager()) != null){
//                //Intent can be resolved:
//                startActivity(i);
//            } else {
//                Log.i(TAG, "Device does't have web-browser!");
//            }

            /** Using a WebView to display web content directly in app **/
            Intent i = PhotoPageActivity.newIntent(getActivity(), mGalleryItem.getPhotoPageUri());
            startActivity(i);

        }//onClick

    }//PhotoHolder

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder> {

        private List<GalleryItem> mGalleryItems;
        private int mPreloadNeighbours; //default is 10

        /**
         * Instantiates a new PhotoAdapter with default number of 10 neighbouring thumbnails
         * to pre-load.
         *
         * @param galleryItems model collection this adapter will bind to views.
         */
        public PhotoAdapter(List<GalleryItem> galleryItems) {
            this(galleryItems, 10);
        }

        /**
         * Instantiates a new PhotoAdapter, specifying both model collection and number of
         * neighbouring thumbnails to preload.
         *
         * @param galleryItems      model collection this adapter will bind to views.
         * @param preloadNeighbours number of neighbouring thumbnails to preload.
         */
        public PhotoAdapter(List<GalleryItem> galleryItems, int preloadNeighbours) {
            this.mGalleryItems = galleryItems;
            this.mPreloadNeighbours = preloadNeighbours;
        }//PhotoAdapter

        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View photoHolderRootView = inflater.inflate(R.layout.gallery_item, parent, false);
            return new PhotoHolder(photoHolderRootView);

        }//onCreateViewHolder

        @Override
        public void onBindViewHolder(PhotoHolder holder, int position) {
//            Log.d(TAG, "onBindViewHolder @ position: " + position);

            //Get a reference to the GalleryItem at this position in the List:
            GalleryItem galleryItem = mGalleryItems.get(position);

            //Bind gallery item to the ViewHolder used to display it:
            holder.bindGalleryItem(galleryItem);

            /*
                tmp placeHolder that's shown each time a view is displayed.
                RECALL: that onBindViewHolder is called each time a RecyclerView
                        needs to populate a View with data!

               -> Need this or else recycled view will have old thumbnail (if any)
             */
            Drawable placeHolder = ContextCompat
                    .getDrawable(getActivity(), R.drawable.image_place_holder);
            holder.bindViewContent(placeHolder);

            /*
                Place a message into the messageQueue of the background worker
                 so it may retrieve a thumbnail for THIS GalleryItem,
                 when it gets a chance; this action doesn't happen instantly! It's QUEUED!
             */

            //Start pre-loading X neighbouring thumbnails of this position
            mThumbnailDownloader.preloadThumbnails(getUrlsToPreLoad(position));

            //While pre-loading neighbouring thumbnails, load this position's thumbnail.
            mThumbnailDownloader.queueThumbnail(holder, galleryItem.getUrl());

        }//onBindViewHolder

        @Override
        public int getItemCount() {
            return mGalleryItems.size();
        }

        private List<String> getUrlsToPreLoad(int position) {

            List<String> urlsToPreLoad = new ArrayList<>();

            if (position < mPreloadNeighbours) {

//            Log.d(TAG, "Case0");

                //Case0: collection @position has less entries before it than #preloadNeighbours
                for (int i = 0; i <= position + mPreloadNeighbours; i++) {
                    if (i != position) {
                        urlsToPreLoad.add(mGalleryItems.get(i).getUrl());
                    }
                }

            } else if (mGalleryItems.size() <= position + mPreloadNeighbours) {

//            Log.d(TAG, "Case1");

                //Case1: collection @position has less entries after it than #preloadNeighbours
                for (int i = position - mPreloadNeighbours; i < mGalleryItems.size(); i++) {
                    if (i != position) {
                        urlsToPreLoad.add(mGalleryItems.get(i).getUrl());
                    }
                }

            } else {

//            Log.d(TAG, "Case2");

                //Case2: collection @position has more entries both ways than #preloadNeighbours
                for (int i = position - mPreloadNeighbours; i <= position + mPreloadNeighbours; i++) {
                    if (i != position) {
                        urlsToPreLoad.add(mGalleryItems.get(i).getUrl());
                    }
                }

            } //if-else (location of position relative to collection size)

            return urlsToPreLoad;

        }//getUrlsToPreLoad

    }//PhotoAdapter

    //AsyncTask to fetch data over the network
    private class FetchItemsTask extends AsyncTask<Void, Void, List<GalleryItem>> {

        /*
            AsyncTask's generic types: <Params, Progress, Result>
                (see: https://developer.android.com/reference/android/os/AsyncTask.html)
            - Params: type of parameter sent to background computation
            - Progress: type of progress units published during background computation
            - Result" type of the result of the background computation
         */

        private String mQuery;
        private PhotoGalleryFragment mPhotoGalleryFragment;

        public FetchItemsTask(String query, PhotoGalleryFragment fragment) {
            this.mQuery = query;
            this.mPhotoGalleryFragment = fragment;
        }//FetchItemsTask c'tor

        @Override
        protected void onPreExecute() {
            /* (OPTIONAL)
                Do nothing; used to setup background task - setup progress bar, etc..
                Note: this is on main UI thread, has access to UI
             */

            //NOTE: This (usually) runs before onCreateView returns; UI elements might be null
            if(mPhotoGalleryFragment.isResumed()){
                //Ensures UI elements are not null because onCreateView must have finished
                showProgressBar();
            }


        }//onPreExecute

        @Override
        protected List<GalleryItem> doInBackground(Void... voids) {
            //Work to do in a worker thread; no access to UI (REQUIRED)

            if (mQuery == null) {
                return new FlickrFetchr().fetchRecentPhotos();
            } else {
                return new FlickrFetchr().searchPhotos(mQuery);
            }//if-else (search query empty?)

        }//doInBackground

        @Override
        protected void onPostExecute(List<GalleryItem> updatedCollection) {
            super.onPostExecute(updatedCollection);
            /*
                doInBackground completed, present result to user if necessary. (OPTIONAL)
                NOTE: This runs on the main UI-Thread; has access to UI elements.

                IMPORTANT:
                    Need to ensure that the fragment is attached to host activity if
                    this callback updates the UI in any way (deals with Views)
                    (Thi is done in the setUpAdapter() method this callback invokes)
             */

            //Update reference to model collection
            mItems = updatedCollection;

            //Set progress bar to be invisible
            hideProgressBar();

            setUpAdapter();

        }//onPostExecute

    }//FetchItemsTask

    /**
     * Custom ItemDecoration for a RecyclerView displaying images using {@link GridLayoutManager};
     * draws over the grid to provide borders between views.
     * <p>
     * IMPORTANT:
     * This also has to implement "getItemOffsets" to offset each
     * child by divider width so the divider isn't drawn over actual child content.
     * (test with a thick divider)
     */
    private class GridBorderDecoration extends RecyclerView.ItemDecoration {

        private Paint gridDivider; //See documentation about Pain objects.

        /**
         * Use to instantiate a new {@link GridBorderDecoration} with a default
         * hairline thickness (1px).
         */
        public GridBorderDecoration() {
            gridDivider = new Paint(Paint.ANTI_ALIAS_FLAG);
            gridDivider.setColor(ContextCompat.getColor(getActivity(), R.color.colorBlack12Opacity));
            gridDivider.setStyle(Paint.Style.STROKE);
            gridDivider.setStrokeWidth(0.0f); //hairline thickness (1dp) as recommended
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDraw(c, parent, state);

            RecyclerView.LayoutManager lm = parent.getLayoutManager();

            //Get # of columns and rows in the Grid; the MODULO
            int gridColSpanCount = ((GridLayoutManager) lm).getSpanCount();

            int gridColRightEdge = gridColSpanCount - 1;

            for (int childIndex = 0; childIndex < parent.getChildCount(); childIndex++) {
                //for each child, draw divider:
                View childView = lm.getChildAt(childIndex);

                //RECALL: Get index of grid columns based on the modulus of the modulo
                int gridColIndex = childIndex % gridColSpanCount; //grid column of this childView
                if (gridColIndex != gridColRightEdge) {
                    /*
                        draw a straight line " | " down the right-hand side
                        of the columns (except far right one)
                     */

                    //right-hand side of the column
                    c.drawLine(
                            lm.getDecoratedRight(childView), //startX
                            lm.getDecoratedTop(childView), //startY
                            lm.getDecoratedRight(childView), //stopX
                            lm.getDecoratedBottom(childView), //stopY
                            gridDivider
                    );

                }//if non-right-edge column

                //draw a straight line divider " --- " below all views
                c.drawLine(
                        lm.getDecoratedLeft(childView), //startX
                        lm.getDecoratedBottom(childView), //startY
                        lm.getDecoratedRight(childView), //stopX
                        lm.getDecoratedBottom(childView), //stopY
                        gridDivider
                );

            }//for each child in the RecyclerView

        }//onDrawOver

    }//GridBorderDecoration

    /**
     * Inner class that may be used to provide padding between each item view of
     * a recycler view using a grid layout manager.
     */
    private class ItemOffsetDecorator extends RecyclerView.ItemDecoration {

        private static final int GRID_LIST_ITEM_PADDING = 4; // need to convert to dp (see below)

        public ItemOffsetDecorator() {
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            RecyclerView.LayoutManager lm = parent.getLayoutManager();
            int gridColSpanCount = ((GridLayoutManager) lm).getSpanCount();
            int gridColRightEdge = gridColSpanCount - 1;
            int childViewPosition = parent.getChildAdapterPosition(view);
            int childViewCol = childViewPosition % gridColSpanCount;
            int childViewRow = childViewPosition / gridColSpanCount; //int division auto floors.

            //Add horizontal padding
            if (childViewRow == 0) { //first row
                //full padding on top AND bottom
                outRect.top = setPadding();
                outRect.bottom = setPadding();
            } else {//not first row
                //full padding bottom
                outRect.bottom = setPadding();
            }

            //Add vertical padding
            if (childViewCol == 0) {//first column child views
                //add full padding on left, half on the right
                outRect.left = setPadding();
                outRect.right = setPadding() / 2;
            } else if (childViewCol == gridColRightEdge) { //last column child views
                //add full padding on right, half on the left
                outRect.right = setPadding();
                outRect.left = setPadding() / 2;
            } else { // middle column child views
                //half padding on left and right
                outRect.left = setPadding() / 2;
                outRect.right = setPadding() / 2;
            }

        }//getItemOffsets

        /**
         * Convenience method to set padding of a child view, in dp.
         *
         * @return int equivalent of appropriate padding value (in dp) as per Material Design.
         */
        private int setPadding() {
            return (int) (GRID_LIST_ITEM_PADDING * getResources().getDisplayMetrics().density);
        }

    }//ItemOffsetDecorator

}//PhotoGalleryFragment
