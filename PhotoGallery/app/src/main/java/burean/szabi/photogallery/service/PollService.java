package burean.szabi.photogallery.service;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;

import burean.szabi.photogallery.R;
import burean.szabi.photogallery.controller.activities.PhotoGalleryActivity;
import burean.szabi.photogallery.model.GalleryItem;
import burean.szabi.photogallery.utils.FlickrFetchr;
import burean.szabi.photogallery.utils.QueryPreferences;

/**
 * A service that polls for search results in the background; upon available results,
 * the user will get a notification in the status bar.
 */
public class PollService extends IntentService {

    private static final String TAG = PollService.class.getSimpleName();

    private static final int POLL_INTERVAL = 1000 * 60; // 60 seconds min on API 22+ (Android 5.1) //fixme: 60 second interval for testing only
    private static final int REQ_PI_POLL_SERVICE = 0; //sender requestCode for PendingIntent
    private static final int NOTIFICATION_ID_POLL_SERVICE = 1; //notification ID from pollService

    //Expose constants for broadcast intent:
    public static final String B_INTENT_EXTRA_NOTFN = "notification";
    public static final String B_INTENT_EXTRA_NOTFN_REQ_CODE = "notificationRequestCode";

    //Broadcast Intent actions:
    public static final String ACTION_SHOW_NOTIFICATION =
            PollService.class.getName() + ".SHOW_NOTIFICATION";

    //Custom permissions:
    public static final String PERMISSION_PRIVATE =
            "burean.szabi.photogallery.permission.PRIVATE";

    public static Intent newIntent(Context context) {
        return new Intent(context, PollService.class);
    }//newIntent

    /**
     * Schedules this service to be ran continuously at some interval until cancelled.
     * NOTE: Expected behaviour is that if the application if force closed via task manager
     * (task overview), then any registered alarms via AlarmManager.set... are REMOVED!
     *
     * @param context Context of component starting this service.
     * @param isOn    Flag to indicate if service should be started or cancelled.
     */
    public static void setServiceAlarm(Context context, boolean isOn) {

        //Intent to package up as PendingIntent to be sent to AlarmManager
        Intent i = PollService.newIntent(context);

        /* NOTE:
            - getService(..) packages up invocation of Context.startService(Intent).
            - returns a TOKEN to the PendingIntent living in the OS; owned by this application.
            - creation of a PendingIntent is IDEMPOTENT for
                identical intents: see Intent.filterEquals() for Intent comparisons.
         */
        PendingIntent pi = PendingIntent.getService(
                context, // context with which to send intent
                REQ_PI_POLL_SERVICE, // request code for setting THIS alarm
                i, // intent to package
                0 // Extra flags
        );

        //Get a reference to the system AlarmManager:
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        //schedule the PendingIntent to be toggled at some interval
        if (isOn) {
            // turn alarm on
            alarmManager.setInexactRepeating(
                    AlarmManager.ELAPSED_REALTIME, // time basis for alarm
                    SystemClock.elapsedRealtime(), // when to start alarm
                    POLL_INTERVAL, // time interval to repeat alarm
                    pi // PendingIntent for alarm to fire
            );
            //NOTE: Predefined interval used to ensure inexact repeating on Pre-API 19 devices.

            /* NOTE:
                Multiple calls to AlarmManager.setInexactRepeating(..) /w same pending intent
                (same request ID) will override the last alarm set.
             */

        } else {
            // turn alarm off
            alarmManager.cancel(pi);
            pi.cancel(); //ALSO cancel the PendingIntent.

        }//if else (toggle PendingIntent)

        //Persist alarm state
        QueryPreferences.setAlarmOn(context, isOn);

    }//setServiceAlarm

    public static boolean isServiceAlarmOn(Context context) {

        //Get a reference to the Token for an intent, but we DON'T create it if doesn't exist!
        PendingIntent pi = PendingIntent.getService(
                context,
                0,
                PollService.newIntent(context),  //same intent as original, idempotent call
                PendingIntent.FLAG_NO_CREATE //DON'T create token if it doesn't already exist.
        );

        return (pi != null);

    }//isServiceAlarmOn

    public PollService() {
        super(TAG);
    }//PollService - constructor

    @Override
    protected void onHandleIntent(Intent intent) {

        /* ON BACKGROUND THREAD */

        /*
            - This runs whenever the IntentService received a command (Intent).
            - It will be placed into the service queue, and then handled
            in this onHandleIntent method once the command is at the front of the queue.
         */

        //First have to make sure background service has NW access:
        if (!isNetworkAvailableAndConnected()) {
            Log.e(TAG, "No background network access");
            return;
        }

//        Log.i(TAG, "Received an intent: " + intent);

        //Get search query and last result ID (if any)
        String query = QueryPreferences.getStoredQuery(this);
        String lastResultId = QueryPreferences.getLastResultId(this);
        List<GalleryItem> items;

        //Fetch list of items from Flickr based on query:
        if (query == null) {
            //no search query, return recent photos
            items = new FlickrFetchr().fetchRecentPhotos();
        } else {
            //have search query, use to get specific photos
            items = new FlickrFetchr().searchPhotos(query);
        }

        //Check to see if we have any results:
        if (items.size() == 0) {
            Log.i(TAG, "no results found!");
            return;
        }

        /*
            NOTE: We check the first element of the collection and compare it with the old
                    stored ID b/c this represents one "page" of results from flickr.
                    If this ID is different than what we have stored, then obvs new photos
                    were uploaded to flickr (based on most recent / search query), so we
                    have new photos to view.
            IMPORTANT: Currently, these searched results aren't passed back to the
                        photo gallery fragment. When the user opens it, they are re-downloaded.
                        (Obviously very inefficient)
         */

        //We have results, grab first one & check if same as last one:
        String resultId = items.get(0).getPhotoId();
        if (resultId.equals(lastResultId)) {
            //same as last one
            Log.i(TAG, "Got an old result: " + resultId);
        } else {
            //different from last one
            Log.i(TAG, "Got an new result: " + resultId);

            //Store latest search result ID
            QueryPreferences.setLastResultId(this, resultId);

            //Create a notification to notify user that a new result is ready:

            //First get variables to resources:
            Resources resources = getResources();
            Intent i = PhotoGalleryActivity.newIntent(this); // intent to wrap into PI & start
            PendingIntent pi = PendingIntent.getActivity( // get PI token to start an activity.
                    this, //context of PI creator
                    REQ_PI_POLL_SERVICE, //request code
                    i, // EXPLICIT intent to wrap with PI
                    0 //flags
            );

            //Build notification
            Notification notification = new NotificationCompat.Builder(this)
                    .setTicker(resources.getString(R.string.new_pictures_title)) //pre API 19
                    .setSmallIcon(android.R.drawable.ic_menu_report_image) //built-in icon
                    .setContentTitle(resources.getString(R.string.new_pictures_title)) //title
                    .setContentText(resources.getString(R.string.new_pictures_text))
                    .setContentIntent(pi) //PendingIntent to fire when notification clicked
                    .setAutoCancel(true) //dismiss notification once user touches it
                    .build();

            /*
                - Send ordered broadcast indicating new results are ready.
                - To be handled by an artificial result receiver & post notification if
                app is not in the foreground (visible).
             */
            showBackgroundNotification(NOTIFICATION_ID_POLL_SERVICE, notification);

        }//if-else (new result?)

    }//onHandleIntent

    /**
     * Package up a notification invocation and send it out as an ordered broadcast.
     */
    private void showBackgroundNotification(int requestCode, Notification notification) {

        /*
            - Send a custom broadcast intent to notify other components
            of this app once new search results are available.
            - Uses custom permission: PERMISSION_PRIVATE to enforce that only
            components of this application may receive the broadcast.

            NOTE: This is intended to be received ONLY while the
                    application is running in the foreground, and prevent
                    the notification from being sent to the status bar.
                  (user is already actively interacting with application)
         */

        Intent iBroadcastIntent = new Intent();
        iBroadcastIntent.setAction(ACTION_SHOW_NOTIFICATION);
        iBroadcastIntent.putExtra(B_INTENT_EXTRA_NOTFN_REQ_CODE, requestCode); //notification ID
        iBroadcastIntent.putExtra(B_INTENT_EXTRA_NOTFN, notification); //notification to send
        sendOrderedBroadcast( // use this method version to provide initial values & resultReceiver

                iBroadcastIntent, // intent to broadcast
                PERMISSION_PRIVATE, // broadcast permission receivers must hold to receive this
                null, // ResultReceiver, use artificial one instead.
                null, // Handler with which to schedule onReceive(...) null = main thread's.

                /** These next three parameters are for the BROADCAST! Not the intent **/

                Activity.RESULT_OK, // initial result code, usually Activity.RESULT_OK
                null, // initial Data
                null // initial Extras (Bundle)

        );

        /* NOTE:
            - We use an artificial ResultReceiver (.receiver.NotificationReceiver) because if
            we register one in the sendOrderedBroadcast(..) call, it will die along with
            PollService after the broadcast has been sent.
            [Recall that sendOrderedBroadcast(..) is asynchronous! see docs.]
            - So to ensure broadcast is handled at the end of the sequence, write a standalone
            receiver, and alter it's priority in the Manifest.xml to the lowest of all
            registered receivers in the sequence, or as needed.
         */

    }//showBackgroundNotification

    private boolean isNetworkAvailableAndConnected() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        /* NOTE:
            Call to ConnectivityManager.getActiveNetworkInfo returns null if:
             - no default Network available on the device.
             - (for background services) background networking is disabled for application.

             Also note, .getActiveNetworkInfo() required ACCESS_NETWORK_STATE normal-permission.

         */
        boolean isAvailable = (cm.getActiveNetworkInfo() != null);
        boolean isConnected = cm.getActiveNetworkInfo()
                .isConnected(); //returns false if not connected to network.

        return isAvailable && isConnected;

    }//isNetworkAvailableAndConnected

}//PollService
