package burean.szabi.photogallery.controller.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import burean.szabi.photogallery.R;
import burean.szabi.photogallery.controller.VisibleFragment;

/**
 * Fragment that displays a WebView. Pass in URL to display as an argument.
 */
public class PhotoPageFragment extends VisibleFragment {

    private static final String TAG = PhotoPageFragment.class.getSimpleName();
    private static final String ARG_URI = "argUri";

    private WebView mWebView;
    private Uri mUri;
    private ProgressBar mProgressBar;

    public static PhotoPageFragment newInstance(Uri uri) {

        Bundle args = new Bundle();
        args.putParcelable(ARG_URI, uri);
        PhotoPageFragment fragment = new PhotoPageFragment();
        fragment.setArguments(args);
        return fragment;

    }//newInstance

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Retrieve Uri for web page to display from args:
        mUri = getArguments().getParcelable(ARG_URI);

    }//onCreate

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        final View fragRootView = inflater.inflate(R.layout.fragment_photo_page, container, false);

        mWebView = (WebView) fragRootView
                .findViewById(R.id.fragment_photo_page_web_view);
        mProgressBar = (ProgressBar) fragRootView
                .findViewById(R.id.fragment_photo_page_progress_bar);

        //Step1: Enable JavaScript (NOT always required, but required by Flickr)
        mWebView.getSettings().setJavaScriptEnabled(true);

        //Step2: Override WebViewClient.shouldOverrideUrlLoading(...) to return false:
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return handleRequest(url);
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                /*WebViewClient is an event interface; allows to respond to rendering events.*/

                /* NOTE: This method determined what happens when a new URL is loading into WebView

                    - returning false: indicates to load the URL in the WebView
                    - returning true: host application will handle URL, WebView won't load it.

                 */

                //If we don't override this, default implementation sends an implicit intent to
                //  a browser application - not what we want.

                return handleRequest(request.getUrl().toString());
            }

            private boolean handleRequest(String url) {

                //Get a uri from the string
                Uri uri = Uri.parse(url);

                //Get the uri scheme:
                String scheme = uri.getScheme();

                if (scheme.equals("https") || scheme.equals("http")) {
                    //Web page request: display in WebView (WebView will handle it)
                    return false;
                } else {
                    //Fire of an intent to let appropriate application handle it:
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_VIEW); //view the data at Uri
                    i.setData(uri); //the Uri
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    return true;
                } //if-else (Scheme http/https)?

            }//handleRequest

        });

        /*
            An event interface for reacting to things that should change elements
            AROUND the browser (subtitle, progress bar, etc...)
         */
        mWebView.setWebChromeClient(new WebChromeClient() {

            //Notice how progress updates and title have own callback methods:

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                //Reacts to progress event and changes element of chrome around the browser?
                if (newProgress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(newProgress);
                }
            }//onProgressChanged

            @Override
            public void onReceivedTitle(WebView view, String title) {
                //Sets the action's bar subtitle to the web page's title once it's available
                AppCompatActivity activity = (AppCompatActivity) getActivity();
                ActionBar actionBar = activity.getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setSubtitle(title);
                }
            }//onReceivedTitle
        });

        //Step3: specify URI to load [this has to be done AFTER configuring WebView]
        mWebView.loadUrl(mUri.toString());

        return fragRootView;

    }//onCreateView

    /**
     * Used to see if associated WebView contains pages to go back to
     *
     * @return Boolean flag.
     */
    public boolean canWebViewGoBack() {
        return mWebView.canGoBack();
    }//canWebViewGoBack

    /**
     * Used to take user back to previous page they were browsing on the WebView.
     * NOTE: Caller's responsibility to ensure that the WebView can go back.
     */
    public void webViewGoBack() {
        mWebView.goBack();
    }//webViewGoBack

}//PhotoPageFragment
