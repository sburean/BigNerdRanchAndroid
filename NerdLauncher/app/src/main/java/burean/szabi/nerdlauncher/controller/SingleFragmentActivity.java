package burean.szabi.nerdlauncher.controller;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import burean.szabi.nerdlauncher.R;

/**
 * Generic template activity that hosts a single fragment. Hook into it to
 * instantiate whichever fragment the concrete activity wants tho host.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId()); //hook for layout to inflate

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            //Fragment not hosted
            fm.beginTransaction()
                    .add(R.id.fragment_container, getFragmentInstance())
                    .commit();
        }

    }

    /**
     * Implement to specify which fragment should be hosted by a
     * concrete subclass of this activity.
     *
     * @return Fragment instance to be hosted.
     */
    protected abstract Fragment getFragmentInstance();

    /**
     * Hook for layout to inflate
     *
     * @return Resource Id of layout to inflate.
     */
    @LayoutRes
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

}
