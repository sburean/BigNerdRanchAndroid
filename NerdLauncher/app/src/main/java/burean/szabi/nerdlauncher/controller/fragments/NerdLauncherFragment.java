package burean.szabi.nerdlauncher.controller.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import burean.szabi.nerdlauncher.R;

/**
 * Blah - todo.
 */
public class NerdLauncherFragment extends Fragment {

    private static final String TAG = NerdLauncherFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;

    public static NerdLauncherFragment newInstance() {

        Bundle args = new Bundle();
        //add args here as needed
        NerdLauncherFragment fragment = new NerdLauncherFragment();
        fragment.setArguments(args);
        return fragment;

    }//newInstance

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_nerd_launcher, container, false);

        mRecyclerView = (RecyclerView) rootView
                .findViewById(R.id.fragment_nerd_launcher_recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(llm);
        setupRecyclerViewAdapter();

        //Add a divider between items of the RecyclerView:
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(
                        mRecyclerView.getContext(),
                        llm.getOrientation())
        );

        return rootView;

    }//onCreateView

    /**
     * Used to create a RecyclerView.Adapter<VH> instance and attach it
     * to this fragment's RecyclerView reference.
     * Queries all launchable activities on the current device to use as the
     * adapter's data.
     */
    private void setupRecyclerViewAdapter() {

        /*
            Implicit intent to determine how many activities on device
            can resolve the MAIN action and belong to the LAUNCHER category:
         */
        Intent iiMainActivities = new Intent();
        iiMainActivities.setAction(Intent.ACTION_MAIN);
        iiMainActivities.addCategory(Intent.CATEGORY_LAUNCHER);

        //Get a list of ResolutionInfo; all (activities) that can resolve the intent.
        PackageManager pm = getActivity().getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(iiMainActivities, 0);
        /*
            FIXME: have to make sure ResolveInfo actually contains activity info:
            ie: ResolveInfo.activityInfo is non null, and
                    ResolveInfo.serviceInfo & ResolveInfo.providerInfo IS NULL!

            From documentation, exactly one of .activityInfo, .serviceInfo,
            or .providerInfo is non-null!

         */

        //Sort ResolveInfo list alphabetically by label - define custom comparator:
        Collections.sort(activities, new Comparator<ResolveInfo>() {
            @Override
            public int compare(ResolveInfo o1, ResolveInfo o2) {
                /*
                    Defines comparison between two ResolveInfo objects;
                    To sort alphabetically, o1 < o2 if o1's label < o2's label, etc..
                    Delegate comparison to match a String comparison, ignoring case:
                 */

                PackageManager pmInner = getActivity().getPackageManager();
                String label1 = o1.loadLabel(pmInner).toString();
                String label2 = o2.loadLabel(pmInner).toString();
                return label1.compareToIgnoreCase(label2);
            }
        });

        Log.i(TAG, "Found " + activities.size() + " activities.");

        //Create and attach adapter to our RecyclerView:
        mRecyclerView.setAdapter(new ActivityAdapter(activities));

    }//setupRecyclerViewAdapter

    private class ActivityHolder extends RecyclerView.ViewHolder
                                implements View.OnClickListener {

        private ResolveInfo mResolveInfo;
        private TextView mNameTextView;
        private ImageView mIconImageView;

        public ActivityHolder(View itemView) {
            super(itemView);

            if(!(itemView instanceof ViewGroup)){
                throw new IllegalArgumentException(
                        "ERROR: Expecting a different itemView in ViewHolder"
                );
            } else {
                cacheViews(itemView);
            }//if-else(itemView as expected?)

        }//ActivityHolder-Ctor

        private void cacheViews(View rootView){

            rootView.setOnClickListener(this); //click listener on whole list-row.
            mNameTextView = (TextView) rootView.findViewById(R.id.list_activity_info_text);
            mIconImageView = (ImageView) rootView.findViewById(R.id.list_activity_info_icon);

        }//cacheViews

        public void bindViews(ResolveInfo resolveInfo){

            //Store reference to the ResolveInfo we're using as our model
            mResolveInfo = resolveInfo;

            PackageManager pmBindViews = getActivity().getPackageManager();

            //Activity's in ResolveInfo for MAIN/LAUNCHER is most-likely app name.
            String appName = mResolveInfo.loadLabel(pmBindViews).toString();
            Drawable appIconDrawable = mResolveInfo.loadIcon(pmBindViews);
            mNameTextView.setText(appName);
            mIconImageView.setImageDrawable(appIconDrawable);

        }//bindViews

        @Override
        public void onClick(View v) {
            //Create an explicit intent to launch the clicked activity:

            Intent i = new Intent();
            i.setAction(Intent.ACTION_MAIN); //intent should start at main entry point

            //Define a ComponentName object to start. (package name & class name inside package)
            ComponentName comp = new ComponentName(
                    mResolveInfo.activityInfo.applicationInfo.packageName, //application pkg
                    mResolveInfo.activityInfo.name //activity name of ResolveInfo; within above pkg!
            );
            //alternatively use convenience method on the Intent: .setClassName(same args).

            //EXPLICITLY state component to HANDLE this intent
            i.setComponent(comp);

            //Start the activity in it's own task so it runs as a separate application
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //Start the clicked activity
            startActivity(i);

        }//onClick

    }//ActivityHolder

    private class ActivityAdapter extends RecyclerView.Adapter<ActivityHolder> {

        private List<ResolveInfo> mResolveInfoList;

        public ActivityAdapter(List<ResolveInfo> resolveInfoList) {
            mResolveInfoList = resolveInfoList;
        }

        @Override
        public ActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View viewHolderRootView = LayoutInflater.from(getActivity())
                    .inflate(R.layout.list_activity_info, parent, false);
            return new ActivityHolder(viewHolderRootView);

        }//onCreateViewHolder

        @Override
        public void onBindViewHolder(ActivityHolder holder, int position) {
            holder.bindViews(mResolveInfoList.get(position));
        }//onBindViewHolder

        @Override
        public int getItemCount() {
            return mResolveInfoList.size();
        }//getItemCount

    }//ActivityAdapter

}
