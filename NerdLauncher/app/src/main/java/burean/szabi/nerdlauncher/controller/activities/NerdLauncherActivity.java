package burean.szabi.nerdlauncher.controller.activities;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import burean.szabi.nerdlauncher.controller.SingleFragmentActivity;
import burean.szabi.nerdlauncher.controller.fragments.NerdLauncherFragment;

public class NerdLauncherActivity extends SingleFragmentActivity {

    private static final String TAG = NerdLauncherActivity.class.getSimpleName();

    @Override
    protected Fragment getFragmentInstance() {
        return NerdLauncherFragment.newInstance();
    }

}
