package burean.szabi.geoquiz.controllers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import burean.szabi.geoquiz.R;

public class CheatActivity extends AppCompatActivity {

    private static final String TAG = CheatActivity.class.getSimpleName();

    /*
        Activities may be started from many places, so define keys for extras on
        the activities that retrieve them!

        IMPORTANT:
            Use package name as qualifier to
             prevent collisions with extras from other apps.
     */
    private static final String INTENT_KEY_ANSWER_BOOL =
            CheatActivity.class.getName() + "answer_boolean";
    private static final String INTENT_KEY_ANSWER_SHOWN =
            CheatActivity.class.getName() + "answer_shown";
    private static final String BUNDLE_KEY_ANSWER_SHOWN = "answer_shown";

    private TextView mAnswerTextView, mApiLevelTextView;
    private Button mShowAnswer;
    private boolean mAnswerIsTrue;
    private boolean mWasAnswerShown; //keep track of state for config. changes

    /**
     * No reason for any other code in application to know the implementation details of <br>
     * what this activity expects as extras on it's Intent. <p>
     * Instead, we encapsulate that information within (overloaded / VARARGS) methods <br>
     * that create a proper intent with extras we need.
     *
     * @param packageContext: context that requests the intent
     * @param answerIsTrue:   extra information; whether the question answer is T/F
     * @return An explicit intent to start this activity.
     */
    public static Intent newIntent(Context packageContext, boolean answerIsTrue) {
        Intent i = new Intent(packageContext, CheatActivity.class);
        i.putExtra(INTENT_KEY_ANSWER_BOOL, answerIsTrue);
        return i;
    }

    /**
     * Since the result intent that was passed back to the parent activity contained a key <br>
     * defined in this class (CheatActivity.java), we create another method such that other <br>
     * activities may access the result, without having to expose the intent extras.<p>
     *
     * @param result: intent returned from a started child activity.
     * @return T/F indicating whether the answer was shown.
     */
    public static boolean wasAnswerShown(Intent result) {
        return result.getBooleanExtra(INTENT_KEY_ANSWER_SHOWN, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        //NOTE: 2nd arg of get<T>Extra is the default value if key is not found.
        mAnswerIsTrue = getIntent().getBooleanExtra(INTENT_KEY_ANSWER_BOOL, false);

        mAnswerTextView = (TextView) findViewById(R.id.answer_text_view);
        mApiLevelTextView = (TextView) findViewById(R.id.api_version_text_view);
        mShowAnswer = (Button) findViewById(R.id.show_answer_button);

        mShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAnswerText(mAnswerIsTrue);
                setAnswerShownResult(true);
                mWasAnswerShown = true;


                /**
                    The ViewAnimationUtils and .createCircularReveal was introduced in
                    API21, which is within bounds of our compileSdkVersion, but it is
                    ABOVE our minSdkVersion! (API16) - (See compiler has no issues with code)

                    -> Therefore we need to add compatibility support for these two calls,
                       because if this app now runs on devices below API21, it will crash!
                       (and we say our app can run on devices running Android API16 and above)

                    -> We WRAP higher-API level code in a conditional that checks the
                       device's version of android!
                 */
/* INFO only:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //Run this code ONLY if device is running Android API 21 or greater.

                    int cx = mShowAnswer.getWidth() / 2;
                    int cy = mShowAnswer.getHeight() / 2;
                    float radius = mShowAnswer.getWidth();
                    Animator anim = ViewAnimationUtils
                            .createCircularReveal(mShowAnswer, cx, cy, radius, 0);
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mAnswerTextView.setVisibility(View.VISIBLE);
                            mShowAnswer.setVisibility(View.INVISIBLE);
                        }
                    });
                    anim.start();

                } else {

                    mAnswerTextView.setVisibility(View.VISIBLE);
                    mShowAnswer.setVisibility(View.INVISIBLE);

                }  //if-else API >= 21 (Lollipop)
*/

            }
        }); //mShowAnswer.onClickListener

        if (savedInstanceState != null) {
            Log.d(TAG, "restoring state from bundle");
            mWasAnswerShown = savedInstanceState.getBoolean(BUNDLE_KEY_ANSWER_SHOWN);
            if (mWasAnswerShown) {
                setAnswerShownResult(true);
                showAnswerText(mAnswerIsTrue);
            }
        }

        //Display current device API in a TextView:
        mApiLevelTextView.setText(
                String.format(Locale.CANADA,
                "API level %d",
                Build.VERSION.SDK_INT));

    } //onCreate()

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(BUNDLE_KEY_ANSWER_SHOWN, mWasAnswerShown);
    }

    /**
     * Sends back an intent to the parent activity indicating if the answer
     * was shown.
     */
    private void setAnswerShownResult(boolean isAnswerShown) {

        Intent data = new Intent();
        data.putExtra(INTENT_KEY_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);

        /*
            If user presses back button without pressing "Show Answer" button, this isn't called.
            Therefore no intent is sent back to parent activity.
         */

    }//setAnswerShownResult(...)

    /**
     * Encapsulate code that updates textView with the correct answer T/F
     *
     * @param answerIsTrue indicates if the answer is True or False.
     */
    private void showAnswerText(boolean answerIsTrue) {

        if (answerIsTrue) {
            mAnswerTextView.setText(R.string.true_button);
        } else {
            mAnswerTextView.setText(R.string.false_button);
        }

    }//showAnswerText(...)
}
