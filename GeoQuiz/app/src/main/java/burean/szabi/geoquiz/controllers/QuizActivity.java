package burean.szabi.geoquiz.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import burean.szabi.geoquiz.R;
import burean.szabi.geoquiz.models.Question;

public class QuizActivity extends AppCompatActivity {

    private static final String TAG = QuizActivity.class.getSimpleName(); //for logging messages
    private static final String BUNDLE_KEY_BANK_INDEX = "mCurrentIndex";
    private static final int INTENT_REQUEST_CODE_CHEAT = 0;
    private static final String BUNDLE_KEY_CHEAT_TRACKER = "mCheatTracker";
//    private static final String BUNDLE_KEY_IS_CHEATER = "mIsCheater";

    //instance variables:
    private Button mFalseButton, mTrueButton, mCheatButton;
    private ImageButton mNextButton, mPrevButton;
    private TextView mQuestionTextView;
    private int mCurrentIndex;
//    private boolean mIsCheater; //track if user cheated on CURRENT question - false by default

    //obviously our model data would be created elsewhere; this is just for simplicity.
    private Question[] mQuestionBank = {
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };

    //keep track of which questions the user cheated on; mIsCheater reset if user navigates away.
    private boolean[] mCheatTracker = new boolean[mQuestionBank.length];

    /*
        NOTE: @Override annotation asks compiler to make sure the superclass actually contains
                the class we're attempting to override.
      */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        Log.d(TAG, "onCreate()");

        /*
            When a layout is inflated, each widget in the XML Layout File is
            created as a view object. (With attributes as defined)
            NOTE: Specify which layout to inflate by passing in the
                    layout's resource ID: R.layout.<LAYOUT_NAME>
         */

        mFalseButton = (Button) findViewById(R.id.false_button);
        mTrueButton = (Button) findViewById(R.id.true_button);
        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mPrevButton = (ImageButton) findViewById(R.id.prev_button);
        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mCheatButton = (Button) findViewById(R.id.cheat_button);

        if (savedInstanceState != null) {
            //Restoring state after configuration change:
            mCurrentIndex = savedInstanceState.getInt(BUNDLE_KEY_BANK_INDEX);
//            mIsCheater = savedInstanceState.getBoolean(BUNDLE_KEY_IS_CHEATER);
            mCheatTracker = savedInstanceState.getBooleanArray(BUNDLE_KEY_CHEAT_TRACKER);
        }
        updateQuestion();

        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                increaseCounter();
                // mIsCheater is false by default;
                updateQuestion();
            }
        });

        /*
            To respond to button events, we need to implement listener interfaces.
            (An application of the observer pattern; "the observer interface")
            Recall inner classes for event handling
                -> convention is to use anon. inner classes;
                    lets us find code right where we expect it to be.
                -> also removes the overhead for a classname, it will only
                    be used in one place; where it's needed.
         */

        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                    When user clicks next button, increment mCurrentIndex by one,
                    and update mQuestionTextView's textValue.
                 */

                increaseCounter();
                updateQuestion();
//                if(mIsCheater) mIsCheater = false;

            }
        });

        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decreaseCounter();
                updateQuestion();
//                if(mIsCheater) mIsCheater = false;
            }
        });

        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                    An intent is an object that a component can use to communicate with the OS.
                    NOTE: components = activities, services, broadcast receivers, and
                            content providers.

                    Explicit intent; target activity (CheatActivity.class) must be declared
                    in the manifest.xml
                 */

                Intent i = CheatActivity.newIntent(QuizActivity.this,
                        mQuestionBank[mCurrentIndex].isAnswerTrue());
                startActivityForResult(i, INTENT_REQUEST_CODE_CHEAT);

                /*
                    2nd Arg: Request-Code used when an activity starts more than one type of child
                            activity, and needs to know who is reporting back.
                 */
            }
        });

    }//onCreate()

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult()");

        /*
            Okay if data empty in this case, will simply mean mIsCheater stays false (Default).
            Will display correct toast in checkAnswer() method.
            Happens when user pressed back button on CheatActivity without pressing "Show Answer".
         */

//        if (data == null) {
//            Log.e(TAG, "Data cannot be empty", new IllegalArgumentException());
//            return;
//        }

        //requestCode indicates which child finished and made a call to setResult(...), use switch:
        switch (requestCode) {
            case INTENT_REQUEST_CODE_CHEAT:

                if (resultCode == RESULT_OK) {

//                    mIsCheater = CheatActivity.wasAnswerShown(data);
                    mCheatTracker[mCurrentIndex] = CheatActivity.wasAnswerShown(data);

                }
//                else {
//                    //RESULT_CANCELED( when user simply pressed back w/o call to setResult(..) )
//                    //    or RESULT_FIRST_USER
//                }

                break;
            default:
                Log.e(TAG, "Invalid RequestCode", new IllegalStateException());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    /**
     * By default, Android saves the View Hierarchy within the Bundle object <br>
     * (during configuration change), and later restores it via the super call in onCreate(..).<br>
     * <br>
     * We can override onSaveInstanceState to save additional data to the Bundle ourselves.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState()");
        outState.putInt(BUNDLE_KEY_BANK_INDEX, mCurrentIndex);
//        outState.putBoolean(BUNDLE_KEY_IS_CHEATER, mIsCheater);
        outState.putBooleanArray(BUNDLE_KEY_CHEAT_TRACKER, mCheatTracker);
    }

    /**
     * Set question the QuestionBanks's current index value:
     */
    private void updateQuestion() {
        this.mQuestionTextView.setText(mQuestionBank[mCurrentIndex].getTextResId());
    }

    /**
     * Shows feedback to the user (via Toast) whether they answered the question correctly or not.
     *
     * @param userAnswer: boolean value representing user's choice for answer.
     */
    private void checkAnswer(boolean userAnswer) {

        int answer;

        if (mCheatTracker[mCurrentIndex]) {
            answer = R.string.judgment_toast;
        } else {
            if (userAnswer == mQuestionBank[mCurrentIndex].isAnswerTrue()) {
                answer = R.string.correct_toast;
            } else {
                answer = R.string.incorrect_toast;
            }
        }

        Toast.makeText(this, answer, Toast.LENGTH_SHORT).show();

    }

    private void increaseCounter() {
        /*
            NOTE: incrementing mCurrentIndex should loop back to the start of the array!
            (modulo by array.length)
         */
        mCurrentIndex = (mCurrentIndex + 1) % (mQuestionBank.length);
    }

    private void decreaseCounter() {

         /*
            Java modulus operator returns the remainder (same sign as the dividend - the number being divided)

            This is not as we expect -> ie: -1 % 5 = 4.

            So to get this operation, if our modulus operation m % n < 0, we have to
                take the result and wrap it around (ie: add n to m % n) -> (m%n) + n
         */

        int m = mCurrentIndex - 1;
        int n = mQuestionBank.length;

        mCurrentIndex = m % n < 0 ? (m % n) + n : m % n;
    }

}
