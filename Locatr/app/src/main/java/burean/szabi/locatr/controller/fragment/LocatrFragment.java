package burean.szabi.locatr.controller.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;

import burean.szabi.locatr.R;
import burean.szabi.locatr.model.GalleryItem;
import burean.szabi.locatr.utils.FlickrFetchr;

/**
 *
 */
public class LocatrFragment extends Fragment {

    private static final String TAG = LocatrFragment.class
            .getSimpleName();
    private static final int PERMISSION_REQ_FINE_COARSE_LOC = 1;
    private static final String STATE_TASK_RUNNING_FLAG = "mSearchTaskRunning";

    private ImageView mImageView;
    private Bitmap mGeoPhoto;
    private MenuItem mSearchItem;
    private GoogleApiClient mClient;
    private ProgressBar mProgressBar;
    private boolean mSearchTaskRunning;

    public static LocatrFragment newInstance() {

        Bundle args = new Bundle();

        LocatrFragment fragment = new LocatrFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true); //get calls to ToolBar
        setRetainInstance(true); //retain instance for AsyncTask ( & Bitmap )

        if(savedInstanceState != null){
            //restore custom state
            mSearchTaskRunning = savedInstanceState.getBoolean(STATE_TASK_RUNNING_FLAG);
        }

        //Create GooglePlayServices API client:
        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        //GooglePlayServices API Client is connected, refresh toolbar
                        getActivity().invalidateOptionsMenu();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        //Do nothing
                    }
                })
                .build();

    }//onCreate

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState); //save super state

        //save custom state:
        outState.putBoolean(STATE_TASK_RUNNING_FLAG, mSearchTaskRunning);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragRootView = inflater.inflate(R.layout.fragment_locatr, container, false);

        mImageView = (ImageView) fragRootView.findViewById(R.id.image);
        mProgressBar = (ProgressBar) fragRootView.findViewById(R.id.progressBar);

        if(mSearchTaskRunning){
            //view re-created and AsyncTask was running, show progress:
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }

        updateDisplayPhoto();
        return fragRootView;

    }//onCreateView

    @Override
    public void onStart() {
        super.onStart();
        mClient.connect(); //establish connection to GooglePlayServices

        //update search button's state that depends on connection - if we come back to this activity
        getActivity().invalidateOptionsMenu();

    }//onStart

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.fragment_locatr, menu);

        //handle to action item:
        mSearchItem = menu.findItem(R.id.action_locate);

        //This will also be ran once GooglePlayServices client connects
        mSearchItem.setEnabled(mClient.isConnected());

    }//onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_locate:

                findImage();
                return true;

            case R.id.action_settings:

                Toast.makeText(getActivity(), "Settings", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }//switch

    }//onOptionsItemSelected

    @Override
    public void onStop() {
        super.onStop();
        mClient.disconnect(); //clear connection to GooglePlayServices
        //no point updating toolbar here again
    }//onStop

    /**
     * Retrieves the device's latest location; fixme: update once done
     */
    private void findImage() {

        //First, check to make sure we have appropriate location permissions for API >= 23:
        int fineLocationPermCheck = ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.ACCESS_FINE_LOCATION
        );
        int coarseLocationPermCheck = ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION
        );

        if (fineLocationPermCheck != PackageManager.PERMISSION_GRANTED ||
                coarseLocationPermCheck != PackageManager.PERMISSION_GRANTED) {
            //Request permissions

            requestPermissions(
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQ_FINE_COARSE_LOC
            );

            //exit function if don't have permissions; can call once have them
            return;

        }//don't have permissions

        //Have permissions, continue getting location:

        //Create LocationRequest object specifying service parameters for request:
        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); //use power for high accuracy
        request.setNumUpdates(1); //request SINGLE fresh location
        request.setInterval(0); //location updates in ms, direct impact on power consumption.
        //NOTE: interval of 0 ok for single update = get update as soon as possible.

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mClient, request, new LocationChangedListener());

    }//findImage

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQ_FINE_COARSE_LOC:

                /* fixme:
                    - if user denies via "never ask me again" clicking the action button
                    will automatically show failure toast toast; alternative behaviour?
                 */

                //check if permission granted or not:
                if (grantResults.length > 0 && //not cancelled
                        grantResults[0] == PackageManager.PERMISSION_GRANTED && //1st permission OK
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {  //2nd permission OK
                    //permissions granted, do what we needed to:
                    mSearchItem.setEnabled(true);
                    findImage();
                } else {
                    //permission denied, disable location feature
                    Toast.makeText(
                            getActivity(),
                            "Location features disabled",
                            Toast.LENGTH_LONG
                    ).show();
                    mSearchItem.setEnabled(false);
                }

                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }//switch

    }//onRequestPermissionsResult

    /**
     * Event-handler that's called whenever new Location info is available
     * through GooglePlayServices' FusedLocationApi.
     */
    private class LocationChangedListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            //have new location info available, use it:
            Log.i(TAG, "location:" + location);

            if(!mSearchTaskRunning) {

                mGeoPhoto = null; //reset display photo
                updateDisplayPhoto();
                new SearchTask().execute(location);

            } else {
                //Alternatively, can disable action item and enable onPostExecute,
                // but then have to keep track of that through rotation

                Log.d(TAG, "already loading");
            }
        }//onLocationChanged

    }//LocationChangedListener

    private void updateDisplayPhoto(){
        mImageView.setImageBitmap(mGeoPhoto); //reset displayed photo
    }

    private class SearchTask extends AsyncTask<Location, Void, Bitmap> {

        private GalleryItem mPhoto;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mSearchTaskRunning = true;
            mProgressBar.setVisibility(View.VISIBLE);

        }//onPreExecute

        @Override
        protected Bitmap doInBackground(Location... params) {
            FlickrFetchr fetchr = new FlickrFetchr();

            //Retrieve a photo at specified Location
            List<GalleryItem> items = fetchr.searchPhotos(params[0]);

            //Log.i(TAG, "photo[0] = " + mItems.get(0).toString());
            if (items.size() == 0) {
                //Got no results, error or no photos
                Log.e(TAG, "no photos found");
                return null;
            }
            mPhoto = items.get(0);

            //Now download that photo
            Bitmap photoAtLocation = null;
            try {
                byte[] photoBytes = fetchr.getUrlBytes(mPhoto.getUrl());
                photoAtLocation = BitmapFactory
                        .decodeByteArray(photoBytes, 0, photoBytes.length);
            } catch (IOException ioe) {
                Log.e(TAG, "" + ioe);
            }
            return photoAtLocation;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            //On MainThread, update ImageView with photo Bitmap
            mGeoPhoto = bitmap;
            updateDisplayPhoto();
            mSearchTaskRunning = false;
            mProgressBar.setVisibility(View.GONE);

        }//onPostExecute

    }//SearchTask

}//LocatrFragment
