package burean.szabi.locatr.controller.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import burean.szabi.locatr.model.GalleryItem;
import burean.szabi.locatr.utils.FlickrFetchr;

/**
 * fixme: add appropriate class names
 * Fragment that hosts an AsyncTask to perform some task.
 * Will retain instance over config changes to let AsyncTask finish.
 * Reports status of AsyncTask to host through callback interface.
 */
public class AsyncTaskFragment extends Fragment {

    /** NOTE: ONLY FOR PRACTICE: How to host an AsyncTask in a fragment & retain instance **/

    private static final String TAG = AsyncTaskFragment.class.getSimpleName();
    private static final String ARG_LOCATION = "location";

    private TaskStatus mTaskListener;

    /**
     * Returns a new instance of this Fragment with appropriate args set.
     *
     * @param location Geo. Location for photo search.
     * @return Fragment instance with args set.
     */
    public static AsyncTaskFragment newInstance(Location location) {

        Bundle args = new Bundle();
        args.putParcelable(ARG_LOCATION, location);
        AsyncTaskFragment fragment = new AsyncTaskFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //Host needs to implement callback interface:
        if (context instanceof TaskStatus) {
            mTaskListener = (TaskStatus) context;
        } else {
            //Host didn't implement callback interface
            throw new ClassCastException(context.toString() +
                    "must implement " + TaskStatus.class.getSimpleName());
        }

        /*
            ALTERNATIVELY, can use Observer Pattern methods & provide a "start" method.
            Makes this more re-usable as it isn't restricted to be hosted by a component.
            -> Can then host in another fragment
         */

    }//onAttach

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Location geoLocation = getArguments().getParcelable(ARG_LOCATION);

        new SearchTask().execute(geoLocation);

    }//onCreate

    public interface TaskStatus {

        void onStart();

        void onComplete(Bitmap bitmap);

    }//TaskStatus

    private class SearchTask extends AsyncTask<Location, Void, Bitmap> {

        private GalleryItem mPhoto;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mTaskListener.onStart();

        }//onPreExecute

        @Override
        protected Bitmap doInBackground(Location... params) {
            FlickrFetchr fetchr = new FlickrFetchr();

            //Retrieve a photo at specified Location
            List<GalleryItem> items = fetchr.searchPhotos(params[0]);

            //Log.i(TAG, "photo[0] = " + mItems.get(0).toString());
            if (items.size() == 0) {
                //Got no results, error or no photos
                Log.e(TAG, "no photos found");
                return null;
            }
            mPhoto = items.get(0);

            //Now download that photo
            Bitmap photoAtLocation = null;
            try {
                byte[] photoBytes = fetchr.getUrlBytes(mPhoto.getUrl());
                photoAtLocation = BitmapFactory
                        .decodeByteArray(photoBytes, 0, photoBytes.length);
            } catch (IOException ioe) {
                Log.e(TAG, "" + ioe);
            }
            return photoAtLocation;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            mTaskListener.onComplete(bitmap);

        }//onPostExecute

    }//SearchTask


}
