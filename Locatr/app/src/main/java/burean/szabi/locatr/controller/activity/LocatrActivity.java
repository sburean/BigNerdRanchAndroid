package burean.szabi.locatr.controller.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import burean.szabi.locatr.controller.fragment.LocatrFragment;
import burean.szabi.locatr.controller.SingleFragmentActivity;

public class LocatrActivity extends SingleFragmentActivity {

    private static final String TAG = LocatrActivity.class.getSimpleName();
    private static final int REQUEST_ERROR = 0;

    @Override
    protected Fragment getFrag() {
        return LocatrFragment.newInstance();
    }//getFrag

    @Override
    protected void onResume() {
        super.onResume();

        /*
            Before the activity comes into focus, check to make sure that Google Play Services
            is available on the device before continuing - easy via PlayServices library:
         */

        GoogleApiAvailability playServicesAvailability = GoogleApiAvailability.getInstance();
        int errCode = playServicesAvailability.isGooglePlayServicesAvailable(this);

        if (errCode != ConnectionResult.SUCCESS) {
            //Runtime error connection to GooglePlayServices, show Error Dialog:

            Dialog errorDialog = playServicesAvailability
                    .getErrorDialog(this, errCode, REQUEST_ERROR, new DialogInterface
                            .OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            //If user cancelled ErrorDialog - exit:
                            finish();
                        }
                    });
            errorDialog.show();
            /*
                NOTE: Not a good way to display an error dialog, see:
                https://developers.google.com/android/guides/api-client#handle_connection_failures
             */

        }//PlayServicesAPI not available?

    }//onResume

}//LocatrActivity
