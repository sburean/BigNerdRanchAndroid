package burean.szabi.locatr.model;

import android.net.Uri;

/**
 * Model class to represent a gallery photo from Flickr.
 * Contains relevant fields with information extracted from Flicker API response.
 */
public class GalleryItem {

    private String mCaption, mPhotoId, mUrl, mOwnerId;

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public String getPhotoId() {
        return mPhotoId;
    }

    public void setPhotoId(String id) {
        mPhotoId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public String toString() {
        return mCaption;
    }

    public String getOwnerId() {
        return mOwnerId;
    }

    public void setOwnerId(String ownerId) {
        this.mOwnerId = ownerId;
    }

    /**
     * Creates a Uri to access this GalleryItem's Flickr web page.
     * @return Uri to access this GalleryItem's Flickr web page.
     */
    public Uri getPhotoPageUri(){

        //To see URL format for a Flickr photo's web page:
        //  https://www.flickr.com/services/api/misc.urls.html

        return Uri.parse("https://www.flickr.com/photos/")
                .buildUpon()
                .appendPath(mOwnerId)
                .appendPath(mPhotoId)
                .build();

    }//getPhotoPageUri

}//GalleryItem
