package burean.szabi.locatr.controller;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import burean.szabi.locatr.R;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    private static final String TAG = SingleFragmentActivity.class
            .getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = fm.findFragmentById(R.id.fragment_container);
        if(frag == null){
            fm.beginTransaction()
                    .add(R.id.fragment_container, getFrag())
                    .commit();
        }

    }//onCreate

    @LayoutRes
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    protected abstract Fragment getFrag();

}//SingleFragmentActivity
