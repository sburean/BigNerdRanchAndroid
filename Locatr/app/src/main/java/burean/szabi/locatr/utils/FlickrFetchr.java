package burean.szabi.locatr.utils;

import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import burean.szabi.locatr.controller.SingleFragmentActivity;
import burean.szabi.locatr.model.GalleryItem;


/**
 * Class to handle networking operations. Used to fetch flickr data from
 * its API via RESTful services.
 */
public class FlickrFetchr {

    private static final String TAG = FlickrFetchr.class.getSimpleName();

    private int HTTP_INPUT_MAX_BYTES_PER_READ = 1024;

    private static final String FLICKR_ENDPOINT = "https://api.flickr.com/services/rest/";
    private static final String FLICKR_ERR_RESPONSE_VALUE = "fail";
    private static final String FLICKR_API_KEY = "api_key";
    private static final String FLICKR_API_KEY_VALUE = "991e189e48a90296b8a3773d9e6e2e13";
    private static final String FLICKR_GET_FORMAT = "format";
    private static final String FLICKR_GET_FORMAT_VALUE = "json";
    private static final String FLICKR_NOJSONCALLBACK = "nojsoncallback";
    private static final String FLICKR_NOJSONCALLBACK_VALUE = "1";
    private static final String FLICKR_METHOD = "method";
    private static final String FLICKR_METHOD_GET_RECENT = "flickr.photos.getRecent";
    private static final String FLICKR_METHOD_EXTRAS = "extras";
    private static final String FLICKR_METHOD_EXTRAS_URL_S_VALUE = "url_s";
    private static final String FLICKR_METHOD_PHOTO_SEARCH = "flickr.photos.search";
    private static final String FLICKR_METHOD_PHOTO_SEARCH_TEXT = "text";
    private static final String FLICKR_METHOD_PHOTO_SEARCH_LAT = "lat";
    private static final String FLICKR_METHOD_PHOTO_SEARCH_LNG = "lon";
    private static final Uri HTTP_URI_FLICKR_ENDPOINT =
            Uri.parse(FLICKR_ENDPOINT)
                    .buildUpon()
                    .appendQueryParameter(FLICKR_API_KEY, FLICKR_API_KEY_VALUE)
                    .appendQueryParameter(FLICKR_GET_FORMAT, FLICKR_GET_FORMAT_VALUE)
                    .appendQueryParameter(FLICKR_NOJSONCALLBACK, FLICKR_NOJSONCALLBACK_VALUE)
                    .appendQueryParameter(FLICKR_METHOD_EXTRAS, FLICKR_METHOD_EXTRAS_URL_S_VALUE)
                    .build();

    /**
     * Fetches raw data from a URL and returns it as an array of bytes. <br>
     * This method requires the app to have INTERNET permission. <br>
     * NOTE: Should NOT be run on the main UI thread!
     *
     * @param urlSpec String representing the URL; data source.
     * @throws IOException Networking error.
     */
    public byte[] getUrlBytes(String urlSpec) throws IOException {
        //IMPORTANT: Need to close HttpURLConnection resources once done.
        //Can also add a check to make sure it's not on UI Thread: if(Thread.currentThread()....)

        //Get a URL object and open an HTTP connection to it: (no timeouts set)
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        //NOTE: Look at Http(s)URLConnection API for specifics: .setRequestMethod, etc...

        try { //Use try-finally to close HTTP connection once we're done with it:

            //Connection only connects to endpoint once we get a stream from the HttpURLConnection.
            InputStream in = connection.getInputStream();

            //First, make sure we get a valid responseCode from the web server before reading data:
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                //invalid HTTP responseCode, throw exception: (StatusCode useful for debug)
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            } else {
                //valid HTTP responseCode:

            /*
                Read bytes from the inputStream (GET) provided by the connection;
                recall best practices for reading bytes from an InputStream!
                (Networking project in Eclipse)
                Recall: closing a ByteArrayOutputStream has no effect
            */
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                int bytesRead;
                byte[] readBuffer = new byte[HTTP_INPUT_MAX_BYTES_PER_READ];
                while ((bytesRead = in.read(readBuffer)) != -1) {

                    //read bytes to byteArrayOutputStream; once done return it as a byte[].
                    byteArrayOutputStream.write(readBuffer, 0, bytesRead);

                }//while (we're reading from HTTP connection's inputStream)

                return byteArrayOutputStream.toByteArray();
            }//if-else(valid HTTP response)?

        } finally {
            //disconnect HttpURLConnection when done
            connection.disconnect();
        }

    }//getUrlBytes

    /**
     * Convert byte-array result from {@link #getUrlBytes} to a String.
     *
     * @param urlSpec String representing the URL; data source.
     * @throws IOException Networking error.
     */
    private String getUrlString(String urlSpec) throws IOException {

        return new String(getUrlBytes(urlSpec));

    }//getUrlString

    /**
     * Helper method to build a string representation of a valid URL based on the
     * specified method to run at the end-point.
     *
     * @param method Method to run at the end-point (Required)
     * @param query  Query for search-related methods; null if no search required (Optional)
     * @return String representation of URL
     */
    private String buildUrl(String method, String query) {

        Uri.Builder urlBuilder = HTTP_URI_FLICKR_ENDPOINT.buildUpon()
                .appendQueryParameter(FLICKR_METHOD, method);

        if ((method.equals(FLICKR_METHOD_PHOTO_SEARCH)) && (query != null)) {
            //have search method & provided query, append it to URL:
            urlBuilder.appendQueryParameter(FLICKR_METHOD_PHOTO_SEARCH_TEXT, query);
        }

        return urlBuilder.build().toString();

    }//buildUrl - method/query

    /**
     * Helper method to build a string representation of a valid URL to search flickr photos
     * around the specified geographical area.
     * @param location Photo search location.
     * @return String representation og URL
     */
    private String buildUrl(Location location) {

        Uri.Builder urlBuilder = HTTP_URI_FLICKR_ENDPOINT.buildUpon()
                .appendQueryParameter(FLICKR_METHOD, FLICKR_METHOD_PHOTO_SEARCH)
                .appendQueryParameter(FLICKR_METHOD_PHOTO_SEARCH_LAT, "" + location.getLatitude())
                .appendQueryParameter(FLICKR_METHOD_PHOTO_SEARCH_LNG, "" + location.getLongitude())
                .appendQueryParameter("page", "1")
                .appendQueryParameter("per_page", "1");
        //NOTE: last two for convenience, since we're only taking the first photo, only get one!

        return urlBuilder.build().toString();

    }//buildUrl - location

    /**
     * Helper method that is used to fetch data in a RESTful GET request.
     *
     * @return List of fetched results.
     */
    private List<GalleryItem> downloadGalleryItems(String url) {

        List<GalleryItem> items = new ArrayList<>();

        try {

            /*
                Use the Uri.Builder to construct a valid URL by simply
                appending parameters to a base URL.
             */

            //Fetch JSON response data from the web-server with a GET request:
            String stringJSONResult = getUrlString(url);
//            Log.i(TAG, "Received JSON: " + stringJSONResult);

            //Create a rootJSONObject:
            JSONObject rootJSONObject = new JSONObject(stringJSONResult);

            //Parse JSON response with org.json package:
//            parseJSON(items, rootJSONObject);

            //parse JSON response with GSON (com.google.gson package)
            parseJSONwithGSON(items, stringJSONResult);

        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON" + je);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items" + ioe);
        }

//        Log.i(TAG, "total GalleryItems: " + items.size());
        return items;

    }//downloadGalleryItems

    /**
     * Used to retrieve a list of the most-recent public photos from Flickr.
     *
     * @return List of items containing Flickr photos
     */
    public List<GalleryItem> fetchRecentPhotos() {

        String url = buildUrl(FLICKR_METHOD_GET_RECENT, null);
        return downloadGalleryItems(url);

    }//fetchRecentPhotos

    /**
     * Used to retrieve a list of photos based on a search query.
     *
     * @param query search query for photos
     * @return List of items containing resulting Flickr photos.
     */
    public List<GalleryItem> searchPhotos(String query) {

        String url = buildUrl(FLICKR_METHOD_PHOTO_SEARCH, query);
        return downloadGalleryItems(url);

    }//searchPhotos-query

    /**
     * Used to retrieve a list of photos based on provided Location.
     *
     * @param location Photo search location.
     * @return List of items containing resulting Flickr photos.
     */
    public List<GalleryItem> searchPhotos(Location location) {

        String url = buildUrl(location);
        return downloadGalleryItems(url);

    }//searchPhotos - location

    /**
     * Method that parses a root JSONObject; extracts a photo's caption, id, and
     * URL to create & populate a model object, and then store it in a model collection.
     *
     * @param items          Model collection to populate with models containing extracted JSON info.
     * @param rootJSONObject Root JSONObject in the JSON Hierarchy
     * @throws JSONException If JSON parsing fails
     */
    private void parseJSON(List<GalleryItem> items, JSONObject rootJSONObject)
            throws JSONException {

        //Parse JSON response into appropriate model objects:
        JSONObject photos = rootJSONObject.getJSONObject("photos");//photos object
        JSONArray photoArray = photos.getJSONArray("photo");//photo array of objects

        //loop through each photo in the photoArray and extract data:
        for (int i = 0; i < photoArray.length(); i++) {

            //Get a JSONObject representing each photo in the array
            JSONObject photo = photoArray.getJSONObject(i);

            //NOTE: Only add item to model collection if it has a valid URL to photo:
            if (!photo.has("url_s")) {
                //Doesn't have valid URL to photo, ignore it; continue to next loop iteration
                continue;
            }

            //Extract data into a model object:
            GalleryItem galleryItem = new GalleryItem();
            galleryItem.setCaption(photo.getString("title"));
            galleryItem.setPhotoId(photo.getString("id"));
            galleryItem.setOwnerId(photo.getString("owner"));
            galleryItem.setUrl(photo.getString("url_s"));

            //Add model object to a list:
            items.add(galleryItem);

        }//for-loop through photoArray

    }//parseJSON

    private void parseJSONwithGSON(List<GalleryItem> items, String stringJSONResult)
            throws JSONException {

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(GalleryItem[].class, new CustomDeserializer())
                .create();

        try {
            GalleryItem[] testItems = gson.fromJson(stringJSONResult, GalleryItem[].class);

//            Log.d(TAG, "test");

            for (GalleryItem item : testItems) {
                //for each item in our array, add it to the list
                /* Error check JSON (POJO) fields here as necessary */
                items.add(item);
            }

        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.toString());
        }

    }//parseJSONwithGSON

    /**
     * User to implement a custom deserialization mechanism for GSON.
     * It returns an array of gallery items from the JSON's "photo" array.
     * <p>
     * IMPORTANT NOTES: <br>
     * - If we're de-serializing an array of objects, simply use an
     * array of the POJO that we want to create from the JSON array!
     * (See GalleryItem[] usage) <br>
     * - Upon JSON error response, will return an EMPTY array of items!
     */
    private class CustomDeserializer implements JsonDeserializer<GalleryItem[]> {
        @Override
        public GalleryItem[] deserialize(JsonElement json,
                                         Type typeOfT,
                                         JsonDeserializationContext context)
                throws JsonParseException {

            //TODO: Here, check if have a "stat" object with value "fail" - error JSON response.
            //stat with "value = ok" is a good response
            JsonElement responseSTATUS = json.getAsJsonObject().get("stat");
            if(responseSTATUS.getAsString().equals(FLICKR_ERR_RESPONSE_VALUE)){
                //We got an error response, handle.
                Log.e(TAG, "Received JSON error response");
                return new GalleryItem[]{}; //return empty item array
            }

            //The JsonElement in the parameter is the root json object
            JsonElement photos = json.getAsJsonObject().get("photos");

            //get the "photo" JSONElement, which is a JSON array
            JsonElement photoArray = photos.getAsJsonObject().get("photo");

            //Deserialize the photo-ARRAY w/e custom FieldNamingStrategy
            Gson gson = new GsonBuilder()
                    .setFieldNamingStrategy(new CustomFieldNamingStrategy())
                    .create();

            return gson.fromJson(photoArray, GalleryItem[].class);

        }//deserialize

    }//CustomDeserializer

    /**
     * Custom FieldNamingStrategy that defines how to map a POJO's field name
     * into JSON element names.
     * The Overridden "translateName" method returns the <b>JSON</b> element name!!
     */
    private class CustomFieldNamingStrategy implements FieldNamingStrategy {

        @Override
        public String translateName(Field f) {
            switch (f.getName()) {
                case "mCaption":
                    return "title";
                case "mPhotoId":
                    return "id";
                case "mUrl":
                    return "url_s";
                case "mOwnerId":
                    return "owner";
                default:
                    return f.getName();
            }
        }//translateName

    }//CustomFieldNamingStrategy

}
